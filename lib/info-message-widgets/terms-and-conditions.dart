import 'package:flutter/material.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class TermsAndConditionInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _titleStyle = TextStyle(
      fontSize: Tape.dx(24),
      fontWeight: FontWeight.bold,
      color: BMColors.bmDarkblue,
    );

    final _paragraphStyle = TextStyle(
      fontSize: Tape.dx(18),
      fontWeight: FontWeight.w300,
      color: BMColors.bmDarkblue,
    );

    return Column(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Terms and conditions',
              style: _titleStyle,
              textAlign: TextAlign.left,
            ),
            SizedBox(height: Tape.dx(16)),
            Text(
              'After entering your mobile phone number we will send you an SMS code. Depending on your telephone, the code will be entered automatically, otherwise you will have to do this yourself.',
              style: _paragraphStyle,
            ),
            SizedBox(height: Tape.dx(10)),
            Text(
              'Sometimes it takes a while before the SMS code arrives, please wait (max. 1 minute). If no SMS has been received, you must check that your mobile number is correct and you can send the code again. If you receive multiple SMS codes, you must always enter the last code, the codes from before are no longer valid.',
              style: _paragraphStyle,
            ),
            SizedBox(height: Tape.dx(10)),
            Text(
              'Receiving the SMS code is free and we do not provide third parties with your telephone number. You can always change your telephone number later in the app profile.',
              style: _paragraphStyle,
            ),
          ],
        )
      ],
    );
  }
}
