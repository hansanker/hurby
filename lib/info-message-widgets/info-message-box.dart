import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/shared/widgets/empty.dart';
import 'package:hurby/shared/widgets/footer-back-button.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class InfoMessageBox extends StatefulWidget {
  _InfoMessageBoxState createState() {
    return _InfoMessageBoxState();
  }
}

class _InfoMessageBoxState extends State<InfoMessageBox> {
  PopupService _popupService = serviceInjector.popupService;
  List<StreamSubscription> _subs = [];
  Widget _infoWidget;

  final _infoContainerDecoration = BoxDecoration(
    color: Colors.white,
    border: Border.all(
      color: Colors.transparent,
    ),
    borderRadius: AppStyles.panelRadiusAll,
    boxShadow: [
      new BoxShadow(
        color: Colors.grey,
        blurRadius: Tape.dx(5.0),
      ),
    ],
  );

  @override
  void initState() {
    super.initState();

    // here we subscribe to when the popupService for new info is triggered
    _subs.add(
      _popupService.infoBoxStream$.listen((Widget info) {
        setState(() {
          _infoWidget = info;
        });
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: Tape.deviceSize.width,
          height: Tape.deviceSize.height,
          color: Theme.of(context).primaryColor,
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(Tape.dx(16)),
              margin: EdgeInsets.only(bottom: Tape.dx(200)),
              constraints: BoxConstraints(
                minHeight: Tape.dx(500),
              ),
              child: Container(
                padding: EdgeInsets.all(Tape.dx(25)),
                decoration: _infoContainerDecoration,
                child: _infoWidget ?? EmptyWidget(),
              ),
            ),
          ),
        ),
        Positioned(
          width: Tape.deviceSize.width,
          bottom: 0,
          child: FooterbackButtonBar(
            onPressed: () {
              _popupService.dismissInfo();
            },
          ),
        )
      ],
    );
  }

  @override
  void dispose() {
    super.dispose();

    // unsubscribe to all subs
    _subs.forEach((sub) => sub.cancel());
  }
}
