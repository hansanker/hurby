import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

class BMColors {
  static final Color bmDarkblue = Color.fromRGBO(26, 68, 150, 1.0);
  static final Color bmGrey = Color.fromRGBO(212, 212, 212, 1.0);
  static final Color bmLightBlue = Color(0xff95E3ED);
  static final Color bmPrimaryColor = Color.fromRGBO(5, 185, 214, 1.0);
}

class AppStyles {
  // For the panel raduis
  static final panelRadiusAll = BorderRadius.only(
    topRight: Radius.circular(Tape.dx(4)),
    topLeft: Radius.circular(Tape.dx(24)),
    bottomLeft: Radius.circular(Tape.dx(24)),
    bottomRight: Radius.circular(Tape.dx(24)),
  );

  static final panelRadiusTop = BorderRadius.only(
    topRight: Radius.circular(Tape.dx(4)),
    topLeft: Radius.circular(Tape.dx(24)),
    bottomLeft: Radius.circular(0),
    bottomRight: Radius.circular(0),
  );

  static final panelRadiusBottom = BorderRadius.only(
    topRight: Radius.circular(0),
    topLeft: Radius.circular(0),
    bottomLeft: Radius.circular(Tape.dx(24)),
    bottomRight: Radius.circular(Tape.dx(24)),
  );

  static final panelRadiusNone = BorderRadius.zero;

  static final panelRadiusLeft = BorderRadius.only(
    topRight: Radius.circular(0),
    topLeft: Radius.circular(Tape.dx(24)),
    bottomLeft: Radius.circular(Tape.dx(24)),
    bottomRight: Radius.circular(0),
  );

  static final panelRadiusRight = BorderRadius.only(
    topRight: Radius.circular(Tape.dx(4)),
    topLeft: Radius.circular(0),
    bottomLeft: Radius.circular(0),
    bottomRight: Radius.circular(Tape.dx(24)),
  );

  // For the button radius
  static final buttonRadiusAll = BorderRadius.only(
    topRight: Radius.circular(Tape.dx(4)),
    topLeft: Radius.circular(Tape.dx(16)),
    bottomLeft: Radius.circular(Tape.dx(16)),
    bottomRight: Radius.circular(Tape.dx(16)),
  );

  static final buttonRadiusTop = BorderRadius.only(
    topRight: Radius.circular(Tape.dx(4)),
    topLeft: Radius.circular(Tape.dx(16)),
    bottomLeft: Radius.circular(0),
    bottomRight: Radius.circular(0),
  );

  static final buttonRadiusBottom = BorderRadius.only(
    topRight: Radius.circular(0),
    topLeft: Radius.circular(0),
    bottomLeft: Radius.circular(Tape.dx(16)),
    bottomRight: Radius.circular(Tape.dx(16)),
  );

  static final buttonRadiusRight = BorderRadius.only(
    topRight: Radius.circular(Tape.dx(4)),
    topLeft: Radius.circular(0),
    bottomLeft: Radius.circular(0),
    bottomRight: Radius.circular(Tape.dx(16)),
  );

  static final buttonRadiusLeft = BorderRadius.only(
    topRight: Radius.circular(0),
    topLeft: Radius.circular(16),
    bottomLeft: Radius.circular(16),
    bottomRight: Radius.circular(0),
  );

  static final buttonRadiusNone = BorderRadius.zero;
}
