import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/steps/select-items-count/select-items-count.style.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class SelectNumberOfItemsToReturn extends StatelessWidget
    with SelectNumberOfItemsToReturnStyles {
  final Function(int) onItemNumberSelected;
  final Function() onPrevious;

  SelectNumberOfItemsToReturn({
    this.onItemNumberSelected,
    this.onPrevious,
  });

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: _headerContent(),
      mainContent: EmptyWidget(),
      footerContent: _footerContainer(),
    );
  }

  Widget _headerContent() {
    return Column(
      children: <Widget>[
        ToolBar(
          onPrevious: onPrevious,
          infoMessage: EmptyWidget(),
          homeUrl: 'dashboard/home',
        ),
        SizedBox(
          height: Tape.dx(8),
        ),
        Container(
          margin: EdgeInsets.only(
            top: Tape.dx(20),
          ),
          child: Image(
            image: AssetImage('assets/images/ill_books_pickup.png'),
            fit: BoxFit.contain,
            height: Tape.dx(168.0),
          ),
        ),
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          alignment: Alignment.bottomCenter,
          child: Column(
            children: <Widget>[
              Text(
                'How many items',
                style: pageTitle,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: Tape.dx(4)),
              Text(
                'do you want to return',
                style: pageTitle,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: Tape.dx(8)),
              Text(
                'max 12 items',
                style: pageSubTitle,
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
        SizedBox(
          height: Tape.dx(22),
        ),
        PaddedContainer(
          contentHeight: Tape.dx(270),
          content: _getNumbersGrid(),
        ),
      ],
    );
  }

  Widget _getNumbersGrid() {
    List<Widget> _numbersList = [];
    double itemWidth = (Tape.deviceSize.width / 3) - Tape.dx(14);

    // here we use a forLoop to generate 12 numbers
    for (int i = 0; i < 12; i++) {
      _numbersList.add(
        Container(
          height: Tape.dx(60),
          width: itemWidth,
          child: RaisedButton(
            child: Text('${i + 1}', style: numberStyle),
            shape: RoundedRectangleBorder(
              borderRadius: AppStyles.buttonRadiusAll,
            ),
            onPressed: () {
              onItemNumberSelected(i + 1);
            },
            color: BMColors.bmPrimaryColor,
          ),
        ),
      );
    }

    return Wrap(
      spacing: Tape.dx(8),
      runSpacing: Tape.dx(8),
      children: _numbersList,
    );
  }
}
