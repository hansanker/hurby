import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

mixin SelectNumberOfItemsToReturnStyles {
  final pageTitle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );

  final pageSubTitle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(14),
    fontWeight: FontWeight.w300,
  );

  final numberStyle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(26),
    fontWeight: FontWeight.w500,
  );
}
