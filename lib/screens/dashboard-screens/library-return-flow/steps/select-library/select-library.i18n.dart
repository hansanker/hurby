import 'package:i18n_extension/i18n_extension.dart';

// Developed by Marcelo Glasberg (Aug 2019).
// For more info, see: https://pub.dartlang.org/packages/i18n_extension
extension Localization on String {
  static var t = Translations("en_us") +
      {
        "en_us": "Select library",
        "pt_br": "Incrementar",
      };

  String get i18n => localize(this, t);
}
