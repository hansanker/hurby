import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/button-pricetag-data.dart';
import 'package:hurby/shared/models/price-breakdown.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/button-with-pricetag/button-with-pricetag.dart';
import 'package:hurby/shared/widgets/footer-back-button.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';
import 'select-library.i18n.dart';
// import 'package:i18n_extension/i18n_widget.dart';

class SelectLibrary extends StatelessWidget {
  final Function onPrevious;
  final Function(LibraryInfo) onSelectLibrary;
  final List<LibraryInfo> libraryList;
  final OrderService _orderService = serviceInjector.orderService;

  SelectLibrary({
    @required this.onPrevious,
    @required this.onSelectLibrary,
    @required this.libraryList,
  });

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        title: 'Select library'.i18n,
        homeUrl: 'dashboard/home',
        infoMessage: EmptyWidget(),
        onPrevious: onPrevious,
      ),
      clearFixBy: Tape.dx(100),
      mainContent: _mainContent(),
      footerContent: FooterbackButtonBar(
        onPressed: onPrevious,
      ),
    );
  }

  Widget _mainContent() {
    return Container(
      padding: EdgeInsets.all(Tape.dx(10)),
      child: Column(
        children: _getLibraryList(),
      ),
    );
  }

  List<Widget> _getLibraryList() {
    List<Widget> widgets = [];

    for (int i = 0; i < libraryList.length; i++) {
      LibraryInfo library = libraryList[i];
      PriceBreakdown price = _orderService.getPriceComponents(
        library.price,
      );

      widgets.add(
        ButtonWithPriceTag(
          data: new ButtonWithPriceTagData(
            id: library.id,
            title: library.title,
            subtitle: library.subtitle,
            unitPrice: price.unitPrice,
            fractionalPrice: price.fractionalPrice,
            leadingImage: Image(
              image: NetworkImage(library.imageUrl),
            ),
            priceTitle: library.priceTitle,
          ),
          onPressed: () {
            onSelectLibrary(library);
          },
          theme: 'light',
        ),
      );

      if (i < 3) {
        widgets.add(SizedBox(
          height: Tape.dx(8),
        ));
      }
    }

    return widgets;
  }
}
