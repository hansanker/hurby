import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class LibraryIntroduction extends StatelessWidget {
  final Function onSkip;

  LibraryIntroduction({
    @required this.onSkip,
  });

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: Container(
        height: Tape.dx(300),
        child: Center(
          child: FlareActor(
            'assets/flare/library_intro.flr',
            animation: 'in',
          ),
        ),
      ),
      footerContent: PaddedContainer(
        contentHeight: Tape.dx(72),
        content: _skipStep(context),
      ),
    );
  }

  Widget _skipStep(BuildContext context) {
    return RaisedButton(
      onPressed: onSkip,
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: Theme.of(context).primaryColor,
      child: Text(
        'skip',
        style: TextStyle(
          color: Colors.white,
          fontSize: Tape.dx(26.0),
          fontWeight: FontWeight.w700,
        ),
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }
}
