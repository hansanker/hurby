import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/steps/library-order-summary/library-order-summary.style.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/custom-list-tile-config.dart';
import 'package:hurby/shared/models/price-breakdown.dart';
import 'package:hurby/shared/models/profile.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/custom-list-tile/custom-list-tile.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/price-tag.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class LibraryOrderSummary extends StatefulWidget {
  final LibraryFlowData flowData;
  final Function onDeleteOrder;
  final Function onCompleteOrder;
  final Function onPrevious;
  final Function onSelectBookCount;
  final Function onSelectLibrary;

  LibraryOrderSummary({
    @required this.onDeleteOrder,
    @required this.onCompleteOrder,
    @required this.onPrevious,
    @required this.onSelectBookCount,
    @required this.onSelectLibrary,
    @required this.flowData,
  });

  @override
  _LibraryOrderSummaryState createState() {
    return _LibraryOrderSummaryState();
  }
}

class _LibraryOrderSummaryState extends State<LibraryOrderSummary>
    with LibraryOrderSummaryStyle {
  final OrderService _orderService = serviceInjector.orderService;

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        title: 'Order summary',
        homeUrl: 'dashboard/home',
        infoMessage: EmptyWidget(),
        onPrevious: widget.onPrevious,
      ),
      clearFixBy: Tape.dx(100),
      mainContent: _bodyContent(context),
      footerContent: _footerContent(),
    );
  }

  Widget _bodyContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: Tape.dx(12),
        right: Tape.dx(12),
      ),
      child: Column(
        children: <Widget>[
          _pickup(),
          SizedBox(height: Tape.dx(8)),
          _books(),
          SizedBox(height: Tape.dx(8)),
          _delivery(),
          SizedBox(height: Tape.dx(16)),
          _delete(),
        ],
      ),
    );
  }

  Widget _pickup() {
    Profile profile = widget.flowData.userProfile;
    String pickupDay = _initialCap(
      widget.flowData.selectedPickupDate['dayName'],
    );

    return CustomListTile(
      items: [
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          leadingImg: 'assets/images/icon_send_green.png',
          title: 'Pickup',
          titleColor: Theme.of(context).primaryColor,
          // omit the subtitle
          body: [profile.personDTO.firstName],
          bodyColor: BMColors.bmDarkblue,
          onClick: () {},
          // omit trailing
          showBorderRadiusTop: true,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(55),
          body: [
            '${profile.personDTO.physicalAddressDTO.postalCode} ${profile.personDTO.physicalAddressDTO.city}',
            profile.personDTO.physicalAddressDTO.addressLine1,
          ],
          bodyColor: Colors.grey,
          onClick: () {},
          // omit trailing
          showBorderRadiusTop: false,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
          isLink: false,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          body: ['$pickupDay 19 - 21hr'],
          bodyColor: BMColors.bmPrimaryColor,
          onClick: () {},
          // omit trailing
          showBorderRadiusTop: false,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
          isLink: true,
        ),
      ],
    );
  }

  String _initialCap(String input) {
    return '${input[0].toUpperCase()}${input.substring(1)}';
  }

  Widget _books() {
    String orderNumber = widget.flowData.pickupInfoRawData['orderNumber'];
    String orderNumShort = 'BM' + orderNumber.substring(orderNumber.length - 4);
    LibraryInfo selectedLibrary = widget.flowData.selectedLibrary;
    PriceBreakdown price = _orderService.getPriceComponents(
      selectedLibrary.price,
    );

    return CustomListTile(
      config: CustomListTileConfig(
        tileHeight: Tape.dx(60),
        leadingImg: 'assets/images/icon_books.png',
        title: 'Books',
        titleColor: BMColors.bmDarkblue,
        body: ['${widget.flowData.itemCount} items'],
        bodyColor: BMColors.bmPrimaryColor,
        onClick: widget.onSelectBookCount,
        trailingWidget: Container(
          padding: EdgeInsets.all(Tape.dx(5)),
          child: Column(
            children: <Widget>[
              PriceTag(
                unitPrice: price.unitPrice,
                fractionalPrice: price.fractionalPrice,
              ),
              SizedBox(
                height: Tape.dx(5),
              ),
              Text(
                orderNumShort,
                style: TextStyle(
                  fontSize: Tape.dx(12),
                  color: Colors.grey,
                ),
              ),
            ],
          ),
        ),
        trailingWidth: Tape.dx(100),
        trailingAlignment: Alignment.topRight,
        showBorderRadiusTop: true,
        showBorderRadiusBottom: true,
        showBottomBorder: false,
        isLink: true,
      ),
    );
  }

  Widget _delivery() {
    String pickupDay = _initialCap(
      widget.flowData.selectedPickupDate['dayName'],
    );

    return CustomListTile(
      items: [
        CustomListTileConfig(
          tileHeight: Tape.dx(74),
          leadingImg: 'assets/images/icon_receive_green.png',
          title: 'Delivery',
          titleColor: BMColors.bmDarkblue,
          // omit the subtitle
          body: [
            widget.flowData.selectedLibrary.title,
            widget.flowData.selectedLibrary.subtitle,
          ],
          bodyColor: BMColors.bmPrimaryColor,
          onClick: widget.onSelectLibrary,
          // omit trailing
          showBorderRadiusTop: true,
          showBorderRadiusBottom: true,
          showBottomBorder: true,
          isLink: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(50),
          body: ['$pickupDay 19 - 21hr'],
          bodyColor: Colors.grey,
          onClick: () {},
          // omit trailing
          showBorderRadiusTop: true,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
          isLink: false,
        ),
      ],
    );
  }

  Widget _delete() {
    return CustomListTile(
      items: [
        CustomListTileConfig(
          leadingImg: 'assets/images/icon_delete.png',
          tileHeight: Tape.dx(60),
          title: 'Delete',
          titleColor: BMColors.bmDarkblue,
          // omit the subtitle
          body: ['Delete this order'],
          bodyColor: Theme.of(context).primaryColor,
          onClick: widget.onDeleteOrder,
          showBorderRadiusTop: true,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
          isLink: true,
        ),
      ],
    );
  }

  Widget _footerContent() {
    return PaddedContainer(
      contentHeight: Tape.dx(124),
      showShadow: true,
      content: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: Tape.dx(10),
              right: Tape.dx(10),
            ),
            child: _totalPrice(),
          ),
          RaisedButton(
            onPressed: widget.onCompleteOrder,
            padding: EdgeInsets.all(
              Tape.dx(20),
            ),
            color: Theme.of(context).primaryColor,
            child: Text(
              'complete',
              style: completeButton,
            ),
            shape: new RoundedRectangleBorder(
              borderRadius: AppStyles.buttonRadiusAll,
            ),
          ),
        ],
      ),
    );
  }

  Widget _totalPrice() {
    LibraryInfo selectedLibrary = widget.flowData.selectedLibrary;
    PriceBreakdown price = _orderService.getPriceComponents(
      selectedLibrary.price,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Total', style: totalText),
            Expanded(child: EmptyWidget()),
            PriceTag(
              unitPrice: price.unitPrice,
              fractionalPrice: price.fractionalPrice,
              color: BMColors.bmDarkblue,
            )
          ],
        ),
        Text('Tax included', style: totalSubText),
        SizedBox(
          height: Tape.dx(8),
        ),
      ],
    );
  }
}
