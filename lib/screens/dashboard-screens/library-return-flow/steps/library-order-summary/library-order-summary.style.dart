import 'package:flutter/material.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

mixin LibraryOrderSummaryStyle {
  final totalText = TextStyle(
    fontSize: Tape.dx(24),
    fontWeight: FontWeight.w500,
    color: BMColors.bmDarkblue,
  );

  final totalSubText = TextStyle(
    fontSize: Tape.dx(14),
    fontWeight: FontWeight.w500,
    color: BMColors.bmGrey,
  );

  final completeButton = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(26.0),
    fontWeight: FontWeight.w700,
  );
}
