import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/choose-option-popup/choose-option-popup.dart';
import 'package:hurby/styles.dart';

mixin LibraryFlowScreenController {
  Function(String) _gotoStepCallback;
  LibraryFlowData flowData = new LibraryFlowData();
  final SpinnerService _spinnerService = serviceInjector.spinnerService;
  final OrderService _orderService = serviceInjector.orderService;
  final ProfileService _profileService = serviceInjector.profileService;
  final PopupService _popupService = serviceInjector.popupService;
  BuildContext _context;

  void initControllerInstance(
    Function renderStepsFn,
    BuildContext context,
    String orderId,
  ) async {
    _context = context;
    _gotoStepCallback = renderStepsFn;

    // we update the flow data before continuing
    await _updateFlowData(orderId);

    _gotoStepCallback('introduction');
  }

  Future<void> _updateFlowData(String orderId) async {
    _spinnerService.toggleSpinner(true);

    List<dynamic> asyncDatasets = await Future.wait([
      _orderService.getPickupInfo(),
      _profileService.getCurrentUserProfile(),
    ]);

    flowData.pickupInfoRawData = asyncDatasets[0];
    flowData.userProfile = asyncDatasets[1];
    flowData.libraryList = await _getLibrariesData(
      flowData.userProfile.siteDTO.parentSite['siteDid'].toString(),
    );

    if (orderId != null) {
      _refreshOrderFromId(orderId);
    }

    _spinnerService.toggleSpinner(false);
    return;
  }

  void _refreshOrderFromId(String orderId) async {
    dynamic orderInfo = await _orderService.getOrderDetails(orderId);

    // here we set the active order to just the order did
    flowData.activeOrder = {
      'did': orderInfo['did'],
    };

    // set the selected pickup date
    flowData.selectedPickupDate = {
      'date': orderInfo['actionDate'],
      'dayName': orderInfo['actionDay']
    };

    // we set the selected webshop
    String libraryId = '${orderInfo['receiver']['did']}';
    flowData.selectedLibrary = flowData.libraryList.firstWhere(
      (library) {
        return library.id == libraryId;
      },
      orElse: () {
        return null;
      },
    );

    // here we update the item count
    flowData.itemCount = orderInfo['itemCount'];

    // here we update the order number in the pickupInfoRawData
    flowData.pickupInfoRawData['orderNumber'] = orderInfo['number'];

    // Here we goto the summary page
    _gotoStepCallback('library_order_summary');
  }

  Future<List<LibraryInfo>> _getLibrariesData(String siteDid) async {
    String baseUrl = 'http://149.210.228.159:8080';
    dynamic res = await _orderService.getLibrariesPerHub(siteDid);
    List<LibraryInfo> libraries = [];

    for (dynamic data in res) {
      String imgUrl =
          '$baseUrl/Buurtmus/resources/core-customerApp/images/lf_library_${data['companyCode']}.png';

      libraries.add(
        LibraryInfo(
          id: '${data['personDid']}',
          title: data['companyName'],
          subtitle: data['affiliateName'],
          price: double.parse(
            '${data['price']['priceInclVat']}',
          ),
          imageUrl: imgUrl,
          priceTitle: data['priceRemark'] ?? 'now',
          companyCode: data['companyCode'],
        ),
      );
    }

    return libraries;
  }

  void checkIfUserNameExists() {
    String userName = flowData.userProfile.personDTO.firstName ?? '';
    if (userName.isEmpty) {
      _gotoStepCallback('update_user_name');
    } else {
      _gotoStepCallback('library_order_summary');
    }
  }

  void updateProfileName(String name) async {
    if (!_spinnerService.isLoading) {
      _spinnerService.toggleSpinner(true);

      // here we update the name on the user profile
      flowData.userProfile.personDTO.firstName = name;
      await _profileService.updateUserProfile(flowData.userProfile);

      _gotoStepCallback('library_order_summary');
      _spinnerService.toggleSpinner(false);
    }
  }

  void handleDeleteOrder() async {
    dynamic res = await _popupService.showCustomDialog(
      context: _context,
      content: ChooseOptionPopup(
        messsageText: 'Are you sure to delete this order?',
        confirmText: 'yes, delete',
        cancelText: 'back',
        image: 'assets/images/order_delete.png',
        confirmButtonColor: BMColors.bmLightBlue,
      ),
    );

    if (res) {
      if (flowData.activeOrder['did'] != null) {
        await _orderService.deleteOrder(flowData.activeOrder['did']);
      }

      Navigator.of(_context).pushReplacementNamed(
        'dashboard/home',
      );
    }
  }

  void handleCompleteOrder() async {
    if (!_spinnerService.isLoading) {
      _spinnerService.toggleSpinner(true);

      flowData.activeOrder['did'] = flowData.activeOrder['did'] ?? '';
      flowData.activeOrder['orderFlow'] = 'STD_RETURN_LIBRARY_FLOW';
      flowData.activeOrder['actionDate'] = flowData.selectedPickupDate['date'];
      flowData.activeOrder['number'] =
          flowData.pickupInfoRawData['orderNumber'];
      flowData.activeOrder['receiver'] = {
        "did": flowData.selectedLibrary.id,
        "companyName": flowData.selectedLibrary.companyCode,
      };
      flowData.activeOrder['itemCount'] = flowData.itemCount;
      flowData.activeOrder['shipmentSizeCode'] = 'SMALL';

      await _orderService.createOrder(flowData.activeOrder);

      // Goto order overview
      Navigator.of(_context).pushReplacementNamed(
        'dashboard/payment_flow',
      );

      _spinnerService.toggleSpinner(false);
    }
  }

  // destroy all subscriptions
  void disposeControllerInstance() {}
}
