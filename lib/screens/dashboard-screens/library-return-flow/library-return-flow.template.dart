import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/library-return-flow.controller.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/steps/library-intro/library-intro.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/steps/library-order-summary/library-order-summary.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/steps/return-instructions/return-instructions.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/steps/select-items-count/select-items-count.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/steps/select-library/select-library.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/steps/root.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/index.dart';

class LibraryFlowScreen extends StatefulWidget {
  final String orderId;

  LibraryFlowScreen({
    @required this.orderId,
  });

  @override
  _LibraryFlowScreenState createState() {
    return _LibraryFlowScreenState();
  }
}

class _LibraryFlowScreenState extends State<LibraryFlowScreen>
    with LibraryFlowScreenController {
  Map<String, Widget> steps;
  Widget activeStep = EmptyWidget();

  @override
  void initState() {
    super.initState();

    // init the controller instance
    initControllerInstance(
      _gotoStep,
      context,
      widget.orderId,
    );
  }

  // STEPS FOR PAYMENT FLOW ====>
  void _gotoStep(String step) {
    steps = {
      'introduction': LibraryIntroduction(
        onSkip: () {
          _gotoStep('select_library');
        },
      ),
      'select_library': SelectLibrary(
        onPrevious: () {
          Navigator.of(context).pushReplacementNamed(
            'dashboard/parcel_type_selection',
          );
        },
        onSelectLibrary: (LibraryInfo library) {
          flowData.selectedLibrary = library;
          _gotoStep('select_pickup_evening');
        },
        libraryList: flowData.libraryList,
      ),
      'select_pickup_evening': SelectPickupEvening(
        pickupInfo: flowData.pickupInfoRawData,
        onPrevious: () {
          _gotoStep('select_library');
        },
        onPickupTimeSelected: (dynamic pickupDate) {
          flowData.selectedPickupDate = pickupDate;
          _gotoStep('select_number_of_items');
        },
        artBoard: 'anim_pickup-books',
      ),
      'select_number_of_items': SelectNumberOfItemsToReturn(
        onItemNumberSelected: (int count) {
          flowData.itemCount = count;
          _gotoStep('return_instructions');
        },
        onPrevious: () {
          _gotoStep('select_pickup_evening');
        },
      ),
      'return_instructions': LibraryReturnInstructions(
        onSkip: checkIfUserNameExists,
      ),
      'update_user_name': PropertyEditor(
        capitalization: TextCapitalization.none,
        titleText: "Enter your name",
        placeholder: 'Name',
        textType: TextInputType.text,
        headerImg: 'assets/images/ill_home_send.png',
        defaultValue: '',
        onBack: () {
          _gotoStep('select_number_of_items');
        },
        onValue: updateProfileName,
        homeUrl: 'dashboard/home',
      ),
      'library_order_summary': LibraryOrderSummary(
        onDeleteOrder: handleDeleteOrder,
        onCompleteOrder: handleCompleteOrder,
        onPrevious: () {
          _gotoStep('select_number_of_items');
        },
        onSelectBookCount: () {
          _gotoStep('select_number_of_items');
        },
        onSelectLibrary: () {
          _gotoStep('select_library');
        },
        flowData: flowData,
      ),
    };

    if (steps[step] != null) {
      activeStep = steps[step];
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return activeStep ?? EmptyWidget();
  }

  @override
  void dispose() {
    disposeControllerInstance();
    super.dispose();
  }
}
