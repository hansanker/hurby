import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/dashboard-screens/home-screen/widgets/header-thumbnail.dart';
import 'package:hurby/services/data.service.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/custom-list-tile-config.dart';
import 'package:hurby/shared/widgets/clickable-container.dart';
import 'package:hurby/shared/widgets/custom-list-tile/custom-list-tile.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

final _buttonShape = RoundedRectangleBorder(
  borderRadius: AppStyles.buttonRadiusAll,
);

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State<HomeScreen> {
  ProfileService _profileService = serviceInjector.profileService;
  DataService _dataService = serviceInjector.dataService;
  SpinnerService _spinnerService = serviceInjector.spinnerService;
  OrderService _orderService = serviceInjector.orderService;

  dynamic _mainMenuTexts = {};

  void initState() {
    super.initState();

    // listens for when the initial build is done
    if (SchedulerBinding.instance.schedulerPhase ==
        SchedulerPhase.persistentCallbacks) {
      SchedulerBinding.instance.addPostFrameCallback((_) async {
        // get the getMainMenuTexts
        _mainMenuTexts = await _profileService.getMainMenuTexts();
        setState(() {});
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: Container(
        alignment: Alignment.centerLeft,
        child: ClickableContainer(
          padding: EdgeInsets.symmetric(
            vertical: Tape.dx(20),
            horizontal: Tape.dx(10),
          ),
          onPressed: () {
            // Goto profile view
            Navigator.of(context).pushReplacementNamed(
              'dashboard/profile',
            );
          },
          child: Icon(
            Icons.settings,
            size: Tape.dx(30),
            color: Color.fromRGBO(104, 217, 232, 1),
          ),
        ),
      ),
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            height: Tape.dx(168),
            child: _logo(),
          ),
          SizedBox(
            height: Tape.dx(12),
          ),
          Container(
            alignment: Alignment.center,
            height: Tape.dx(128),
            child: _scrollableHeaderList(),
          ),
          SizedBox(height: Tape.dx(4)),
          Container(
            padding: EdgeInsets.all(
              Tape.dx(12),
            ),
            child: Column(
              children: _getMenuItems(),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _getMenuItems() {
    List<Widget> menuItems = [];
    String changeDateText = _mainMenuTexts['changeDateText'] ?? '';
    String trackTraceText = _mainMenuTexts['trackTraceText'] ?? '';
    String finalizeOrdersText = _mainMenuTexts['finalizeOrdersText'] ?? '';
    Widget vSpace = SizedBox(height: Tape.dx(10));

    // add tile for connect your friends
    menuItems.add(
      CustomListTile(
        config: CustomListTileConfig(
          tileHeight: Tape.dx(60),
          leadingImg: 'assets/images/icon_connected.png',
          title: 'Connect your friends',
          titleColor: Theme.of(context).primaryColor,
          // omit the subtitle
          body: [
            '3 Months free local shipments',
          ],
          bodyColor: BMColors.bmDarkblue,
          onClick: () {},
          // omit trailing
          showBorderRadiusTop: true,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
          isLink: true,
        ),
      ),
    );

    // add a space
    menuItems.add(vSpace);

    if (changeDateText.isNotEmpty) {
      menuItems.add(
        CustomListTile(
          config: CustomListTileConfig(
            tileHeight: Tape.dx(60),
            leadingImg: 'assets/images/icon_e_truck_solid.png',
            title: 'Planned pickup\'s',
            titleColor: Theme.of(context).primaryColor,
            // omit the subtitle
            body: [changeDateText],
            bodyColor: BMColors.bmDarkblue,
            onClick: () {},
            trailingWidget: Text(
              '!',
              style: TextStyle(
                fontSize: Tape.dx(20),
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
            trailingAlignment: Alignment.center,
            trailingBackgroundColor: Colors.green,
            showBorderRadiusTop: true,
            showBorderRadiusBottom: true,
            showBottomBorder: false,
            isLink: true,
          ),
        ),
      );

      // add a space
      menuItems.add(vSpace);
    }

    // always show track and trace even if the trackTraceText is empt
    menuItems.add(
      CustomListTile(
        config: CustomListTileConfig(
          tileHeight: Tape.dx(60),
          leadingImg: 'assets/images/icon_trace_solid.png',
          title: 'Track & Trace',
          titleColor: Theme.of(context).primaryColor,
          // omit the subtitle
          body: [trackTraceText.isNotEmpty ? trackTraceText : '...loading'],
          bodyColor: BMColors.bmDarkblue,
          onClick: () {},
          // omit trailing
          showBorderRadiusTop: true,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
          isLink: true,
        ),
      ),
    );

    // add a space
    menuItems.add(vSpace);

    if (finalizeOrdersText.isNotEmpty) {
      menuItems.add(
        CustomListTile(
          config: CustomListTileConfig(
            tileHeight: Tape.dx(60),
            leadingImg: 'assets/images/icon_parcel_solid.png',
            title: 'Finalize your order',
            titleColor: Theme.of(context).accentColor,
            // omit the subtitle
            body: [finalizeOrdersText],
            bodyColor: BMColors.bmDarkblue,
            onClick: () async {
              // here we update the list of parcel pricing
              if (_dataService.getParcelTypes().length == 0) {
                _spinnerService.toggleSpinner(true);

                // update the shipment rates
                _dataService.updateParcelRates(
                  await _orderService.getShipmentRateSummary(),
                );

                _spinnerService.toggleSpinner(false);
              }

              // Here we go to the payment flow directly
              Navigator.of(context).pushReplacementNamed(
                'dashboard/payment_flow',
              );
            },
            trailingWidget: Text(
              '!',
              style: TextStyle(
                fontSize: Tape.dx(20),
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
            trailingAlignment: Alignment.center,
            trailingBackgroundColor: Color.fromRGBO(255, 154, 0, 1),
            showBorderRadiusTop: true,
            showBorderRadiusBottom: true,
            showBottomBorder: false,
            isLink: true,
          ),
        ),
      );
    }

    return menuItems;
  }

  Widget _scrollableHeaderList() {
    return ListView(
      // This next line does the trick.
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        SizedBox(width: Tape.dx(4)),
        HeaderThumbnail(
          image: AssetImage('assets/images/news_tell_a_friend.png'),
        ),
        HeaderThumbnail(
          image: AssetImage('assets/images/news_free_local.png'),
          footerText: 'Free local delivery',
        ),
        HeaderThumbnail(
          image: AssetImage('assets/images/news_marktplaats.png'),
          footerText: 'Marktplaats pickup',
        ),
        SizedBox(width: Tape.dx(4)),
      ],
    );
  }

  Widget _footerContainer() {
    return PaddedContainer(
      contentHeight: Tape.dx(72),
      content: RaisedButton(
        onPressed: () {
          // Goto order send and return flow screen
          Navigator.of(context).pushReplacementNamed(
            'dashboard/parcel_type_selection',
          );
        },
        color: Theme.of(context).primaryColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image(
              width: Tape.dx(56),
              height: Tape.dx(44),
              image: AssetImage(
                'assets/images/icon_parcel_solid_blue.png',
              ),
              fit: BoxFit.contain,
            ),
            SizedBox(width: Tape.dx(16)),
            Text(
              'add a parcel',
              style: TextStyle(
                color: Colors.white,
                fontSize: Tape.dx(26.0),
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
        shape: _buttonShape,
      ),
    );
  }

  Widget _logo() {
    return Image(
      image: AssetImage('assets/images/dashboard_mus.png'),
      fit: BoxFit.contain,
    );
  }
}
