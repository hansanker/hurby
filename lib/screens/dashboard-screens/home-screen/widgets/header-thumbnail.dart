import 'package:flutter/material.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class HeaderThumbnail extends StatelessWidget {
  final AssetImage image;
  final String footerText;

  HeaderThumbnail({
    @required this.image,
    this.footerText,
  });

  @override
  Widget build(BuildContext context) {
    Widget innerContent;

    if (footerText == null) {
      innerContent = ClipRRect(
        borderRadius: AppStyles.buttonRadiusAll,
        child: Image(
          width: Tape.dx(156),
          image: image,
          fit: BoxFit.fitWidth,
        ),
      );
    } else {
      innerContent = Stack(
        children: <Widget>[
          ClipRRect(
            borderRadius: AppStyles.buttonRadiusAll,
            child: Image(
              width: Tape.dx(156),
              height: Tape.dx(128),
              image: image,
              fit: BoxFit.fitWidth,
            ),
          ),
          if (footerText != null)
            Positioned(
              width: Tape.dx(156),
              bottom: 0,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: AppStyles.buttonRadiusBottom,
                ),
                padding: EdgeInsets.only(
                  top: Tape.dx(4),
                  left: Tape.dx(8),
                  right: Tape.dx(8),
                  bottom: Tape.dx(4),
                ),
                child: Text(
                  footerText,
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: Tape.dx(12),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
        ],
      );
    }

    return Card(
      child: Container(
        child: innerContent,
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.white,
            width: Tape.dx(3),
          ),
          borderRadius: AppStyles.buttonRadiusAll,
        ),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }
}
