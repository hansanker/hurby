import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/data.service.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/choose-option-popup/choose-option-popup.dart';
import 'package:hurby/styles.dart';

// here we define the steps in the webshop return flow
class OrderSendReturnSteps {
  static const int how_it_works = 0;
  static const int select_webshop = 1;
  static const int enter_webshop_name = 2;
  static const int shop_allow_free_return = 3;
  static const int select_pickup_evening = 4;
  static const int select_parcel_longest_side = 5;
  static const int parcel_weights_20grams = 6;
  static const int return_instructions = 7;
  static const int return_order_summary = 8;
}

mixin OrderSendReturnFlowScreenController {
  final PopupService _popupService = serviceInjector.popupService;
  final SpinnerService _spinnerService = serviceInjector.spinnerService;
  final OrderService _orderService = serviceInjector.orderService;
  final ProfileService _profileService = serviceInjector.profileService;
  final DataService _dataService = serviceInjector.dataService;
  final WebShopReturnFlowData flowData = new WebShopReturnFlowData();

  Function _renderStepsCallback;
  Function _setStateCallback;
  BuildContext _context;

  int step = OrderSendReturnSteps.how_it_works;
  List<Widget> webshopReturnFlowSteps;

  void initControllerInstance(
    BuildContext context,
    Function renderStepsFn,
    Function setStateFn,
    String orderId,
  ) async {
    _renderStepsCallback = renderStepsFn;
    _setStateCallback = setStateFn;
    _context = context;

    // here we update the flowData
    await _updateFlowData(orderId);

    // here we render the steps
    _renderStepsCallback();
    _setStateCallback(() {});
  }

  Future<void> _updateFlowData(String orderId) async {
    _spinnerService.toggleSpinner(true);

    List<dynamic> asyncDatasets = await Future.wait([
      _orderService.getRetailers(),
      _orderService.getOtherWebshopRate(),
      _profileService.getCurrentUserProfile(),
      _orderService.getPackageSizeSurcharge(),
      _orderService.getPickupInfo(),
      _orderService.getOrderCountPerOrderType(),
    ]);

    // here we update the flowData with values
    List<dynamic> retailers = asyncDatasets[0];
    dynamic otherWebshopFee = asyncDatasets[1];

    flowData.userProfile = asyncDatasets[2];
    flowData.packageSizeSurchargeRawData = asyncDatasets[3];
    flowData.pickupInfoRawData = asyncDatasets[4];

    flowData.otherWebshop = new WebshopData(
      did: '',
      companyCode: 'other_web_shop',
      companyName: 'other webshop',
      price: otherWebshopFee['priceInclVat'],
    );

    // Get the getOrderCountPerOrderType then go to the how it works page
    dynamic orderCountRes = asyncDatasets[5];
    flowData.allCount = orderCountRes['allCount'];

    _setRetailers(retailers);
    _updatePackageLengthRates();

    if (orderId != null) {
      _refreshOrderFromId(orderId);
    }

    _spinnerService.toggleSpinner(false);
    return;
  }

  void _updatePackageLengthRates() {
    for (ParcelSizeData size in flowData.parcelSides) {
      switch (size.id) {
        case '50_to_100':
          {
            dynamic surcharge = flowData.packageSizeSurchargeRawData['LARGE'];
            size.price = surcharge['priceInclVat'] ?? 0.0;
            break;
          }

        case 'less_than_50':
          {
            dynamic surcharge = flowData.packageSizeSurchargeRawData['SMALL'];
            size.price = surcharge['priceInclVat'] ?? 0.0;
            break;
          }
      }
    }
  }

  void _setRetailers(List<dynamic> retailersData) {
    String _baseUrl = 'http://149.210.228.159:8080/Buurtmus';

    for (dynamic shop in retailersData) {
      String companyCode = shop['companyCode'];

      flowData.webshopList.add(
        new WebshopData(
          did: '${shop['personDid']}',
          companyCode: shop['companyCode'],
          companyName: shop['companyCode'],
          companyImage:
              '$_baseUrl/resources/core-customerApp/images/retailer-$companyCode.png',
          price: shop['price'] != null ? shop['price']['priceInclVat'] : 0.0,
        ),
      );
    }
  }

  void handleWebShopSelection(WebshopData webShop) async {
    flowData.selectedWebShop = webShop;

    if (flowData.selectedWebShop.companyCode == 'other_web_shop') {
      gotoStep(OrderSendReturnSteps.enter_webshop_name);
    } else {
      gotoStep(OrderSendReturnSteps.select_pickup_evening);
    }
  }

  void handleAllowFreeReturnResponse(bool status) async {
    if (status) {
      gotoStep(OrderSendReturnSteps.select_pickup_evening);
    } else {
      dynamic res = await _popupService.showCustomDialog(
        context: _context,
        content: ChooseOptionPopup(
          messsageText: 'Choose another send option',
          image: 'assets/images/order_delete.png',
        ),
      );

      if (res) {
        Navigator.of(_context).pushReplacementNamed(
          'dashboard/home',
        );
      }
    }
  }

  void handlePackageLengthSelection(ParcelSizeData side) async {
    flowData.selectedPackageLength = side;

    switch (side.id) {
      case 'longer_than_100':
        {
          _serviceDoesNotSupportParcel();
          break;
        }

      case '50_to_100':
        {
          gotoStep(OrderSendReturnSteps.parcel_weights_20grams);
          break;
        }

      case 'less_than_50':
        {
          gotoStep(OrderSendReturnSteps.return_instructions);
          break;
        }
    }
  }

  void handleParcelWeightSelection(bool weightIsOk) {
    if (weightIsOk) {
      gotoStep(OrderSendReturnSteps.return_instructions);
    } else {
      _serviceDoesNotSupportParcel();
    }
  }

  void handleDeleteOrder() async {
    dynamic res = await _popupService.showCustomDialog(
      context: _context,
      content: ChooseOptionPopup(
        messsageText: 'Are you sure to delete this order?',
        confirmText: 'yes, delete',
        cancelText: 'back',
        image: 'assets/images/order_delete.png',
        confirmButtonColor: BMColors.bmLightBlue,
      ),
    );

    if (res) {
      if (flowData.activeOrder['did'] != null) {
        await _orderService.deleteOrder(flowData.activeOrder['did']);
      }

      Navigator.of(_context).pushReplacementNamed(
        'dashboard/home',
      );
    }
  }

  void handleCompleteOrder() async {
    if (!_spinnerService.isLoading) {
      _spinnerService.toggleSpinner(true);

      flowData.activeOrder['did'] = flowData.activeOrder['did'] ?? '';
      flowData.activeOrder['orderFlow'] = 'STD_RETURN_FLOW';
      flowData.activeOrder['actionDate'] = flowData.selectedPickupDate['date'];
      flowData.activeOrder['number'] =
          flowData.pickupInfoRawData['orderNumber'];
      flowData.activeOrder['receiver'] = {
        "did": flowData.selectedWebShop.did,
        "companyName": flowData.selectedWebShop.companyCode,
      };

      // set the shipment size
      bool isSmall = flowData.selectedPackageLength.id == 'less_than_50';
      flowData.activeOrder['shipmentSizeCode'] = isSmall ? 'SMALL' : 'LARGE';

      await _orderService.createOrder(flowData.activeOrder);

      // Goto order overview
      Navigator.of(_context).pushReplacementNamed(
        'dashboard/payment_flow',
      );

      _spinnerService.toggleSpinner(false);
    }
  }

  void _refreshOrderFromId(String orderId) async {
    dynamic orderInfo = await _orderService.getOrderDetails(orderId);

    // here we set the active order to just the order did
    flowData.activeOrder = {
      'did': orderInfo['did'],
    };

    // set the selected pickup date
    flowData.selectedPickupDate = {
      'date': orderInfo['actionDate'],
      'dayName': orderInfo['actionDay']
    };

    // we set the selected webshop
    String shopId = '${orderInfo['receiver']['did']}';
    flowData.selectedWebShop = flowData.webshopList.firstWhere(
      (shop) {
        return shop.did == shopId;
      },
      orElse: () {
        return new WebshopData(
          did: '${orderInfo['receiver']['did']}',
          companyCode: orderInfo['receiver']['companyName'],
          companyName: orderInfo['receiver']['companyName'],
          price: flowData.otherWebshop.price,
        );
      },
    );

    // here we get the selected parcelType from the dataService
    flowData.selectedParcelType = _dataService.getParcelTypes().firstWhere(
      (parcelType) {
        return parcelType.id == 'STD_RETURN_FLOW';
      },
      orElse: () {
        return null;
      },
    );

    // here we update the selected package length
    String packageSizeId =
        orderInfo['shipmentSizeCode'] == 'SMALL' ? 'less_than_50' : '50_to_100';

    flowData.selectedPackageLength = flowData.parcelSides.firstWhere(
      (side) {
        return side.id == packageSizeId;
      },
      orElse: () {
        return null;
      },
    );

    // here we update the order number in the pickupInfoRawData
    flowData.pickupInfoRawData['orderNumber'] = orderInfo['number'];

    // After updating the flowData we navigate to the summary page
    gotoStep(OrderSendReturnSteps.return_order_summary);
  }

  void _serviceDoesNotSupportParcel() async {
    dynamic res = await _popupService.showCustomDialog(
      context: _context,
      content: ChooseOptionPopup(
        messsageText: 'Our service does not support this size of parcel',
        image: 'assets/images/order_delete.png',
      ),
    );

    if (res) {
      Navigator.of(_context).pushReplacementNamed(
        'dashboard/home',
      );
    }
  }

  void gotoStep(int webshopReturnFlowStep) {
    // here we set the step
    step = webshopReturnFlowStep;
    _renderStepsCallback();
    _setStateCallback(() {});
  }

  // destroy all subscriptions
  void disposeControllerInstance() {}
}
