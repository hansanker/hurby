import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/order-send-return.controller.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/steps/root.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/property-editor.dart';

class OrderSendReturnFlowScreen extends StatefulWidget {
  final String orderId;

  OrderSendReturnFlowScreen({
    @required this.orderId,
  });

  @override
  _OrderSendReturnFlowScreenState createState() {
    return _OrderSendReturnFlowScreenState();
  }
}

class _OrderSendReturnFlowScreenState extends State<OrderSendReturnFlowScreen>
    with OrderSendReturnFlowScreenController {
  @override
  void initState() {
    super.initState();

    // init the controller instance
    initControllerInstance(
      context,
      _renderSteps,
      (void Function() fn) => setState(fn),
      widget.orderId,
    );
  }

  // STEPS FOR WEBSHOP ORDER SEND AND RETURN FLOW ====>>>
  void _renderSteps() {
    webshopReturnFlowSteps = [
      // shows the user a short animated video of how it works
      HowWebshopReturnWorks(
        visitCount: flowData.allCount,
        onSkip: () {
          gotoStep(OrderSendReturnSteps.select_webshop);
        },
      ),

      // here we show a list of webshops that the user can select from
      SelectWebshopStep(
        onPrevious: () {
          // Goto order send and return flow screen
          Navigator.of(context).pushReplacementNamed(
            'dashboard/parcel_type_selection',
          );
        },
        webShopList: flowData.webshopList,
        otherWebshop: flowData.otherWebshop,
        onWebShopSelected: handleWebShopSelection,
      ),

      // here we show an editor for the user to enter the webshop name
      PropertyEditor(
        capitalization: TextCapitalization.none,
        titleText: "What's the name of the webshop?",
        placeholder: 'webshop',
        textType: TextInputType.text,
        headerImg: 'assets/images/ill_home_send.png',
        defaultValue: '',
        onBack: () {
          gotoStep(OrderSendReturnSteps.select_webshop);
        },
        onValue: (String shopName) async {
          // here we update the other webshop name
          flowData.selectedWebShop.companyCode = shopName;

          // then ask the user if webshop allows return for free
          gotoStep(OrderSendReturnSteps.shop_allow_free_return);
        },
        homeUrl: 'dashboard/home',
      ),

      // Ask the user if the webshop allows free returns
      AllowFreeReturn(
        onPrevious: () {
          gotoStep(OrderSendReturnSteps.select_webshop);
        },
        onOptionSelected: handleAllowFreeReturnResponse,
      ),

      // select pickup evening
      SelectPickupEvening(
        pickupInfo: flowData.pickupInfoRawData,
        onPrevious: () {
          gotoStep(OrderSendReturnSteps.select_webshop);
        },
        onPickupTimeSelected: (dynamic pickupDate) {
          flowData.selectedPickupDate = pickupDate;
          gotoStep(OrderSendReturnSteps.select_parcel_longest_side);
        },
        artBoard: 'anim_pickup-etruck',
      ),

      // select parcel longest side
      SelectLongestSide(
        parcelSides: flowData.parcelSides,
        onLengthSelected: handlePackageLengthSelection,
        onPrevious: () {
          gotoStep(OrderSendReturnSteps.select_pickup_evening);
        },
      ),

      // does the parcel weights less than 20 grams
      Weights20gramsOrLess(
        onPrevious: () {
          gotoStep(OrderSendReturnSteps.select_parcel_longest_side);
        },
        onOptionSelected: handleParcelWeightSelection,
      ),

      // show return instructions
      ReturnInstructions(
        onSkip: () {
          gotoStep(OrderSendReturnSteps.return_order_summary);
        },
      ),

      // Here we show the webshop return order summary page
      OrderSummary(
        onDeleteOrder: handleDeleteOrder,
        onCompleteOrder: handleCompleteOrder,
        onSelectPickupEvening: () {
          gotoStep(OrderSendReturnSteps.select_pickup_evening);
        },
        onSelectParcelSize: () {
          gotoStep(OrderSendReturnSteps.select_parcel_longest_side);
        },
        onSelectWebshop: () {
          gotoStep(OrderSendReturnSteps.select_webshop);
        },
        flowData: flowData,
        onPrevious: () {},
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return webshopReturnFlowSteps != null
        ? webshopReturnFlowSteps[step]
        : EmptyWidget();
  }

  @override
  void dispose() {
    disposeControllerInstance();
    super.dispose();
  }
}
