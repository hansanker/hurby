import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/steps/order-summary/order-summary.style.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/custom-list-tile-config.dart';
import 'package:hurby/shared/models/price-breakdown.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/custom-list-tile/custom-list-tile.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/price-tag.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class OrderSummary extends StatefulWidget {
  final WebShopReturnFlowData flowData;
  final Function onDeleteOrder;
  final Function onCompleteOrder;
  final Function onSelectPickupEvening;
  final Function onSelectParcelSize;
  final Function onSelectWebshop;
  final Function onPrevious;

  OrderSummary({
    @required this.onDeleteOrder,
    @required this.onCompleteOrder,
    @required this.onPrevious,
    this.onSelectParcelSize,
    this.onSelectPickupEvening,
    this.onSelectWebshop,
    @required this.flowData,
  });

  @override
  _OrderSummaryState createState() {
    return _OrderSummaryState();
  }
}

class _OrderSummaryState extends State<OrderSummary> with OrderSummaryStyle {
  final OrderService _orderService = serviceInjector.orderService;

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        title: 'Webshop return ',
        homeUrl: 'dashboard/home',
        infoMessage: EmptyWidget(),
        onPrevious: widget.onPrevious,
      ),
      clearFixBy: Tape.dx(100),
      mainContent: _bodyContent(context),
      footerContent: _footerContent(),
    );
  }

  Widget _bodyContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: Tape.dx(12),
        right: Tape.dx(12),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _pickupInfo(),
          SizedBox(height: Tape.dx(8)),
          _parcelSizeInfo(),
          SizedBox(height: Tape.dx(8)),
          _shopInfo(),
          SizedBox(height: Tape.dx(16)),
          _deleteOrder(),
        ],
      ),
    );
  }

  Widget _pickupInfo() {
    String orderNumber = widget.flowData.pickupInfoRawData['orderNumber'];
    String orderNumShort = 'BM' + orderNumber.substring(orderNumber.length - 4);

    return CustomListTile(
      items: [
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          leadingImg: 'assets/images/icon_send_green.png',
          title: 'Pickup',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          // omit the subtitle
          body: [
            widget
                .flowData.userProfile.personDTO.physicalAddressDTO.addressLine1,
          ],
          bodyColor: Color.fromRGBO(22, 65, 148, 0.5),
          onClick: () {},
          // omit trailing
          showBorderRadiusTop: true,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          leadingImg: 'assets/images/icon_trace_solid.png',
          title: widget.flowData.selectedPickupDate['dayName'],
          titleColor: Theme.of(context).primaryColor,
          // omit the subtitle
          body: ['19 - 21 hr'],
          bodyColor: Theme.of(context).primaryColor,
          onClick: () {},
          trailingWidget: Container(
            padding: EdgeInsets.all(Tape.dx(5)),
            child: Text(
              orderNumShort,
              style: TextStyle(
                fontSize: Tape.dx(14),
                color: BMColors.bmGrey,
              ),
            ),
          ),
          trailingWidth: Tape.dx(88),
          trailingAlignment: Alignment.bottomRight,
          showBorderRadiusTop: false,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
        ),
      ],
    );
  }

  Widget _parcelSizeInfo() {
    ParcelSizeData packageLength = widget.flowData.selectedPackageLength;
    bool isSmall = packageLength.id == 'less_than_50';

    PriceBreakdown priceComponents = _orderService.getPriceComponents(
      packageLength.price,
    );

    return CustomListTile(
      config: CustomListTileConfig(
        tileHeight: Tape.dx(60),
        title: isSmall ? 'Parcel small' : 'Parcel large',
        titleColor: Color.fromRGBO(22, 65, 148, 1),
        // omit the subtitle
        body: [packageLength.title],
        bodyColor: Theme.of(context).primaryColor,
        onClick: widget.onSelectParcelSize,
        trailingWidget: Container(
          padding: EdgeInsets.all(Tape.dx(5)),
          child: PriceTag(
            unitPrice: priceComponents.unitPrice,
            fractionalPrice: priceComponents.fractionalPrice,
          ),
        ),
        trailingWidth: Tape.dx(100),
        trailingAlignment: Alignment.topRight,
        showBorderRadiusTop: true,
        showBorderRadiusBottom: true,
        showBottomBorder: false,
      ),
    );
  }

  Widget _shopInfo() {
    WebshopData webshop = widget.flowData.selectedWebShop;

    PriceBreakdown priceComponents =
        _orderService.getPriceComponents(webshop.price);

    return CustomListTile(
      config: CustomListTileConfig(
        tileHeight: Tape.dx(60),
        title: 'Webshop return',
        titleColor: Color.fromRGBO(22, 65, 148, 1),
        // omit the subtitle
        body: [webshop.companyCode],
        bodyColor: Theme.of(context).primaryColor,
        onClick: widget.onSelectWebshop,
        trailingWidget: Container(
          padding: EdgeInsets.all(Tape.dx(5)),
          child: PriceTag(
            unitPrice: priceComponents.unitPrice,
            fractionalPrice: priceComponents.fractionalPrice,
          ),
        ),
        trailingWidth: Tape.dx(100),
        trailingAlignment: Alignment.topRight,
        showBorderRadiusTop: true,
        showBorderRadiusBottom: true,
        showBottomBorder: false,
      ),
    );
  }

  Widget _deleteOrder() {
    return CustomListTile(
      config: CustomListTileConfig(
        leadingImg: 'assets/images/icon_delete.png',
        tileHeight: Tape.dx(60),
        title: 'Delete',
        titleColor: Color.fromRGBO(22, 65, 148, 1),
        // omit the subtitle
        body: ['Delete this order'],
        bodyColor: Theme.of(context).primaryColor,
        onClick: widget.onDeleteOrder,
        showBorderRadiusTop: true,
        showBorderRadiusBottom: true,
        showBottomBorder: false,
      ),
    );
  }

  Widget _footerContent() {
    return PaddedContainer(
      contentHeight: Tape.dx(124),
      showShadow: true,
      content: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: Tape.dx(10),
              right: Tape.dx(10),
            ),
            child: _totalPrice(),
          ),
          RaisedButton(
            onPressed: widget.onCompleteOrder,
            padding: EdgeInsets.all(
              Tape.dx(20),
            ),
            color: Theme.of(context).primaryColor,
            child: Text(
              'complete',
              style: completeButton,
            ),
            shape: new RoundedRectangleBorder(
              borderRadius: AppStyles.buttonRadiusAll,
            ),
          ),
        ],
      ),
    );
  }

  Widget _totalPrice() {
    ParcelSizeData packageLength = widget.flowData.selectedPackageLength;
    WebshopData webshop = widget.flowData.selectedWebShop;

    double totalPrice = packageLength.price + webshop.price;
    PriceBreakdown priceComponents =
        _orderService.getPriceComponents(totalPrice);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Total', style: totalText),
            Expanded(child: EmptyWidget()),
            PriceTag(
              unitPrice: '${priceComponents.unitPrice}',
              fractionalPrice: priceComponents.fractionalPrice,
              color: BMColors.bmDarkblue,
            )
          ],
        ),
        Text('Tax included', style: totalSubText),
        SizedBox(
          height: Tape.dx(8),
        ),
      ],
    );
  }
}
