import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/steps/allows-free-return/allows-free-return.styles.dart';
import 'package:hurby/shared/widgets/binary-option.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class AllowFreeReturn extends StatefulWidget {
  final Function onPrevious;
  final Function(bool) onOptionSelected;

  AllowFreeReturn({
    @required this.onPrevious,
    @required this.onOptionSelected,
  });

  @override
  _AllowFreeReturnState createState() {
    return _AllowFreeReturnState();
  }
}

class _AllowFreeReturnState extends State<AllowFreeReturn>
    with TickerProviderStateMixin, AllowsFreeReturnStyles {
  AnimationController _animationController;
  Animation<double> _entryTransitionLeft;
  Animation<double> _entryOpacity;
  StreamSubscription _sub;

  @override
  void initState() {
    super.initState();
    _setupWidget();
  }

  void _setupWidget() async {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);

    // here we describe the animations for logo entering the screen
    _entryTransitionLeft = Tween<double>(
      begin: Tape.dx(50.0),
      end: 0.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    // here we describe the opacity of that logo entering the screen
    _entryOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    // here we play the animation
    await _animationController.forward();

    // we listen to the keyboard height changes
    _sub = Tape.keyboardHeight$.listen((double _) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        infoMessage: EmptyWidget(),
        homeUrl: 'dashboard/home',
        onPrevious: widget.onPrevious,
      ),
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Column(
      children: <Widget>[
        SizedBox(height: Tape.dx(50)),
        Container(
          alignment: Alignment.topCenter,
          height: Tape.dx(230),
          child: AnimatedBuilder(
            builder: _logo,
            animation: _animationController,
          ),
        ),
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          height: Tape.dx(100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Does the webshop allows you', style: textStyle),
              Text('to return your parcel', style: textStyle),
              Text('for free?', style: textStyle),
            ],
          ),
        ),
        SizedBox(
          height: Tape.dx(30),
        ),
        PaddedContainer(
          contentHeight: Tape.dx(116),
          content: BinaryOption(
            firstOption: 'yes',
            secondOption: 'no',
            onClickedFirst: () {
              widget.onOptionSelected(true);
            },
            onClickedSecond: () {
              widget.onOptionSelected(false);
            },
          ),
        ),
      ],
    );
  }

  Widget _logo(_, __) {
    return Opacity(
      opacity: _entryOpacity.value,
      child: Container(
        transform: Matrix4.translationValues(
          _entryTransitionLeft.value,
          0.0,
          0.0,
        ),
        child: Image(
          image: AssetImage('assets/images/info_parcel_return.png'),
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    _sub.cancel();
    super.dispose();
  }
}
