import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/steps/select-webshop/select-webshop.styles.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/button-pricetag-data.dart';
import 'package:hurby/shared/models/price-breakdown.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/button-with-pricetag/button-with-pricetag.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class SelectWebshopStep extends StatefulWidget {
  final Function onPrevious;
  final List<WebshopData> webShopList;
  final WebshopData otherWebshop;
  final Function(WebshopData webShop) onWebShopSelected;

  SelectWebshopStep({
    this.onPrevious,
    @required this.onWebShopSelected,
    @required this.webShopList,
    @required this.otherWebshop,
  });

  @override
  _SelectWebshopStepState createState() {
    return new _SelectWebshopStepState();
  }
}

class _SelectWebshopStepState extends State<SelectWebshopStep>
    with SelectWebshopStyles {
  OrderService _orderService = serviceInjector.orderService;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        title: 'Webshop selection',
        onPrevious: widget.onPrevious,
        infoMessage: EmptyWidget(),
        homeUrl: 'dashboard/home',
      ),
      clearFixBy: Tape.dx(50),
      mainContent: _mainContent(),
      footerContent: _footerContainer(context),
    );
  }

  Widget _mainContent() {
    return Container(
      padding: EdgeInsets.only(
        top: Tape.dx(24),
        left: Tape.dx(12),
        right: Tape.dx(12),
        bottom: Tape.dx(12),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: _getShopList(),
      ),
    );
  }

  List<Widget> _getShopList() {
    List<Widget> results = [];

    for (int i = 0; i < widget.webShopList.length; i++) {
      WebshopData shop = widget.webShopList[i];
      PriceBreakdown priceComponents =
          _orderService.getPriceComponents(shop.price);

      results.add(
        ButtonWithPriceTag(
          theme: 'light',
          data: new ButtonWithPriceTagData(
            image: Image(
              image: NetworkImage(shop.companyImage),
            ),
            unitPrice: priceComponents.unitPrice,
            fractionalPrice: priceComponents.fractionalPrice,
          ),
          onPressed: () {
            widget.onWebShopSelected(widget.webShopList[i]);
          },
        ),
      );

      // If last item don't add spacing
      if (i < widget.webShopList.length - 1) {
        results.add(SizedBox(
          height: Tape.dx(8),
        ));
      }
    }

    return results;
  }

  Widget _footerContainer(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PaddedContainer(
          contentHeight: Tape.dx(72),
          content: _buttonText(context),
          showShadow: true,
        ),
      ],
    );
  }

  Widget _buttonText(BuildContext context) {
    PriceBreakdown priceComponents =
        _orderService.getPriceComponents(widget.otherWebshop.price);

    return ButtonWithPriceTag(
      data: new ButtonWithPriceTagData(
        id: widget.otherWebshop.did,
        title: widget.otherWebshop.companyName,
        unitPrice: priceComponents.unitPrice,
        fractionalPrice: priceComponents.fractionalPrice,
      ),
      onPressed: () {
        widget.onWebShopSelected(widget.otherWebshop);
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
