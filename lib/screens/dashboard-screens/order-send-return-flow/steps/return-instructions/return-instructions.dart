import 'dart:async';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/steps/return-instructions/return-instructions.style.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class ReturnInstructions extends StatefulWidget {
  final Function onSkip;

  ReturnInstructions({
    this.onSkip,
  });

  @override
  _ReturnInstructionsState createState() {
    return _ReturnInstructionsState();
  }
}

class _ReturnInstructionsState extends State<ReturnInstructions>
    with ReturnInstructionsStyles {
  Timer _delayTimer;

  @override
  void initState() {
    super.initState();

    _delayTimer = Timer(
      Duration(seconds: 13),
      widget.onSkip,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: Container(
        height: Tape.dx(300),
        child: Center(
          child: FlareActor(
            'assets/flare/return_instruction.flr',
            animation: 'Animations',
          ),
        ),
      ),
      footerContent: _footerContainer(context),
    );
  }

  Widget _footerContainer(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PaddedContainer(
          contentHeight: Tape.dx(72),
          content: _skipStep(context),
        ),
      ],
    );
  }

  Widget _skipStep(BuildContext context) {
    return RaisedButton(
      onPressed: widget.onSkip,
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: Theme.of(context).primaryColor,
      child: Text(
        'skip',
        style: TextStyle(
          color: Colors.white,
          fontSize: Tape.dx(26.0),
          fontWeight: FontWeight.w700,
        ),
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _delayTimer.cancel();
  }
}
