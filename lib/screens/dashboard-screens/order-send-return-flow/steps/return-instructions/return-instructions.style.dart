import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hurby/utils/screen.util.dart';

mixin ReturnInstructionsStyles {
  final TextStyle textStyle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );
}
