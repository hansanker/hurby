import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

mixin SelectPickupEveningStyles {
  final textStyle = new TextStyle(
    fontSize: Tape.dx(22),
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );

  final subTextStyle = new TextStyle(
    fontSize: Tape.dx(14),
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );

  final dateTextStyle = new TextStyle(
    fontSize: Tape.dx(24),
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );

  final dateSubTextStyle = new TextStyle(
    fontSize: Tape.dx(16),
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );
}
