import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/steps/select-pickup-evening/select-pickup-evening.styles.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/binary-option.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class SelectPickupEvening extends StatefulWidget {
  final Function onPrevious;
  final Function(dynamic) onPickupTimeSelected;
  final dynamic pickupInfo;
  final String artBoard;

  SelectPickupEvening({
    @required this.pickupInfo,
    @required this.onPrevious,
    @required this.onPickupTimeSelected,
    @required this.artBoard,
  });

  @override
  _SelectPickupEveningState createState() {
    return _SelectPickupEveningState();
  }
}

class _SelectPickupEveningState extends State<SelectPickupEvening>
    with SelectPickupEveningStyles {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        onPrevious: widget.onPrevious,
        homeUrl: 'dashboard/home',
        infoMessage: EmptyWidget(),
      ),
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Column(
      children: <Widget>[
        SizedBox(height: Tape.dx(50)),
        _logo(),
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Select the pickup evening', style: textStyle),
              Text('between 19h - 21h', style: subTextStyle),
            ],
          ),
        ),
        SizedBox(
          height: Tape.dx(20),
        ),
        PaddedContainer(
          contentHeight: Tape.dx(116),
          content: BinaryOption(
            firstOption: _getPickupDate(
              widget.pickupInfo['possibleDates'][0],
            ),
            secondOption: _getPickupDate(
              widget.pickupInfo['possibleDates'][1],
            ),
            onClickedFirst: () {
              widget.onPickupTimeSelected(
                widget.pickupInfo['possibleDates'][0],
              );
            },
            onClickedSecond: () {
              widget.onPickupTimeSelected(
                widget.pickupInfo['possibleDates'][1],
              );
            },
          ),
        ),
      ],
    );
  }

  Widget _getPickupDate(dynamic dateInfo) {
    String dateStr = dateInfo['date'];
    String dayName = dateInfo['dayName'];
    String dayInMonth = dateStr.split('-')[2];
    String dateText = '$dayInMonth ${dateInfo['monthName']}';

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(dayName, style: dateTextStyle),
        SizedBox(height: Tape.dx(5)),
        Text(dateText, style: dateSubTextStyle),
      ],
    );
  }

  Widget _logo() {
    return Container(
      padding: EdgeInsets.all(0),
      child: FlareActor(
        'assets/flare/illustrations.flr',
        artboard: widget.artBoard,
        animation: 'in',
      ),
      height: Tape.dx(300),
    );
  }
}
