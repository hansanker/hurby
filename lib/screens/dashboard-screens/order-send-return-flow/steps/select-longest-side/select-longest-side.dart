import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/steps/select-longest-side/select-longest-side.style.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/button-pricetag-data.dart';
import 'package:hurby/shared/models/price-breakdown.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/button-with-pricetag/button-with-pricetag.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class SelectLongestSide extends StatefulWidget {
  final Function(ParcelSizeData side) onLengthSelected;
  final Function onPrevious;
  final List<ParcelSizeData> parcelSides;

  SelectLongestSide({
    @required this.onLengthSelected,
    @required this.onPrevious,
    @required this.parcelSides,
  });

  @override
  _SelectLongestSideState createState() {
    return new _SelectLongestSideState();
  }
}

class _SelectLongestSideState extends State<SelectLongestSide>
    with SelectLongestSideStyles {
  OrderService _orderService = serviceInjector.orderService;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: _headerContent(),
      mainContent: EmptyWidget(),
      footerContent: _footerContainer(),
    );
  }

  Widget _headerContent() {
    return Column(
      children: <Widget>[
        ToolBar(
          title: '',
          onPrevious: widget.onPrevious,
          infoMessage: EmptyWidget(),
          homeUrl: 'dashboard/home',
        ),
        SizedBox(
          height: Tape.dx(8),
        ),
        Container(
          margin: EdgeInsets.only(
            top: Tape.dx(20),
          ),
          child: Image(
            image: AssetImage('assets/images/parcel_large.png'),
            fit: BoxFit.contain,
            height: Tape.dx(168.0),
          ),
        ),
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          alignment: Alignment.bottomCenter,
          child: Column(
            children: <Widget>[
              Text(
                'The longest side',
                style: textStyle,
                textAlign: TextAlign.center,
              ),
              Text(
                'of the parcel is',
                style: textStyle,
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
        SizedBox(
          height: Tape.dx(22),
        ),
        PaddedContainer(
          contentHeight: Tape.dx(238),
          content: Column(
            children: _getSizeOptions(),
          ),
        ),
      ],
    );
  }

  List<Widget> _getSizeOptions() {
    List<Widget> options = [];

    for (var index = 0; index < widget.parcelSides.length; index++) {
      ParcelSizeData sizeData = widget.parcelSides[index];
      PriceBreakdown priceComponents =
          _orderService.getPriceComponents(sizeData.price);

      options.add(
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            ButtonWithPriceTag(
              data: new ButtonWithPriceTagData(
                id: sizeData.id,
                title: sizeData.title,
                unitPrice: priceComponents.unitPrice,
                fractionalPrice: priceComponents.fractionalPrice,
                isActive: sizeData.isActive,
                priceTitle: sizeData.priceTitle,
              ),
              onPressed: () {
                widget.onLengthSelected(sizeData);
              },
            ),

            // If last item don't add spacing
            if (index < widget.parcelSides.length - 1)
              SizedBox(
                height: Tape.dx(8),
              )
          ],
        ),
      );
    }

    return options;
  }
}
