import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

mixin SelectLongestSideStyles {
  TextStyle textStyle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );
}
