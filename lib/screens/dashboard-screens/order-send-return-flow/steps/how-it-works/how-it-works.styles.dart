import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hurby/utils/screen.util.dart';

class HowWebshopReturnWorksStyles {
  static TextStyle textStyle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );
}
