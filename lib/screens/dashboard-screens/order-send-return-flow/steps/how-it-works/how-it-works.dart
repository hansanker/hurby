import 'dart:async';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class HowWebshopReturnWorks extends StatefulWidget {
  final int visitCount;
  final Function onSkip;

  HowWebshopReturnWorks({
    this.visitCount,
    this.onSkip,
  });

  @override
  _HowWebshopReturnWorksState createState() {
    return _HowWebshopReturnWorksState();
  }
}

class _HowWebshopReturnWorksState extends State<HowWebshopReturnWorks> {
  Timer _delayTimer;

  @override
  void initState() {
    super.initState();

    // here if the visitCount > 0,wait for 3 seconds then skip
    // else  wait for 30 seconds then skip
    // here we run the timeout

    _delayTimer = Timer(
      Duration(seconds: widget.visitCount > 0 ? 3 : 28),
      widget.onSkip,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: Container(
        height: Tape.dx(300),
        child: Center(
          // todo 'assets/flare/return_intro_long.flr' is not working
          child: FlareActor(
            'assets/flare/return_intro_short.flr',
            animation: 'Animations',
          ),
        ),
      ),
      footerContent: _footerContainer(context),
    );
  }

  Widget _footerContainer(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PaddedContainer(
          contentHeight: Tape.dx(72),
          content: _skipStep(context),
        ),
      ],
    );
  }

  Widget _skipStep(BuildContext context) {
    return RaisedButton(
      onPressed: widget.onSkip,
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: Theme.of(context).primaryColor,
      child: Text(
        'skip',
        style: TextStyle(
          color: Colors.white,
          fontSize: Tape.dx(26.0),
          fontWeight: FontWeight.w700,
        ),
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _delayTimer.cancel();
  }
}
