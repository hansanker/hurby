import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

mixin Weights20gramsOrLessStyles {
  final textStyle = new TextStyle(
    fontSize: Tape.dx(22),
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );
}
