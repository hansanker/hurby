import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/data.service.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/button-pricetag-data.dart';
import 'package:hurby/shared/widgets/button-with-pricetag/button-with-pricetag.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class SelectParcelType extends StatefulWidget {
  @override
  _SelectParcelTypeState createState() {
    return new _SelectParcelTypeState();
  }
}

class _SelectParcelTypeState extends State<SelectParcelType> {
  final DataService _dataService = serviceInjector.dataService;
  final OrderService _orderService = serviceInjector.orderService;
  final SpinnerService _spinnerService = serviceInjector.spinnerService;

  List<ButtonWithPriceTagData> _parcelTypes = [];

  @override
  void initState() {
    super.initState();

    _initParcelTypes();
  }

  void _initParcelTypes() async {
    // here we initially get the parcel types
    setState(() {
      _parcelTypes = _dataService.getParcelTypes();
    });

    _spinnerService.toggleSpinner(true);
    _dataService.updateParcelRates(
      await _orderService.getShipmentRateSummary(),
    );
    _spinnerService.toggleSpinner(false);

    // here we update the parcel types list
    setState(() {
      _parcelTypes = _dataService.getParcelTypes();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        homeUrl: 'dashboard/home',
        onPrevious: () {
          // Goto order send and return flow screen
          Navigator.of(context).pushReplacementNamed(
            'dashboard/home',
          );
        },
      ),
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Container(
      height: Tape.dx(300),
      child: Center(
        child: FlareActor(
          'assets/flare/sendmenu.flr',
          animation: 'Animations',
        ),
      ),
    );
  }

  Widget _footerContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PaddedContainer(
          contentHeight: Tape.dx(312),
          content: Column(
            children: <Widget>[
              for (var i = 0; i < _parcelTypes.length; i++)
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    ButtonWithPriceTag(
                      data: _parcelTypes[i],
                      onPressed: () {
                        _selectparcelType(_parcelTypes[i]);
                      },
                    ),

                    // If last item don't add spacing
                    if (i < _parcelTypes.length - 1)
                      SizedBox(
                        height: Tape.dx(8),
                      )
                  ],
                ),
            ],
          ),
        ),
      ],
    );
  }

  void _selectparcelType(ButtonWithPriceTagData parcelType) {
    // Goto order send and return flow screen
    // dashboard/library_return_flow
    switch (parcelType.id) {
      case 'STD_RETURN_FLOW':
        {
          Navigator.of(context).pushReplacementNamed(
            'dashboard/order_send_return_flow',
          );

          break;
        }

      case 'STD_RETURN_LIBRARY_FLOW':
        {
          Navigator.of(context).pushReplacementNamed(
            'dashboard/library_return_flow',
          );

          break;
        }
    }
  }
}
