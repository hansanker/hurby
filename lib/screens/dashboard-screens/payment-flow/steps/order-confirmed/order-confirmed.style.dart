import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

mixin OrderConfirmedStyle {
  final TextStyle bodyText = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );

  final buttonTextStyle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(26),
    fontWeight: FontWeight.w700,
  );
}
