import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/order-confirmed/order-confirmed.style.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class OrderConfirmed extends StatelessWidget with OrderConfirmedStyle {
  final Function onNext;

  OrderConfirmed({
    this.onNext,
  });

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: _headerContent(),
      mainContent: EmptyWidget(),
      footerContent: _footerContainer(),
    );
  }

  Widget _headerContent() {
    return Container(
      height: Tape.dx(300),
      child: Center(
        child: FlareActor(
          'assets/flare/sendmenu.flr',
          animation: 'Animations',
        ),
      ),
    );
  }

  Widget _footerContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Column(
          children: <Widget>[
            Text('Tjilp!', style: bodyText),
            SizedBox(height: Tape.dx(10)),
            Text('your orders have been', style: bodyText),
            Text('successfully confirmed', style: bodyText),
            SizedBox(height: Tape.dx(80)),
          ],
        ),
        PaddedContainer(
          contentHeight: Tape.dx(72),
          content: _continue(),
        ),
      ],
    );
  }

  Widget _continue() {
    return RaisedButton(
      onPressed: onNext,
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: BMColors.bmPrimaryColor,
      child: Text(
        'continue',
        style: buttonTextStyle,
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }
}
