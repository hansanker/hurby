import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

mixin WaitingPaymentStyles {
  final infoText = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );
}
