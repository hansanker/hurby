import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/waiting-payment/waiting-payment.style.dart';
import 'package:hurby/services/payment.service.dart';
import 'package:hurby/shared/widgets/footer-back-button.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';
import 'package:url_launcher/url_launcher.dart';

class WaitingPayment extends StatefulWidget {
  final dynamic selectedBank;
  final String paymentUrl;
  final String txId;

  final Function() onPrevious;
  final Function() onPaymentSuccessful;
  final Function() onRetry;

  WaitingPayment({
    @required this.onPrevious,
    @required this.onPaymentSuccessful,
    @required this.onRetry,
    @required this.selectedBank,
    @required this.paymentUrl,
    @required this.txId,
  });

  @override
  _WaitingPaymentState createState() {
    return _WaitingPaymentState();
  }
}

class _WaitingPaymentState extends State<WaitingPayment>
    with WaitingPaymentStyles {
  // 1016 = success, 1017=fail, 1018=expired, 1019=cancelledƒ
  int _paymentStatus = 0;

  PaymentService _paymentService = serviceInjector.paymentService;

  String _errorMessage = '';

  @override
  void initState() {
    super.initState();

    // here we delay for 3 seconds then we redirect the user
    Future.delayed(Duration(seconds: 3), () {
      _launchURL(widget.paymentUrl);
      _poolPaymentStatus();
    });
  }

  void _poolPaymentStatus() async {
    // here we reset the payment status
    setState(() {
      _paymentStatus = 0;
    });

    dynamic res = await _paymentService.checkPaymentStatus(widget.txId);

    if (res == null) {
      Future.delayed(Duration(seconds: 3), () {
        _poolPaymentStatus();
      });
    } else {
      // When we redirect, this widget is temporarily not mounted but we can still have the state in the tree
      if (mounted) {
        setState(() {
          _errorMessage = res['statusMessage'];
          _paymentStatus = int.parse(res['statusCode']);
        });

        if (_paymentStatus == 1016) {
          widget.onPaymentSuccessful();
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    String bankCode = widget.selectedBank['bankCode'];

    return SimplePageLayout(
      mainContent: Container(
        alignment: Alignment.center,
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(0),
              child: FlareActor(
                'assets/flare/pay_communication.flr',
                animation: 'com-$bankCode',
              ),
              height: Tape.dx(300),
            ),
            Container(
              padding: EdgeInsets.all(0),
              child: FlareActor(
                'assets/flare/pay_communication.flr',
                animation: _paymentStatus == 0 ? 'busy' : 'error',
              ),
              height: Tape.dx(300),
            )
          ],
        ),
      ),
      footerContent: Column(
        children: <Widget>[
          _infoMessage(),
          SizedBox(height: Tape.dx(50)),
          _footerActionButton(),
        ],
      ),
    );
  }

  Widget _infoMessage() {
    if (_paymentStatus == 0) {
      return Column(
        children: <Widget>[
          Text('One moment ...', style: infoText),
          Text('Making a connection', style: infoText),
        ],
      );
    } else {
      return Column(
        children: <Widget>[
          Text(_errorMessage, style: infoText),
        ],
      );
    }
  }

  Widget _footerActionButton() {
    return _paymentStatus == 0 ? _backButton() : _retryButton();
  }

  Widget _retryButton() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PaddedContainer(
          contentHeight: Tape.dx(72),
          content: RaisedButton(
            onPressed: () {
              widget.onRetry();
            },
            padding: EdgeInsets.all(
              Tape.dx(20),
            ),
            color: Theme.of(context).primaryColor,
            child: Text(
              'try again',
              style: TextStyle(
                color: Colors.white,
                fontSize: Tape.dx(26.0),
                fontWeight: FontWeight.w700,
              ),
            ),
            shape: new RoundedRectangleBorder(
              borderRadius: AppStyles.buttonRadiusAll,
            ),
          ),
          showShadow: true,
        ),
      ],
    );
  }

  Widget _backButton() {
    return FooterbackButtonBar(
      onPressed: widget.onPrevious,
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
