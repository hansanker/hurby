import 'package:flutter/material.dart';
import 'package:hurby/info-message-widgets/terms-and-conditions.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/order-overview/order-overview.style.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/custom-list-tile-config.dart';
import 'package:hurby/shared/models/price-breakdown.dart';
import 'package:hurby/shared/widgets/choose-option-popup/choose-option-popup.dart';
import 'package:hurby/shared/widgets/custom-list-tile/custom-list-tile.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class OrderData {
  String id;
  String parcelTyle;
  String shopName;
  String unitPrice;
  String fractionalPrice;
  String orderNumber;
  String orderFlow;

  OrderData({
    this.id,
    this.parcelTyle,
    this.shopName,
    this.unitPrice,
    this.fractionalPrice,
    this.orderNumber,
    this.orderFlow,
  });
}

class OrderOverview extends StatefulWidget {
  final Function onChangePromocode;
  final Function onAddParcel;
  final Function onAddPromoCode;
  final Function(double) onCompleteOrder;
  final Function(OrderData) onOrderSelected;
  final List<dynamic> orders;
  final dynamic promocodePrice;
  final String promocode;
  final bool showPopup;

  OrderOverview({
    this.onChangePromocode,
    this.onAddParcel,
    this.onCompleteOrder,
    this.onAddPromoCode,
    this.onOrderSelected,
    this.orders,
    this.promocodePrice,
    this.promocode,
    this.showPopup,
  });

  @override
  _OrderOverviewState createState() {
    return _OrderOverviewState();
  }
}

class _OrderOverviewState extends State<OrderOverview> with OrderOverviewStyle {
  OrderService _orderService = serviceInjector.orderService;
  PopupService _popupService = serviceInjector.popupService;

  List<OrderData> _orders = [];
  double _totalPrice = 0.0;

  void initState() {
    super.initState();
    _setupOrders();
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        title: 'Order overview',
        homeUrl: 'dashboard/home',
        infoMessage: EmptyWidget(),
        onPrevious: () {},
      ),
      mainContent: _bodyContent(context),
      footerContent: _footerContent(),
    );
  }

  void _setupOrders() async {
    for (dynamic order in widget.orders) {
      String orderNumber = order['number'];
      String orderNumShort =
          'BM' + orderNumber.substring(orderNumber.length - 4);

      double price = double.parse(order['shipmentAmountInclVAT']) +
          double.parse(order['surchargeAmountInclVAT'] ?? '0');

      PriceBreakdown priceSegments = _orderService.getPriceComponents(price);

      _orders.add(
        new OrderData(
          id: '${order['did']}',
          parcelTyle: _getParcelTypeName(order['orderFlow']),
          orderFlow: order['orderFlow'],
          shopName: order['toName'],
          unitPrice: priceSegments.unitPrice,
          fractionalPrice: priceSegments.fractionalPrice,
          orderNumber: orderNumShort,
        ),
      );

      // Here we also track the totol price
      _totalPrice += price;
    }

    // Subtract promocode from the total price
    // if the total price is less than 0, we just set it to 0
    if (widget.promocode.isNotEmpty) {
      _totalPrice -= double.parse(widget.promocodePrice['amountInclVAT']);
      _totalPrice = _totalPrice < 0 ? 0 : _totalPrice;
    }

    setState(() {});

    if (widget.showPopup) {
      // here we delay for a sec and ask the user to add another order
      Future.delayed(Duration(seconds: 1), () {
        _askUserToAddAnotherOrder();
      });
    }
  }

  void _askUserToAddAnotherOrder() async {
    dynamic res = await _popupService.showCustomDialog(
      context: context,
      content: ChooseOptionPopup(
        messsageText: 'Do you want to add another order?',
        confirmText: 'yes',
        cancelText: 'no, continue',
        image: 'assets/images/logo_mus_only.png',
      ),
    );

    if (res) {
      widget.onAddParcel();
    }
  }

  Widget _bodyContent(BuildContext context) {
    return Container(
      padding: bodyContentPadding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(height: Tape.dx(100)),
          ..._getOrderList(),
          SizedBox(height: Tape.dx(16)),
          if (widget.promocode.isNotEmpty) _promoCodeTile(),
          _secondaryActionButtons(),
        ],
      ),
    );
  }

  Widget _promoCodeTile() {
    PriceBreakdown price = _orderService.getPriceComponents(
      double.parse(widget.promocodePrice['amountInclVAT']),
    );

    return Column(
      children: <Widget>[
        CustomListTile(
          config: CustomListTileConfig(
            leadingImg: 'assets/images/icon_promocode.png',
            tileHeight: Tape.dx(60),
            title: 'Promocode',
            titleColor: BMColors.bmDarkblue,
            // omit the subtitle
            body: [widget.promocode],
            bodyColor: Theme.of(context).primaryColor,
            onClick: widget.onChangePromocode,
            trailingWidget: Container(
              padding: EdgeInsets.fromLTRB(
                Tape.dx(5),
                Tape.dx(5),
                Tape.dx(14),
                Tape.dx(5),
              ),
              child: PriceTag(
                unitPrice: price.unitPrice,
                fractionalPrice: price.fractionalPrice,
                negative: true,
                color: Color.fromRGBO(255, 186, 0, 1),
              ),
            ),
            trailingWidth: Tape.dx(100),
            trailingAlignment: Alignment.topRight,
            showBorderRadiusTop: true,
            showBorderRadiusBottom: true,
            showBottomBorder: false,
            isLink: true,
          ),
        ),
        SizedBox(height: Tape.dx(16)),
      ],
    );
  }

  List<Widget> _getOrderList() {
    List<Widget> results = [];

    for (var index = 0; index < _orders.length; index++) {
      OrderData order = _orders[index];

      results.add(
        CustomListTile(
          config: CustomListTileConfig(
            leadingImg: _getParcelTypeImage(order.orderFlow),
            tileHeight: Tape.dx(60),
            title: order.parcelTyle,
            titleColor: Color.fromRGBO(22, 65, 148, 1),
            // omit the subtitle
            body: [order.shopName],
            bodyColor: Theme.of(context).primaryColor,
            onClick: () {
              widget.onOrderSelected(order);
            },
            trailingWidget: Container(
              padding: EdgeInsets.all(Tape.dx(5)),
              child: Column(
                children: <Widget>[
                  PriceTag(
                    unitPrice: order.unitPrice,
                    fractionalPrice: order.fractionalPrice,
                  ),
                  Text(
                    order.orderNumber,
                    style: TextStyle(
                      fontSize: Tape.dx(14),
                      color: BMColors.bmGrey,
                    ),
                  )
                ],
              ),
            ),
            trailingWidth: Tape.dx(88),
            showBorderRadiusTop: true,
            showBorderRadiusBottom: true,
            showBottomBorder: false,
            isLink: true,
          ),
        ),
      );

      if (index < _orders.length - 1)
        results.add(
          SizedBox(height: Tape.dx(8)),
        );
    }

    return results;
  }

  String _getParcelTypeName(String orderFlow) {
    switch (orderFlow) {
      case 'STD_SHIPMENT_EU_FLOW':
        return 'EU parcel';
      case 'STD_SHIPMENT_NL_FLOW':
        return 'NL parcel';
      case 'STD_RETURN_LIBRARY_FLOW':
        return 'Library return';
      case 'STD_RETURN_FLOW':
        return 'Webshop return';
      case 'STD_SHIPMENT_LOCAL_FLOW':
        return 'Local parcel';
      default:
        return '';
    }
  }

  String _getParcelTypeImage(String orderFlow) {
    switch (orderFlow) {
      case 'STD_RETURN_LIBRARY_FLOW':
        {
          return 'assets/images/icon_books.png';
        }

      default:
        return 'assets/images/icon_parcel.png';
    }
  }

  Widget _secondaryActionButtons() {
    return Row(
      children: <Widget>[
        Expanded(
          child: _dottedBorderFlatButton(
            content: Text(
              'add parcel',
              style: secondaryButtonText,
            ),
            onPressed: widget.onAddParcel,
            height: Tape.dx(44),
          ),
        ),
        SizedBox(width: Tape.dx(8)),
        Expanded(
          child: _dottedBorderFlatButton(
            content: Text(
              'promocode',
              style: secondaryButtonText,
            ),
            onPressed: widget.onAddPromoCode,
            height: Tape.dx(44),
          ),
        ),
      ],
    );
  }

  Widget _dottedBorderFlatButton({
    Widget content,
    Function onPressed,
    double height,
  }) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
      padding: EdgeInsets.all(0),
      child: Stack(
        children: <Widget>[
          Container(
            height: height,
            child: Image(
              image: AssetImage('assets/images/add_empty.png'),
              fit: BoxFit.fill,
            ),
          ),
          Container(
            alignment: Alignment.center,
            height: height,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.add, color: Colors.white),
                SizedBox(width: Tape.dx(10)),
                content,
              ],
            ),
          ),
        ],
      ),
      onPressed: onPressed,
    );
  }

  Widget _footerContent() {
    return PaddedContainer(
      content: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
              left: Tape.dx(10),
              right: Tape.dx(10),
            ),
            child: _getTotalPriceWidget(),
          ),
          RaisedButton(
            onPressed: () {
              widget.onCompleteOrder(_totalPrice);
            },
            padding: EdgeInsets.all(
              Tape.dx(20),
            ),
            color: Theme.of(context).primaryColor,
            child: Text(
              'complete',
              style: completeButton,
            ),
            shape: new RoundedRectangleBorder(
              borderRadius: AppStyles.buttonRadiusAll,
            ),
          ),
        ],
      ),
      contentHeight: Tape.dx(148),
      showShadow: true,
    );
  }

  Widget _getTotalPriceWidget() {
    //  Get the total price from the list of orders
    PriceBreakdown price = _orderService.getPriceComponents(
      _totalPrice,
    );

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          children: <Widget>[
            Text('Total', style: totalText),
            Expanded(child: EmptyWidget()),
            PriceTag(
              unitPrice: '${price.unitPrice}',
              fractionalPrice: price.fractionalPrice,
              color: BMColors.bmDarkblue,
            )
          ],
        ),
        Text('Tax included', style: totalSubText),
        SizedBox(height: Tape.dx(4)),
        _termsAndCondition(),
        SizedBox(height: Tape.dx(12)),
      ],
    );
  }

  Widget _termsAndCondition() {
    return GestureDetector(
      onTap: () {
        _popupService.displayInfo(TermsAndConditionInfo());
      },
      child: RichText(
        text: TextSpan(
          style: termsAndConditionText,
          children: [
            TextSpan(
              text: 'By pressing complete, you accept our ',
            ),
            TextSpan(
              text: 'terms & conditions',
              style: TextStyle(decoration: TextDecoration.underline),
            ),
          ],
        ),
      ),
    );
  }
}
