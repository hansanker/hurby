import 'package:flutter/material.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

mixin OrderOverviewStyle {
  final headerShadow = BoxShadow(
    color: BMColors.bmPrimaryColor,
    blurRadius: 15.0,
    offset: Offset(0.0, 0.75),
  );

  final totalText = TextStyle(
    fontSize: Tape.dx(24),
    fontWeight: FontWeight.w500,
    color: BMColors.bmDarkblue,
  );

  final totalSubText = TextStyle(
    fontSize: Tape.dx(14),
    fontWeight: FontWeight.w500,
    color: BMColors.bmGrey,
  );

  final completeButton = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(26.0),
    fontWeight: FontWeight.w700,
  );

  final headerTitle = TextStyle(
    fontSize: Tape.dx(25),
    fontWeight: FontWeight.w500,
    color: BMColors.bmDarkblue,
  );

  final headerPadding = EdgeInsets.fromLTRB(
    Tape.dx(4),
    Tape.dx(4),
    Tape.dx(0),
    Tape.dx(4),
  );

  final bodyContentPadding = EdgeInsets.only(
    left: Tape.dx(12),
    right: Tape.dx(12),
  );

  final termsAndConditionText = TextStyle(
    fontSize: Tape.dx(11),
    color: BMColors.bmDarkblue,
  );

  final secondaryButtonText = TextStyle(
      fontSize: Tape.dx(18), color: Colors.white, fontWeight: FontWeight.w300);
}
