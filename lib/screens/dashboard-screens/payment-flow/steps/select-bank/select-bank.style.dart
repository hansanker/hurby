import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

mixin SelectBankStyles {
  final tileRadius = BorderRadius.only(
    topRight: Radius.circular(Tape.dx(16)),
    topLeft: Radius.circular(Tape.dx(16)),
    bottomLeft: Radius.circular(Tape.dx(16)),
    bottomRight: Radius.circular(Tape.dx(16)),
  );
}
