import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/select-bank/select-bank.style.dart';
import 'package:hurby/shared/widgets/footer-back-button.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

final Map<String, String> banksMap = {
  'abnamro': 'assets/images/banks/abn.png',
  'asnbank': 'assets/images/banks/asn.png',
  'bunq': 'assets/images/banks/bunq.png',
  'ingbank': 'assets/images/banks/ing.png',
  'knab': 'assets/images/banks/knab.png',
  'rabobank': 'assets/images/banks/rabobank.png',
  'snsbank': 'assets/images/banks/sns.png',
  'triodos': 'assets/images/banks/triodos.png',
  'vanlanschot': 'assets/images/banks/vanlanschot.png',
  'regiobank': 'assets/images/banks/regiobank.png',
};

class SelectBank extends StatelessWidget with SelectBankStyles {
  final List<dynamic> bankList;
  final Function(dynamic) onBankSelected;
  final Function() onPrevious;

  SelectBank({
    this.bankList,
    this.onBankSelected,
    @required this.onPrevious,
  });

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        onPrevious: onPrevious,
        title: 'Select your bank',
        infoMessage: EmptyWidget(),
        homeUrl: 'dashboard/home',
      ),
      clearFixBy: Tape.dx(36),
      mainContent: _mainContent(),
      footerContent: _footerContent(),
    );
  }

  Widget _mainContent() {
    return Container(
      padding: EdgeInsets.all(Tape.dx(10)),
      height: Tape.deviceSize.height - Tape.dx(100),
      child: GridView.count(
        padding: EdgeInsets.only(top: Tape.dx(60)),
        crossAxisCount: 3,
        mainAxisSpacing: Tape.dx(12),
        crossAxisSpacing: Tape.dx(12),
        children: <Widget>[..._getBankList()],
      ),
    );
  }

  List<Widget> _getBankList() {
    Widget emptyCard = Container(
      child: EmptyWidget(),
      decoration: BoxDecoration(
        borderRadius: tileRadius,
        border: Border.all(
          width: Tape.dx(0.5),
          color: Colors.white,
        ),
      ),
    );

    List<Widget> list = [];

    bankList.forEach((dynamic bank) {
      String bankUrl = banksMap[bank['bankCode']];

      list.add(
        GestureDetector(
          onTap: () {
            onBankSelected(bank);
          },
          child: Card(
            child: Image(
              image: AssetImage(bankUrl),
              fit: BoxFit.contain,
            ),
            elevation: 2,
            shape: RoundedRectangleBorder(
              borderRadius: tileRadius,
            ),
          ),
        ),
      );
    });

    // insert empty widgets in the grid
    list.insert(1, emptyCard);
    list.insert(1, emptyCard);

    return list;
  }

  Widget _footerContent() {
    return FooterbackButtonBar(
      onPressed: onPrevious,
    );
  }
}
