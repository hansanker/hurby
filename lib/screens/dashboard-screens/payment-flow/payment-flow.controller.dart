import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/order-overview/order-overview.dart';
import 'package:hurby/services/data.service.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/services/payment.service.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';

mixin PaymentFlowScreenController {
  Function(String) _gotoStepCallback;
  PaymentFlowData flowData = new PaymentFlowData();
  SpinnerService _spinnerService = serviceInjector.spinnerService;
  PaymentService _paymentService = serviceInjector.paymentService;
  DataService _dataService = serviceInjector.dataService;
  PopupService _popupService = serviceInjector.popupService;
  OrderService _orderService = serviceInjector.orderService;
  BuildContext _context;

  void initControllerInstance(
    Function renderStepsFn,
    WebShopReturnFlowData orderFlowData,
    BuildContext context,
  ) async {
    _gotoStepCallback = renderStepsFn;
    _context = context;

    // here we go to the first step
    _setPaymentOptions();
  }

  void _setPaymentOptions() async {
    _spinnerService.toggleSpinner(true);

    List<dynamic> asyncDatasets = await Future.wait([
      _paymentService.getBanksForPaymentGateway(),
      _orderService.getUnPaidOrders(),
    ]);

    flowData.bankList = asyncDatasets[0];
    flowData.orders = asyncDatasets[1];

    _gotoStepCallback('order_overview');
    _spinnerService.toggleSpinner(false);
  }

  bool showPopup() {
    // see if we can show the popup
    return _dataService.canPopupOnOverviewPage();
  }

  void handleBankSelection(dynamic bank) async {
    if (!_spinnerService.isLoading) {
      _spinnerService.toggleSpinner(true);

      flowData.selectedBank = bank;

      // here we get the list of order dids from the orderFlowData
      List<int> orderDids = [];
      for (dynamic order in flowData.orders) {
        orderDids.add(order['did']);
      }

      // we get the promo code if applicable
      String promocodeId = '';
      if (flowData.promoCode.isNotEmpty) {
        promocodeId = flowData.promocodePrice['did'].toString();
      }

      // here we get the payment url
      dynamic res = await _paymentService.getSisowPaymentURL(
        orderDids,
        promocodeId,
        bank['issuerId'],
      );

      if (res['responseStatus']['responseCode'] == '001') {
        flowData.paymentUrl = res['bankURL'];
        flowData.txId = _getTxId();

        _spinnerService.toggleSpinner(false);
        _gotoStepCallback('waiting_payment');
      }
    }
  }

  void handleOrderSelection(OrderData order) {
    switch (order.orderFlow) {
      case 'STD_RETURN_FLOW':
        {
          Navigator.of(_context).pushReplacementNamed(
            'dashboard/order_send_return_flow',
            arguments: order.id,
          );

          break;
        }
      case 'STD_RETURN_LIBRARY_FLOW':
        {
          Navigator.of(_context).pushReplacementNamed(
            'dashboard/library_return_flow',
            arguments: order.id,
          );

          break;
        }
    }
  }

  void handleCompleteAllOrders(double totalPrice) async {
    if (!_spinnerService.isLoading) {
      _spinnerService.toggleSpinner(true);

      // here we get the list of order dids
      List<int> orderDids = [];
      for (dynamic order in flowData.orders) {
        orderDids.add(order['did']);
      }

      if (totalPrice == 0) {
        String promocodeId = '';
        if (flowData.promoCode.isNotEmpty) {
          promocodeId = flowData.promocodePrice['did'].toString();
        }

        await _paymentService.updatePayment(
          orderDids,
          promocodeId,
        );
        _gotoStepCallback('order_confirmed');
      } else {
        _gotoStepCallback('selectBank');
      }

      _spinnerService.toggleSpinner(false);
    }
  }

  void gotoParcelSelectionScreen() {
    // Goto order send and return flow screen
    Navigator.of(_context).pushReplacementNamed(
      'dashboard/parcel_type_selection',
    );
  }

  void handleUpdatePromoCode(String code) async {
    if (!_spinnerService.isLoading) {
      _spinnerService.toggleSpinner(true);

      if (code.isEmpty) {
        flowData.promoCode = '';
        flowData.promocodePrice = null;
        _gotoStepCallback('order_overview');
      } else {
        // validate the promocode on the server
        dynamic codePrice = await _paymentService.getPromoCode(code);

        if (codePrice['responseStatus']['responseCode'] == '001') {
          flowData.promoCode = code;
          flowData.promocodePrice = codePrice;
          _gotoStepCallback('order_overview');
        } else {
          _popupService.showInputBoxError(
            'Invalid promo code',
          );
        }
      }

      _spinnerService.toggleSpinner(false);
    }
  }

  String _getTxId() {
    // extract the txid from the url
    List<String> urlSections = flowData.paymentUrl.split('&');
    String sectionWithTx = urlSections.firstWhere(
      (section) {
        return section.startsWith('txid=');
      },
    );

    return sectionWithTx.split('=')[1];
  }

  // destroy all subscriptions
  void disposeControllerInstance() {}
}
