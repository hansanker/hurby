import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/payment-flow.controller.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/order-confirmed/order-confirmed.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/order-overview/order-overview.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/select-bank/select-bank.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/steps/waiting-payment/waiting-payment.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/index.dart';

class PaymentFlowScreen extends StatefulWidget {
  final WebShopReturnFlowData orderFlowData;

  PaymentFlowScreen({
    this.orderFlowData,
  });

  @override
  _PaymentFlowScreenState createState() {
    return _PaymentFlowScreenState();
  }
}

class _PaymentFlowScreenState extends State<PaymentFlowScreen>
    with PaymentFlowScreenController {
  Map<String, Widget> paymentSteps;
  Widget activeStep = EmptyWidget();

  @override
  void initState() {
    super.initState();

    // init the controller instance
    initControllerInstance(
      _gotoStep,
      widget.orderFlowData,
      context,
    );
  }

  // STEPS FOR PAYMENT FLOW ====>
  void _gotoStep(String step) {
    paymentSteps = {
      'order_overview': OrderOverview(
        orders: flowData.orders,
        promocodePrice: flowData.promocodePrice,
        promocode: flowData.promoCode,
        showPopup: showPopup(),
        onChangePromocode: () {
          _gotoStep('enter_promo_code');
        },
        onAddParcel: gotoParcelSelectionScreen,
        onAddPromoCode: () {
          _gotoStep('enter_promo_code');
        },
        onCompleteOrder: handleCompleteAllOrders,
        onOrderSelected: handleOrderSelection,
      ),
      'enter_promo_code': PropertyEditor(
        capitalization: TextCapitalization.none,
        titleText: 'Enter your promo code',
        subtitleText: '(not case sensitive)',
        placeholder: 'Enter code',
        textType: TextInputType.text,
        alwaysActive: true,
        headerImg: 'assets/images/ill_home_send.png',
        defaultValue: flowData.promoCode,
        onBack: () {
          _gotoStep('order_overview');
        },
        onValue: handleUpdatePromoCode,
        homeUrl: 'dashboard/home',
      ),
      'selectBank': SelectBank(
        bankList: flowData.bankList,
        onBankSelected: handleBankSelection,
        onPrevious: () {
          _gotoStep('order_overview');
        },
      ),
      'waiting_payment': WaitingPayment(
        selectedBank: flowData.selectedBank,
        onPaymentSuccessful: () {
          _gotoStep('order_confirmed');
        },
        onRetry: () {
          _gotoStep('order_overview');
        },
        onPrevious: () {
          _gotoStep('selectBank');
        },
        paymentUrl: flowData.paymentUrl,
        txId: flowData.txId,
      ),
      'order_confirmed': OrderConfirmed(
        onNext: () {
          Navigator.of(context).pushReplacementNamed(
            'dashboard/home',
          );
        },
      ),
    };

    if (paymentSteps[step] != null) {
      activeStep = paymentSteps[step];
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return activeStep ?? EmptyWidget();
  }

  @override
  void dispose() {
    disposeControllerInstance();
    super.dispose();
  }
}
