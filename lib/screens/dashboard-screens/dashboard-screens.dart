import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/screens/dashboard-screens/home-screen/home-screen.dart';
import 'package:hurby/screens/dashboard-screens/library-return-flow/library-return-flow.template.dart';
import 'package:hurby/screens/dashboard-screens/order-send-return-flow/order-send-return.template.dart';
import 'package:hurby/screens/dashboard-screens/parcel-selection-screen/parcel-selection.dart';
import 'package:hurby/screens/dashboard-screens/payment-flow/payment-flow.template.dart';
import 'package:hurby/screens/dashboard-screens/profile-screen/profile-screen.dart';
import 'package:hurby/shared/widgets/custom-navigator.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() {
    return _DashboardScreenState();
  }
}

class _DashboardScreenState extends State<DashboardScreen> {
  List<StreamSubscription> _subs = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: 'dashboard/home',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case 'dashboard/home':
            builder = (BuildContext _) => HomeScreen();
            break;

          case 'dashboard/profile':
            builder = (BuildContext _) => ProfileScreen();
            break;

          case 'dashboard/parcel_type_selection':
            builder = (BuildContext _) => SelectParcelType();
            break;

          case 'dashboard/order_send_return_flow':
            builder = (BuildContext _) => OrderSendReturnFlowScreen(
                  orderId: settings.arguments,
                );
            break;

          case 'dashboard/payment_flow':
            builder = (BuildContext _) => PaymentFlowScreen(
                  orderFlowData: settings.arguments,
                );
            break;

          case 'dashboard/library_return_flow':
            builder = (BuildContext _) => LibraryFlowScreen(
                  orderId: settings.arguments,
                );
            break;

          default:
            throw Exception('Invalid route: ${settings.name}');
        }

        return CustomRoute(
          builder: builder,
          settings: settings,
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();

    // unsubscribe to all subs
    _subs.forEach((sub) => sub.cancel());
  }
}
