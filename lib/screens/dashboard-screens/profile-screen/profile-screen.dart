import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/shared/models/custom-list-tile-config.dart';
import 'package:hurby/shared/models/profile.dart';
import 'package:hurby/shared/widgets/choose-option-popup/choose-option-popup.dart';
import 'package:hurby/shared/widgets/custom-list-tile/custom-list-tile.dart';
import 'package:hurby/shared/widgets/footer-back-button.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/property-editor.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() {
    return _ProfileScreenState();
  }
}

class _ProfileScreenState extends State<ProfileScreen> {
  PropertyEditor _propertyEditor;
  ProfileService _profileService = serviceInjector.profileService;
  PopupService _popupService = serviceInjector.popupService;
  AuthService _authService = serviceInjector.authService;
  Profile _userProfile;
  StreamSubscription _sub;

  void initState() {
    super.initState();

    _profileService.getCurrentUserProfile().then((profile) {
      setState(() {
        _userProfile = profile;
      });
    });

    // we listen to the keyboard height changes
    _sub = Tape.keyboardHeight$.listen((double _) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return _propertyEditor ?? _mainContent(context);
  }

  Widget _mainContent(BuildContext context) {
    // loads to prevents the laggy feel
    // or we could introduce a store for things like this
    return _userProfile != null
        ? SimplePageLayout(
            headerContent: _headerContent(),
            clearFixBy: Tape.dx(80),
            mainContent: _bodyContent(context),
            footerContent: _footerContent(),
          )
        : EmptyWidget();
  }

  Widget _headerContent() {
    return ToolBar(
      onPrevious: _goHome,
      title: 'Your Profile',
      infoMessage: EmptyWidget(),
      homeUrl: 'dashboard/home',
    );
  }

  Widget _bodyContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: Tape.dx(12),
        right: Tape.dx(12),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _userInfoSection(),
          SizedBox(height: Tape.dx(20)),
          _userPreferenceSection(),
          SizedBox(height: Tape.dx(20)),
          _supportSection(context),
        ],
      ),
    );
  }

  Widget _userInfoSection() {
    return CustomListTile(
      items: [
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          title: 'Mobile',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          body: [
            _userProfile.personDTO.mobile,
          ],
          bodyColor: Color.fromRGBO(22, 65, 148, 0.5),
          onClick: () {},
          showBorderRadiusTop: true,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          title: 'Name',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          body: [
            _userProfile.personDTO.firstName ?? '',
          ],
          bodyColor: Theme.of(context).primaryColor,
          onClick: () {
            _initPropertyEditor(
              titleText: 'Edit the default name on your parcel',
              placeholder: 'name',
              propFn: (Profile profile, String value) {
                profile.personDTO.firstName = value;
                return profile;
              },
              defaultValue: _userProfile.personDTO.firstName,
            );
          },
          showBorderRadiusTop: false,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
          isLink: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          title: 'Company name',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          body: [
            _userProfile.personDTO.senderReceiverDTO.companyName ?? '',
          ],
          bodyColor: Theme.of(context).primaryColor,
          onClick: () {
            _initPropertyEditor(
              titleText: 'Add your company name',
              placeholder: 'company name',
              propFn: (Profile profile, String value) {
                profile.personDTO.senderReceiverDTO.companyName = value;
                return profile;
              },
              defaultValue:
                  _userProfile.personDTO.senderReceiverDTO.companyName,
            );
          },
          showBorderRadiusTop: false,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
          isLink: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(78),
          title: 'Home address',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          // omit the subtitle
          body: [
            _userProfile.personDTO.physicalAddressDTO.addressLine1,
            '${_userProfile.personDTO.physicalAddressDTO.postalCode} ${_userProfile.personDTO.physicalAddressDTO.city}',
          ],
          bodyColor: Theme.of(context).primaryColor,
          onClick: () {},
          showBorderRadiusTop: false,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
          isLink: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          title: 'Email',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          body: [
            _userProfile.personDTO.email ?? 'email',
          ],
          bodyColor: Theme.of(context).primaryColor,
          onClick: () {},
          showBorderRadiusTop: false,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
          isLink: true,
        ),
      ],
    );
  }

  Widget _userPreferenceSection() {
    return CustomListTile(
      items: [
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          title: 'Knock the door first',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          // omit the subtitle
          body: ['For when kids sleep'],
          bodyColor: Color.fromRGBO(22, 65, 148, 0.5),

          onClick: () {},

          trailingWidget: Switch(
            value: true,
            onChanged: (bool status) {},
          ),
          trailingWidth: Tape.dx(88),
          trailingAlignment: Alignment.topRight,

          showBorderRadiusTop: true,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          title: 'Language',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          // omit the subtitle
          body: ['English'],
          bodyColor: Theme.of(context).primaryColor,
          trailingWidget: LanguageSelector(
            radiusSize: Tape.dx(40),
          ),
          trailingBackgroundColor: Colors.transparent,
          trailingAlignment: Alignment.topRight,
          trailingWidth: Tape.dx(50),
          onClick: () {},
          showBorderRadiusTop: false,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
          isLink: true,
        ),
      ],
    );
  }

  Widget _supportSection(BuildContext context) {
    return CustomListTile(
      items: [
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          title: 'Support',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          // omit the subtitle
          body: ['We are happy to help you'],
          bodyColor: Color.fromRGBO(22, 65, 148, 0.5),

          onClick: () {},

          showBorderRadiusTop: true,
          showBorderRadiusBottom: false,
          showBottomBorder: true,
          isLink: true,
        ),
        CustomListTileConfig(
          tileHeight: Tape.dx(60),
          title: 'Logout',
          titleColor: Color.fromRGBO(22, 65, 148, 1),
          // omit the subtitle
          body: ['Sign into your acount later'],
          bodyColor: Color.fromRGBO(22, 65, 148, 0.5),

          onClick: () async {
            dynamic res = await _popupService.showCustomDialog(
              context: context,
              content: ChooseOptionPopup(
                messsageText: 'Are you sure you want to logout?',
                confirmText: 'yes, log-out',
                cancelText: 'back',
                image: 'assets/images/logo_mus_only.png',
              ),
            );

            if (res) {
              _authService.logout(context);
            }
          },
          showBorderRadiusTop: false,
          showBorderRadiusBottom: true,
          showBottomBorder: false,
        ),
      ],
    );
  }

  Widget _footerContent() {
    return FooterbackButtonBar(
      onPressed: _goHome,
    );
  }

  void _goHome() {
    Navigator.of(context).pushReplacementNamed(
      'dashboard/home',
    );
  }

  // this function initilizes the property editor
  void _initPropertyEditor({
    @required String titleText,
    @required String placeholder,
    @required Profile Function(Profile, String) propFn,
    String defaultValue,
  }) {
    setState(() {
      _propertyEditor = new PropertyEditor(
        titleText: titleText,
        placeholder: placeholder,
        textType: TextInputType.text,
        defaultValue: defaultValue ?? '',
        headerImg: 'assets/images/ill_home_send.png',
        capitalization: TextCapitalization.none,
        onBack: () {
          setState(() {
            _propertyEditor = null;
          });
        },
        onValue: (String val) async {
          _userProfile = propFn(_userProfile, val);

          setState(() {
            _propertyEditor = null;
          });

          _updateProfile();
        },
        homeUrl: 'dashboard/home',
      );
    });
  }

  _updateProfile() async {
    // Save the profile back to the server
    dynamic res = await _profileService.updateUserProfile(_userProfile);

    if (res['responseCode'] == '001') {
      _popupService.displayToastSuccessMessage(
        'Profile updated successfully',
        context,
      );
    } else {
      _popupService.displayToastErrorMessage(
        'Error updating profile',
        context,
      );
    }
  }

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }
}
