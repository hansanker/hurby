import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:hurby/main.dart';
import 'package:hurby/screens/auth-screens/auth-home/auth-home.dart';
import 'package:hurby/screens/auth-screens/login-flow/login-flow.template.dart';
import 'package:hurby/screens/auth-screens/registration-flow/registration-flow.template.dart';
import 'package:hurby/services/auth.service.dart';
import 'package:hurby/shared/widgets/custom-navigator.dart';
import 'package:hurby/shared/widgets/empty.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() {
    return _AuthScreenState();
  }
}

class _AuthScreenState extends State<AuthScreen> {
  AuthService _authService = serviceInjector.authService;
  List<StreamSubscription> _subs = [];
  bool _authDetermined = false;

  @override
  void initState() {
    super.initState();

    // listens for when the initial build is done
    if (SchedulerBinding.instance.schedulerPhase ==
        SchedulerPhase.persistentCallbacks) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        // here we auto login
        if (!_authService.isLoggedIn()) {
          setState(() {
            _authDetermined = true;
          });
        } else {
          // here we navigate to the dashboard
          Navigator.of(context, rootNavigator: true)
              .pushReplacementNamed('/dashboard');
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return _authDetermined ? _getAuthRoutes(context) : EmptyWidget();
  }

  // Setup and return auth module sub routes
  Widget _getAuthRoutes(BuildContext context) {
    return Navigator(
      initialRoute: 'auth/home',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case 'auth/home':
            builder = (BuildContext _) => AuthHome();
            break;

          case 'auth/register':
            builder = (BuildContext _) => RegistrationFlowScreen();
            break;

          case 'auth/login':
            {
              builder = (BuildContext _) => LoginFlowScreen(
                    authData: settings.arguments,
                  );
              break;
            }

          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return CustomRoute(
          builder: builder,
          settings: settings,
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();

    // unsubscribe to all subs
    _subs.forEach((sub) => sub.cancel());
  }
}
