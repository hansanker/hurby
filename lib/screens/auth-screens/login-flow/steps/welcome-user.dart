import 'dart:async';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/shared/models/profile.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

final TextStyle _bodyText = TextStyle(
  color: Colors.white,
  fontSize: Tape.dx(22),
  fontWeight: FontWeight.w500,
);

class WelcomeUser extends StatefulWidget {
  final Function onContinue;
  final Profile profile;

  WelcomeUser({
    this.onContinue,
    this.profile,
  });

  @override
  _WelcomeUserState createState() {
    return _WelcomeUserState();
  }
}

class _WelcomeUserState extends State<WelcomeUser> {
  StreamSubscription _sub;

  @override
  void initState() {
    super.initState();
    _setupWidget();
  }

  void _setupWidget() async {
    // we listen to the keyboard height changes
    _sub = Tape.keyboardHeight$.listen((double _) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    String name = widget.profile.personDTO.firstName;
    String userName = name.isEmpty ? 'there' : name;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          height: Tape.dx(300),
          child: Center(
            child: FlareActor(
              'assets/flare/anim_checked.flr',
              animation: 'anim_in',
            ),
          ),
        ),
        SizedBox(
          height: Tape.dx(50),
        ),
        Container(
          height: Tape.dx(100),
          child: Column(
            children: <Widget>[
              Text('Hi $userName,', style: _bodyText),
              Text('Welcome back!', style: _bodyText),
            ],
          ),
        ),
      ],
    );
  }

  Widget _footerContainer() {
    return PaddedContainer(
      contentHeight: Tape.dx(72),
      content: _lastStep(),
    );
  }

  Widget _lastStep() {
    return RaisedButton(
      onPressed: () {
        widget.onContinue();
      },
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: Theme.of(context).primaryColor,
      child: Text(
        'continue',
        style: TextStyle(
          color: Colors.white,
          fontSize: Tape.dx(26.0),
          fontWeight: FontWeight.w700,
        ),
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }
}
