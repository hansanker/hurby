import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/binary-option.dart';
import 'package:hurby/shared/widgets/empty.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

final _textStyle = new TextStyle(
  fontSize: Tape.dx(22),
  color: Colors.white,
  fontWeight: FontWeight.w500,
);

class InvalidNumber extends StatefulWidget {
  final Function goBack;
  final Function newAccount;
  final String number;
  final Widget infoMessage;

  InvalidNumber({
    @required this.goBack,
    @required this.newAccount,
    @required this.number,
    @required this.infoMessage,
  });

  @override
  _InvalidNumberState createState() {
    return _InvalidNumberState();
  }
}

class _InvalidNumberState extends State<InvalidNumber>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _entryTransitionLeft;
  Animation<double> _entryOpacity;
  StreamSubscription _sub;

  @override
  void initState() {
    super.initState();
    _setupWidget();
  }

  void _setupWidget() async {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);

    // here we describe the animations for logo entering the screen
    _entryTransitionLeft = Tween<double>(
      begin: Tape.dx(50.0),
      end: 0.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    // here we describe the opacity of that logo entering the screen
    _entryOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    // here we play the animation
    await _animationController.forward();

    // we listen to the keyboard height changes
    _sub = Tape.keyboardHeight$.listen((double _) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        infoMessage: EmptyWidget(),
        homeUrl: 'auth/home',
        onPrevious: () {},
      ),
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          height: Tape.dx(230),
          child: AnimatedBuilder(
            builder: _logo,
            animation: _animationController,
          ),
        ),
        Container(
          height: Tape.dx(115),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('This number is not valid', style: _textStyle),
              Text(widget.number, style: _textStyle),
            ],
          ),
        ),
        SizedBox(
          height: Tape.dx(50),
        ),
        Container(
          height: Tape.dx(100),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text('Create an account or go back to', style: _textStyle),
              Text('change your number', style: _textStyle),
            ],
          ),
        ),
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PaddedContainer(
          contentHeight: Tape.dx(116),
          content: BinaryOption(
            firstOption: 'back',
            secondOption: 'create account',
            onClickedFirst: widget.goBack,
            onClickedSecond: widget.newAccount,
          ),
        ),
      ],
    );
  }

  Widget _logo(_, __) {
    return Opacity(
      opacity: _entryOpacity.value,
      child: Container(
        transform: Matrix4.translationValues(
          _entryTransitionLeft.value,
          0.0,
          0.0,
        ),
        child: Image(
          image: AssetImage('assets/images/phone_unknown.png'),
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    _sub.cancel();
    super.dispose();
  }
}
