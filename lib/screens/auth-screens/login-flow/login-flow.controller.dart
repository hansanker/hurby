import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/storage.service.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';

class LoginSteps {
  static const int enter_mobile_number = 0;
  static const int user_not_found = 1;
  static const int verify_pincode = 2;
  static const int welcome_user = 3;
}

mixin LoginFlowScreenController {
  final PopupService _popupService = serviceInjector.popupService;
  final SpinnerService _spinnerService = serviceInjector.spinnerService;
  final AuthService _authService = serviceInjector.authService;
  final StorageService _storageService = serviceInjector.storageService;
  final ProfileService _profileService = serviceInjector.profileService;

  Function _renderStepsCallback;
  Function _setStateCallback;
  BuildContext _context;
  AuthFlowData _authData;

  List<StreamSubscription> _subs = [];
  bool popupVisible = false;

  int step = LoginSteps.enter_mobile_number;
  List<Widget> authFlowSteps = [];

  void initControllerInstance(
    BuildContext context,
    Function renderStepsFn,
    Function setStateFn,
    AuthFlowData authData,
  ) {
    _renderStepsCallback = renderStepsFn;
    _setStateCallback = setStateFn;
    _context = context;
    _authData = authData;

    // here we listen for when the info popup is visible
    _subs.add(
      _popupService.infoBoxStream$.listen((Widget info) {
        _setStateCallback(() {
          _renderStepsCallback();
          popupVisible = info != null;
        });
      }),
    );

    // here we render the steps
    _renderStepsCallback();
  }

  // If true, we proceed to enter verification code
  // else user already exist so we go to the login flow with the number already prepopulated
  void verifyMobileNumber(String value) async {
    _authData.phoneNumber = value;
    try {
      _spinnerService.toggleSpinner(true);
      String responseCode = await _authService.sendLoginOTP(
        _authData.phoneNumber,
      );

      switch (responseCode) {
        case '001':
          {
            gotoStep(LoginSteps.verify_pincode);
            break;
          }
        case '1014':
          {
            _popupService.displayToastErrorMessage(
              'Error verifying mobile number',
              _context,
            );
            break;
          }
        case '1011':
          {
            gotoStep(LoginSteps.user_not_found);
            break;
          }
      }
    } catch (e) {
      _popupService.displayToastErrorMessage(
        'Error verifying mobile number',
        _context,
      );
    } finally {
      _spinnerService.toggleSpinner(false);
    }
  }

  void verifyOtp(String value) async {
    _authData.pinCode = value;

    try {
      _spinnerService.toggleSpinner(true);
      dynamic responseData = (await _authService.verifyLoginOTP(
            number: _authData.phoneNumber,
            otp: _authData.pinCode,
          )) ??
          null;

      if (responseData != null) {
        // Save access token to storage
        await _storageService.setItem(
          'accessToken',
          responseData['accessToken'],
        );

        // Get user profile from the server
        _authData.userProfile = await _profileService.getCurrentUserProfile();

        gotoStep(LoginSteps.welcome_user);
      } else {
        _popupService.showInputBoxError(
          'Error verifying mobile number',
        );
      }
    } catch (e) {
      _popupService.showInputBoxError(
        'Invalid pin code',
      );
    } finally {
      _spinnerService.toggleSpinner(false);
    }
  }

  void gotoStep(int authStep) {
    // here we set the step
    step = authStep;
    _renderStepsCallback();
    _setStateCallback(() {});
  }

  // destroy all subscriptions
  void disposeControllerInstance() {
    // dispose of subscriptions
    for (var sub in _subs) {
      sub.cancel();
    }
  }
}
