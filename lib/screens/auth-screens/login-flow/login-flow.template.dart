import 'package:flutter/material.dart';
import 'package:hurby/info-message-widgets/phonenumber-info.dart';
import 'package:hurby/screens/auth-screens/login-flow/login-flow.controller.dart';
import 'package:hurby/screens/auth-screens/login-flow/steps/invalid-number.dart';
import 'package:hurby/screens/auth-screens/login-flow/steps/welcome-user.dart';
import 'package:hurby/screens/auth-screens/registration-flow/steps/root.dart';
import 'package:hurby/screens/auth-screens/registration-flow/widgets/pincode-input.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/empty.dart';
import 'package:hurby/shared/widgets/input-box.dart';
import 'package:hurby/shared/widgets/property-editor.dart';

class LoginFlowScreen extends StatefulWidget {
  final AuthFlowData authData;

  LoginFlowScreen({
    @required this.authData,
  });

  @override
  _LoginFlowScreenState createState() {
    return _LoginFlowScreenState();
  }
}

class _LoginFlowScreenState extends State<LoginFlowScreen>
    with LoginFlowScreenController {
  @override
  void initState() {
    super.initState();

    // init the controller instance
    initControllerInstance(
      context,
      _renderSteps,
      (void Function() fn) => setState(fn),
      widget.authData,
    );
  }

  // STEPS FOR NEW USER AUTHENTICATION ====>>>
  void _renderSteps() {
    authFlowSteps = [
      // Enter mobile number
      PropertyEditor(
        titleText: 'Enter your mobile number',
        headerImg: 'assets/images/phone.png',
        placeholder: 'Enter mobile number',
        homeUrl: 'auth/home',
        onValue: verifyMobileNumber,
        onChange: (String value) {
          widget.authData.phoneNumber = value;
        },
        defaultValue: widget.authData.phoneNumber,
        textType: TextInputType.number,
        min: 10,
        max: 14,
        infoMessage: EnterPhoneNumberInfo(),
        capitalization: TextCapitalization.none,
        onBack: () {
          Navigator.of(context).pushReplacementNamed('auth/home');
        },
      ),

      // Number is invalid
      InvalidNumber(
        goBack: () {
          gotoStep(LoginSteps.enter_mobile_number);
        },
        newAccount: () {
          Navigator.of(context).pushReplacementNamed('auth/register');
        },
        number: widget.authData.phoneNumber,
        infoMessage: EmptyWidget(),
      ),

      // Ask user to verify their pin code
      VerifyPinCode(
        onPrevious: () {
          gotoStep(LoginSteps.enter_mobile_number);
        },
        bottomAction: InputBox(
          inputField: PinCodeInput(
            onSubmit: verifyOtp,
            defaultValue: '',
          ),
        ),
      ),

      // welocome user
      WelcomeUser(
        onContinue: () {
          // here we navigate to the dashboard
          Navigator.of(context, rootNavigator: true).pushReplacementNamed('/dashboard');
        },
        profile: widget.authData.userProfile,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return popupVisible ? EmptyWidget() : authFlowSteps[step];
  }

  @override
  void dispose() {
    disposeControllerInstance();
    super.dispose();
  }
}
