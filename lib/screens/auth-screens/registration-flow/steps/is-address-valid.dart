import 'package:flutter/material.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/binary-option.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class IsAddressValid extends StatefulWidget {
  final Function onPrevious;
  final Function onSubmit;
  final AuthFlowData userData;

  IsAddressValid({this.onPrevious, this.onSubmit, @required this.userData});

  @override
  _IsAddressValidState createState() {
    return _IsAddressValidState();
  }
}

class _IsAddressValidState extends State<IsAddressValid> {
  TextStyle _textStyle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        infoMessage: EmptyWidget(),
        onPrevious: widget.onPrevious,
        homeUrl: 'auth/home',
      ),
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(height: Tape.dx(50)),
        Container(
          child: Image(
            image: AssetImage('assets/images/house_empty.png'),
            fit: BoxFit.contain,
            height: Tape.dx(168.0),
          ),
        ),
        SizedBox(height: Tape.dx(50)),
        Container(
          height: Tape.dx(300),
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                '${widget.userData.street} ${widget.userData.houseNumber} ${widget.userData.suffix}',
                style: _textStyle,
              ),
              Text(widget.userData.postalCode, style: _textStyle),
              Text(widget.userData.city, style: _textStyle),
            ],
          ),
        ),
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          alignment: Alignment.bottomCenter,
          child: Text('Is this adress correct?', style: _textStyle),
        ),
        SizedBox(
          height: Tape.dx(50),
        ),
        PaddedContainer(
          contentHeight: Tape.dx(116),
          content: BinaryOption(
            firstOption: 'yes',
            secondOption: 'no',
            onClickedFirst: () {
              widget.onSubmit(true);
            },
            onClickedSecond: () {
              widget.onSubmit(false);
            },
          ),
        ),
      ],
    );
  }
}
