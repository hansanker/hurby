import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class AddressIsNotValid extends StatefulWidget {
  final Function onNext;

  AddressIsNotValid({
    this.onNext,
  });

  @override
  _AddressIsNotValidState createState() {
    return _AddressIsNotValidState();
  }
}

class _AddressIsNotValidState extends State<AddressIsNotValid> {
  TextStyle _bodyText = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );

  TextStyle _bodySubText = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(17),
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: Tape.dx(50),
        ),
        Container(
          height: Tape.dx(296),
          child: Image(
            image: AssetImage('assets/images/not_in_area.png'),
            fit: BoxFit.contain,
          ),
        ),
        Container(
          child: Column(
            children: <Widget>[
              Text('Unfortunately!', style: _bodyText),
              SizedBox(height: Tape.dx(5)),
              Text('We are currently not active in', style: _bodyText),
              SizedBox(height: Tape.dx(5)),
              Text('your area', style: _bodyText),
              SizedBox(height: Tape.dx(10)),
              Text(
                '(hopefully this will change soon)',
                style: _bodySubText,
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        PaddedContainer(
          contentHeight: Tape.dx(116),
          content: _continue(),
        ),
      ],
    );
  }

  Widget _continue() {
    return RaisedButton(
      onPressed: () {
        widget.onNext();
      },
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: Theme.of(context).primaryColor,
      child: Text(
        'stay up-to date',
        style: TextStyle(
          color: Colors.white,
          fontSize: Tape.dx(26.0),
          fontWeight: FontWeight.w700,
        ),
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }
}
