import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class ThankYouForEmail extends StatelessWidget {
  final Function onNext;

  ThankYouForEmail({
    this.onNext,
  });

  final TextStyle _bodyText = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(25),
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: _mainContent(),
      footerContent: _footerContainer(context),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: Tape.dx(50),
        ),
        Container(
          height: Tape.dx(296),
          child: Image(
            image: AssetImage('assets/images/login_logo.png'),
            fit: BoxFit.contain,
          ),
        ),
        Column(
          children: <Widget>[
            Text('Thank you!', style: _bodyText),
            SizedBox(
              height: Tape.dx(20),
            ),
            Text('We will keep you posted!', style: _bodyText),
          ],
        ),
      ],
    );
  }

  Widget _footerContainer(BuildContext context) {
    return PaddedContainer(
      contentHeight: Tape.dx(116),
      content: _continue(context),
    );
  }

  Widget _continue(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        onNext();
      },
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: Theme.of(context).primaryColor,
      child: Text(
        'stay up-to date',
        style: TextStyle(
          color: Colors.white,
          fontSize: Tape.dx(26.0),
          fontWeight: FontWeight.w700,
        ),
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }
}
