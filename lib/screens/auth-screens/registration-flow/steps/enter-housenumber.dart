import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/empty.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class EnterHouseNumber extends StatefulWidget {
  final Widget bottomAction;
  final Function onPrevious;

  EnterHouseNumber({
    @required this.bottomAction,
    @required this.onPrevious,
  });

  @override
  _EnterHouseNumberState createState() {
    return _EnterHouseNumberState();
  }
}

class _EnterHouseNumberState extends State<EnterHouseNumber>
    with TickerProviderStateMixin {
  bool _animationDone = false;
  AnimationController _animationController;
  Animation<double> _entryTransitionLeft;
  Animation<double> _entryOpacity;
  StreamSubscription _sub;

  @override
  void initState() {
    super.initState();
    _setupWidget();
  }

  void _setupWidget() async {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);

    // here we describe the animations for logo entering the screen
    _entryTransitionLeft = Tween<double>(
      begin: Tape.dx(50.0),
      end: 0.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    // here we describe the opacity of that logo entering the screen
    _entryOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    // here we play the animation
    await _animationController.forward();
    setState(() {
      _animationDone = true;
    });

    // we listen to the keyboard height changes
    _sub = Tape.keyboardHeight$.listen((double _) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        homeUrl: 'auth/home',
        onPrevious: widget.onPrevious,
      ),
      mainContent: _bodyContent(),
      footerContent: _footerContent(),
    );
  }

  Widget _bodyContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(height: Tape.dx(40)),
        AnimatedBuilder(
          builder: _logo,
          animation: _animationController,
        ),
        SizedBox(height: Tape.dx(40)),
        Container(
          alignment: Alignment.bottomCenter,
          child: Column(
            children: <Widget>[
              Text(
                'Enter your house number',
                style: TextStyle(
                  fontSize: Tape.dx(22),
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(
                height: Tape.dx(10),
              ),
              Text(
                '(without suffix)',
                style: TextStyle(
                  fontSize: Tape.dx(17),
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _footerContent() {
    return Container(
      height: Tape.dx(150),
      transform: Matrix4.translationValues(
        0.0,
        -(Tape.keyboardHeight),
        0.0,
      ),
      child: _animationDone ? widget.bottomAction : EmptyWidget(),
    );
  }

  Widget _logo(_, __) {
    return Opacity(
      opacity: _entryOpacity.value,
      child: Container(
        transform: Matrix4.translationValues(
          _entryTransitionLeft.value,
          0.0,
          0.0,
        ),
        child: Image(
          image: AssetImage('assets/images/house_empty.png'),
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    _sub.cancel();
    super.dispose();
  }
}
