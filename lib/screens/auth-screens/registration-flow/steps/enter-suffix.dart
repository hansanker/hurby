import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/empty.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class EnterSuffix extends StatefulWidget {
  final Widget bottomAction;
  final Function onPrevious;

  EnterSuffix({
    @required this.bottomAction,
    @required this.onPrevious,
  });

  @override
  _EnterSuffixState createState() {
    return _EnterSuffixState();
  }
}

class _EnterSuffixState extends State<EnterSuffix>
    with TickerProviderStateMixin {
  bool _animationDone = false;
  AnimationController _animationController;
  Animation<double> _entryTransitionLeft;
  Animation<double> _entryOpacity;
  StreamSubscription _sub;

  @override
  void initState() {
    super.initState();
    _setupWidget();
  }

  void _setupWidget() async {
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);

    // here we describe the animations for logo entering the screen
    _entryTransitionLeft = Tween<double>(
      begin: Tape.dx(50.0),
      end: 0.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    // here we describe the opacity of that logo entering the screen
    _entryOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.5,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    // here we play the animation
    await _animationController.forward();
    setState(() {
      _animationDone = true;
    });

    // we listen to the keyboard height changes
    _sub = Tape.keyboardHeight$.listen((double _) {
      setState(() {});
    });
  }

  @override
  build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        homeUrl: 'auth/home',
        onPrevious: widget.onPrevious,
      ),
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: Tape.dx(50),
        ),
        AnimatedBuilder(
          builder: _houseLogo,
          animation: _animationController,
        ),
        Column(
          children: <Widget>[
            Text(
              'Enter the suffix',
              style: TextStyle(
                fontSize: Tape.dx(25),
                color: Colors.white,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: Tape.dx(10)),
            Text(
              '(or choose none)',
              style: TextStyle(
                fontSize: Tape.dx(17),
                color: Colors.white,
                fontWeight: FontWeight.w300,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _houseLogo(_, __) {
    return Opacity(
      opacity: _entryOpacity.value,
      child: Container(
        transform: Matrix4.translationValues(
          _entryTransitionLeft.value,
          0.0,
          0.0,
        ),
        child: Image(
          image: AssetImage('assets/images/house_empty.png'),
          fit: BoxFit.contain,
        ),
      ),
    );
  }

  Widget _footerContainer() {
    return Container(
      transform: Matrix4.translationValues(
        0.0,
        -(Tape.keyboardHeight),
        0.0,
      ),
      child: _animationDone ? widget.bottomAction : EmptyWidget(),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    _sub.cancel();
    super.dispose();
  }
}
