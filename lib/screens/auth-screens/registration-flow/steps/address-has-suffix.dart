import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/binary-option.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

class AddressHasSuffix extends StatefulWidget {
  final Function onPrevious;
  final Function onSubmit;

  AddressHasSuffix({
    this.onPrevious,
    this.onSubmit,
  });

  @override
  _AddressHasSuffixState createState() {
    return _AddressHasSuffixState();
  }
}

class _AddressHasSuffixState extends State<AddressHasSuffix> {
  TextStyle _textStyle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        infoMessage: EmptyWidget(),
        onPrevious: widget.onPrevious,
        homeUrl: 'auth/home',
      ),
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: Tape.dx(50),
        ),
        Container(
          child: Image(
            image: AssetImage('assets/images/house_empty.png'),
            fit: BoxFit.contain,
            height: Tape.dx(168.0),
          ),
        )
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          alignment: Alignment.bottomCenter,
          child: Text('Add a suffix?', style: _textStyle),
        ),
        SizedBox(
          height: Tape.dx(20),
        ),
        PaddedContainer(
          contentHeight: Tape.dx(116),
          content: BinaryOption(
            firstOption: 'yes',
            secondOption: 'no',
            onClickedFirst: () {
              widget.onSubmit(true);
            },
            onClickedSecond: () {
              widget.onSubmit(false);
            },
          ),
        ),
      ],
    );
  }
}
