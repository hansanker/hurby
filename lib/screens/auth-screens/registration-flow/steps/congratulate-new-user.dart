import 'dart:convert';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/storage.service.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/utils/screen.util.dart';

class CongratulateNewUser extends StatefulWidget {
  final dynamic registeredUserResponse;
  CongratulateNewUser({
    this.registeredUserResponse,
  });

  @override
  _CongratulateNewUserState createState() {
    return _CongratulateNewUserState();
  }
}

class _CongratulateNewUserState extends State<CongratulateNewUser> {
  StorageService _storageService = serviceInjector.storageService;

  TextStyle _bodyText = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration(seconds: 3), () async {
      await _storageService.setItem(
        'registeredUserResponse',
        json.encode(widget.registeredUserResponse),
      );

      // here we navigate to the dashboard
      Navigator.of(context, rootNavigator: true)
          .pushReplacementNamed('/dashboard');
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: _mainContent(),
      footerContent: EmptyWidget(),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        SizedBox(
          height: Tape.dx(50),
        ),
        Container(
          height: Tape.dx(296),
          child: Center(
            child: FlareActor(
              'assets/flare/anim_checked.flr',
              animation: 'anim_in',
            ),
          ),
        ),
        Container(
          alignment: Alignment.center,
          child: Column(
            children: <Widget>[
              Text('Congratulations!', style: _bodyText),
              SizedBox(height: Tape.dx(25)),
              Text('From now on send', style: _bodyText),
              SizedBox(height: Tape.dx(5)),
              Text('Packages from home', style: _bodyText),
            ],
          ),
        ),
      ],
    );
  }
}
