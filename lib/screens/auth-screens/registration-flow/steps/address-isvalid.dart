import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class AddressIsValid extends StatefulWidget {
  final Function onNext;

  AddressIsValid({
    this.onNext,
  });

  @override
  _AddressIsValidState createState() {
    return _AddressIsValidState();
  }
}

class _AddressIsValidState extends State<AddressIsValid> {
  TextStyle _bodyText = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(22),
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: _mainContent(),
      footerContent: _footerContainer(),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: Tape.dx(50),
        ),
        Container(
          height: Tape.dx(296),
          child: Center(
            child: FlareActor(
              'assets/flare/anim_checked.flr',
              animation: 'anim_in',
            ),
          ),
        ),
      ],
    );
  }

  Widget _footerContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(Tape.dx(10)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Tjilp!', style: _bodyText),
              SizedBox(height: Tape.dx(20)),
              Text('We do pickup at your address', style: _bodyText),
            ],
          ),
        ),
        SizedBox(
          height: Tape.dx(30),
        ),
        PaddedContainer(
          contentHeight: Tape.dx(72),
          content: _lastStep(),
        ),
      ],
    );
  }

  Widget _lastStep() {
    return RaisedButton(
      onPressed: () {
        widget.onNext();
      },
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: Theme.of(context).primaryColor,
      child: Text(
        'final step',
        style: TextStyle(
          color: Colors.white,
          fontSize: Tape.dx(26.0),
          fontWeight: FontWeight.w700,
        ),
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
    );
  }
}
