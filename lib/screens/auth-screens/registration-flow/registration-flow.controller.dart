import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/storage.service.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';

class RegistrationSteps {
  static const int enter_postal_code = 0;
  static const int enter_house_number = 1;
  static const int address_has_suffix = 2;
  static const int enter_suffix = 3;
  static const int confirm_address = 4;
  static const int success_address_validity = 5;
  static const int failed_address_validity = 6;
  static const int enter_email_address = 7;
  static const int thank_you_for_email = 8;
  static const int enter_mobile_number = 9;
  static const int verify_pincode = 10;
  static const int congratulate_new_user = 11;
  static const int number_is_inuse = 12;
}

mixin RegistrationFlowScreenController {
  final PopupService _popupService = serviceInjector.popupService;
  final SpinnerService _spinnerService = serviceInjector.spinnerService;
  final PostcodeService _postcodeService = serviceInjector.postcodeService;
  final LabelService _labelService = serviceInjector.labelService;
  final AuthService _authService = serviceInjector.authService;
  final StorageService _storageService = serviceInjector.storageService;
  final AuthFlowData authData = new AuthFlowData();

  Function _renderStepsCallback;
  Function _setStateCallback;
  BuildContext _context;

  List<StreamSubscription> _subs = [];
  bool popupVisible = false;

  int step = RegistrationSteps.enter_postal_code;
  List<Widget> authFlowSteps = [];

  void initControllerInstance(
    BuildContext context,
    Function renderStepsFn,
    Function setStateFn,
  ) {
    _renderStepsCallback = renderStepsFn;
    _setStateCallback = setStateFn;
    _context = context;

    // here we listen for when the info popup is visible
    _subs.add(
      _popupService.infoBoxStream$.listen((Widget info) {
        _setStateCallback(() {
          _renderStepsCallback();
          popupVisible = info != null;
        });
      }),
    );

    // here we render the steps
    _renderStepsCallback();
  }

  // verify address is valid in area
  void verifyAddressIsValidInArea() async {
    if (!_spinnerService.isLoading) {
      try {
        _spinnerService.toggleSpinner(true);
        bool isValidAdress = await _postcodeService.isvalidNeighbourhood(
          houseNumber: authData.houseNumber,
          postalCode: authData.postalCode,
          suffix: authData.suffix,
          lang: _labelService.getActiveLanguage().symbol,
        );

        if (isValidAdress) {
          dynamic address = await _postcodeService.getAddress(
            houseNumber: authData.houseNumber,
            postalCode: authData.postalCode,
            suffix: authData.suffix,
            lang: _labelService.getActiveLanguage().symbol,
          );

          if (address != null) {
            // save the data in the authData then ask the user if the details is correct
            authData.street = address['street'];
            authData.city = address['city'];

            gotoStep(RegistrationSteps.confirm_address);
          } else {
            _popupService.displayToastErrorMessage(
              'Server returned empty adress',
              _context,
            );
            gotoStep(RegistrationSteps.enter_postal_code);
          }
          // if no address is returned we just take them to the enter postal code route
        } else {
          gotoStep(RegistrationSteps.failed_address_validity);
        }
      } catch (e) {
        _popupService.displayToastErrorMessage(
          'Error verifying address',
          _context,
        );
      } finally {
        _spinnerService.toggleSpinner(false);
      }
    }
  }

  void saveUserEmailForFailedRegistration(String value) async {
    authData.emailAddress = value;

    try {
      _spinnerService.toggleSpinner(true);

      await _postcodeService.saveFailedRegistrationAttempt(
        houseNumber: authData.houseNumber,
        postalCode: authData.postalCode,
        suffix: authData.suffix,
        lang: _labelService.getActiveLanguage().symbol,
        email: authData.emailAddress,
      );
    } catch (e) {
      _popupService.displayToastErrorMessage(
        'Error saving user email address',
        _context,
      );
    } finally {
      _spinnerService.toggleSpinner(false);
      gotoStep(RegistrationSteps.thank_you_for_email);
    }
  }

  // If true, we proceed to enter verification code
  // else user already exist so we go to the login flow with the number already prepopulated
  void verifyMobileNumber(String value) async {
    authData.phoneNumber = value;
    try {
      _spinnerService.toggleSpinner(true);
      bool numberIsValid = await _authService.sendRegistrationOTP(
        authData.phoneNumber,
      );

      if (numberIsValid) {
        gotoStep(RegistrationSteps.verify_pincode);
      } else {
        gotoStep(RegistrationSteps.number_is_inuse);
      }
    } catch (e) {
      print(e);
      _popupService.displayToastErrorMessage(
        'Error verifying mobile number',
        _context,
      );
    } finally {
      _spinnerService.toggleSpinner(false);
    }
  }

  // Go to the login flow when the entered number is already in use
  void gotoLoginFlow() {
    Navigator.of(_context).pushReplacementNamed(
      'auth/login',
      arguments: authData,
    );
  }

  void verifyOtp(String value) async {
    authData.pinCode = value;

    try {
      _spinnerService.toggleSpinner(true);
      bool otpIsValid = await _authService.verifyRegistrationOTP(
        number: authData.phoneNumber,
        otp: authData.pinCode,
      );

      if (otpIsValid) {
        // register the user then goto dashboard
        dynamic responseData = (await _registerUser()) ?? null;
        dynamic responseStatus = responseData['responseStatus'];

        if (responseData != null && responseStatus['responseCode'] == '001') {
          // Save access token to storage
          await _storageService.setItem(
              'accessToken', responseData['accessToken']);

          authData.registeredUserResponse = responseData;
          gotoStep(RegistrationSteps.congratulate_new_user);
        } else {
          _popupService.showInputBoxError(
            'Error registering user',
          );
        }
      } else {
        _popupService.showInputBoxError(
          'Error verifying mobile number',
        );
      }
    } catch (e) {
      _popupService.displayToastErrorMessage(
        'Error verifying mobile number',
        _context,
      );
    } finally {
      _spinnerService.toggleSpinner(false);
    }
  }

  Future<dynamic> _registerUser() async {
    dynamic payload = {
      'loginDetails': {
        'appId': 'CONSUMER_APP',
        'accessToken': '',
        'userName': authData.phoneNumber,
        'lang': _labelService.getActiveLanguage().symbol,
      },
      'address': {
        'houseNumber': authData.houseNumber,
        'houseNumberSuffix': authData.suffix,
        'street': authData.street,
        'postalCode': authData.postalCode,
        'city': authData.city,
        'countryCode': 'NL'
      },
      'email': authData.emailAddress,
      'mobileNumber': authData.phoneNumber,
      'firstName': '',
      'lastName': '',
      'type': 'PERSONAL'
    };

    return _authService.registerUser(
      payload: payload,
    );
  }

  void gotoStep(int authStep) {
    // here we set the step
    step = authStep;
    _renderStepsCallback();
    _setStateCallback(() {});
  }

  // destroy all subscriptions
  void disposeControllerInstance() {
    // dispose of subscriptions
    for (var sub in _subs) {
      sub.cancel();
    }
  }
}
