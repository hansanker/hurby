import 'package:flutter/material.dart';
import 'package:hurby/info-message-widgets/phonenumber-info.dart';
import 'package:hurby/screens/auth-screens/registration-flow/registration-flow.controller.dart';
import 'package:hurby/screens/auth-screens/registration-flow/steps/number-is-inuse.dart';
import 'package:hurby/screens/auth-screens/registration-flow/widgets/index.dart';
import 'package:hurby/shared/widgets/empty.dart';
import 'package:hurby/screens/auth-screens/registration-flow/steps/root.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/property-editor.dart';
import 'package:hurby/shared/widgets/suffix-input.dart';

class RegistrationFlowScreen extends StatefulWidget {
  @override
  _RegistrationFlowScreenState createState() {
    return _RegistrationFlowScreenState();
  }
}

class _RegistrationFlowScreenState extends State<RegistrationFlowScreen>
    with RegistrationFlowScreenController {
  @override
  void initState() {
    super.initState();

    // init the controller instance
    initControllerInstance(
      context,
      _renderSteps,
      (void Function() fn) => setState(fn),
    );
  }

  // STEPS FOR NEW USER AUTHENTICATION ====>>>
  void _renderSteps() {
    authFlowSteps = [
      // Enter postal Code step
      EnterPostalCode(
        onPrevious: () {
          // Goto register flow
          Navigator.of(context).pushReplacementNamed('auth/home');
        },
        bottomAction: InputBox(
          inputField: PostalCodeInput(
            onSubmit: (String value) {
              authData.postalCode = value;
              gotoStep(RegistrationSteps.enter_house_number);
            },
            defaultValue: authData.postalCode,
          ),
        ),
      ),

      // Enter house number step
      EnterHouseNumber(
        onPrevious: () {
          gotoStep(RegistrationSteps.enter_postal_code);
        },
        bottomAction: InputBox(
          inputField: TextInput(
            onSubmit: (String value) {
              authData.houseNumber = value;
              gotoStep(RegistrationSteps.address_has_suffix);
            },
            onChange: () {},
            defaultValue: authData.houseNumber,
            textType: TextInputType.number,
            min: 1,
            max: 6,
          ),
        ),
      ),

      // Ask if user address has suffix
      AddressHasSuffix(
        onPrevious: () {
          gotoStep(RegistrationSteps.enter_house_number);
        },
        onSubmit: (bool response) async {
          authData.addressHasSuffix = response;

          if (response) {
            gotoStep(RegistrationSteps.enter_suffix);
          } else {
            verifyAddressIsValidInArea();
          }
        },
      ),

      // Enter suffix - step
      EnterSuffix(
        onPrevious: () {
          gotoStep(RegistrationSteps.enter_house_number);
        },
        bottomAction: InputBox(
          inputField: SuffixInput(
            onSubmit: (String value) async {
              authData.suffix = value;
              verifyAddressIsValidInArea();
            },
            defaultValue: authData.suffix,
          ),
        ),
      ),

      // Is address valid
      IsAddressValid(
        userData: authData,
        onPrevious: () {
          if (authData.addressHasSuffix) {
            gotoStep(RegistrationSteps.enter_suffix);
          } else {
            gotoStep(RegistrationSteps.enter_house_number);
          }
        },
        onSubmit: (bool response) {
          if (response) {
            bool isValid = authData.city.toLowerCase() == 'haarlem';

            if (isValid) {
              gotoStep(RegistrationSteps.success_address_validity);
            } else {
              gotoStep(RegistrationSteps.failed_address_validity);
            }
          } else {
            // we go to step
            gotoStep(RegistrationSteps.enter_postal_code);
          }
        },
      ),

      // Address validated successfully.
      AddressIsValid(
        onNext: () {
          gotoStep(RegistrationSteps.enter_mobile_number);
        },
      ),

      // Address failed to validate.
      AddressIsNotValid(
        onNext: () {
          gotoStep(RegistrationSteps.enter_email_address);
        },
      ),

      // Enter email address to stay up to date
      PropertyEditor(
        titleText: 'Enter email address',
        headerImg: 'assets/images/house_empty.png',
        textType: TextInputType.text,
        min: 5,
        max: 30,
        regex: RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+"),
        placeholder: 'email address',
        capitalization: TextCapitalization.none,
        defaultValue: '',
        onValue: saveUserEmailForFailedRegistration,
        onBack: () {
          Navigator.of(context).pushReplacementNamed('auth/home');
        },
        homeUrl: 'auth/home',
      ),

      // Thank user for email
      ThankYouForEmail(
        onNext: () {
          // Goto register flow
          Navigator.of(context).pushReplacementNamed('auth/home');
        },
      ),

      // Enter mobile number
      PropertyEditor(
        titleText: 'Enter your mobile number',
        headerImg: 'assets/images/phone.png',
        placeholder: 'Enter mobile number',
        onValue: verifyMobileNumber,
        onChange: (String value) {
          authData.phoneNumber = value;
        },
        defaultValue: authData.phoneNumber,
        textType: TextInputType.number,
        min: 10,
        max: 14,
        infoMessage: EnterPhoneNumberInfo(),
        capitalization: TextCapitalization.none,
        onBack: () {
          gotoStep(RegistrationSteps.confirm_address);
        },
        homeUrl: 'auth/home',
      ),

      // Ask user to verify their pin code
      VerifyPinCode(
        onPrevious: () {
          gotoStep(RegistrationSteps.enter_mobile_number);
        },
        bottomAction: InputBox(
          inputField: PinCodeInput(
            onSubmit: verifyOtp,
            defaultValue: '',
          ),
        ),
      ),

      // Congratulate user
      CongratulateNewUser(
        registeredUserResponse: authData.registeredUserResponse,
      ),

      // number is in use
      NumberIsInUse(
        goBack: () {
          gotoStep(RegistrationSteps.enter_mobile_number);
        },
        login: gotoLoginFlow,
        number: authData.phoneNumber,
        infoMessage: EmptyWidget(),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return authFlowSteps[step];
  }

  @override
  void dispose() {
    print('Was this disposed?');
    disposeControllerInstance();
    super.dispose();
  }
}
