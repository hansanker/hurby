import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:hurby/shared/models/inputfield-state.model.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class PostalCodeInput extends StatefulWidget {
  final Function onSubmit;
  final String defaultValue;

  PostalCodeInput({
    @required this.onSubmit,
    this.defaultValue,
  });

  @override
  _PostalCodeInputState createState() {
    return new _PostalCodeInputState();
  }
}

class _PostalCodeInputState extends State<PostalCodeInput> {
  bool _isValid = false;

  List<InputFieldState> _formFields = [
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.number,
      validation: new RegExp('[0-9]'),
      isActive: false,
    ),
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.number,
      validation: new RegExp('[0-9]'),
      isActive: false,
    ),
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.number,
      validation: new RegExp('[0-9]'),
      isActive: false,
    ),
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.number,
      validation: new RegExp('[0-9]'),
      isActive: false,
    ),
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.emailAddress,
      validation: new RegExp('[a-zA-z]'),
      isActive: false,
    ),
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.emailAddress,
      validation: new RegExp('[a-zA-z]'),
      isActive: false,
    ),
  ];

  @override
  void initState() {
    super.initState();

    // listens for when the initial build is done
    if (SchedulerBinding.instance.schedulerPhase ==
        SchedulerPhase.persistentCallbacks) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        _formFields[0].controller.text = widget.defaultValue;
        _handleInputChange(_formFields[0], 0);

        int focusIndex = widget.defaultValue.length == 0
            ? 0
            : widget.defaultValue.length - 1;
        FocusScope.of(context).requestFocus(_formFields[focusIndex].node);

        _updateActiveInputs(focusIndex);
      });
    }
  }

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> content = [
      ..._buildFormInputs(),
      SizedBox(
        width: Tape.dx(20),
      ),
      _actionButton(context),
    ];

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: content,
    );
  }

  List<Widget> _buildFormInputs() {
    List<Widget> inputs = [];

    for (var i = 0; i < _formFields.length; i++) {
      InputFieldState field = _formFields[i];

      inputs.add(
        Container(
          width: Tape.dx(36.0),
          height: Tape.dx(48.0),
          margin: EdgeInsets.only(right: Tape.dx(5)),
          child: _codeInput(field, i),
        ),
      );
    }

    // add a space between the number fields and the text fields
    inputs.insert(
      4,
      Container(
        width: Tape.dx(12),
      ),
    );

    return inputs;
  }

  Widget _codeInput(InputFieldState field, int index) {
    return TextField(
      autocorrect: false,
      controller: field.controller,
      focusNode: field.node,
      textAlign: TextAlign.center,
      keyboardType: field.type,
      textCapitalization: TextCapitalization.characters,
      enabled: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(
          bottom: Tape.dx(20),
          left: Tape.dx(4),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            Tape.dx(12.0),
          ),
          borderSide: BorderSide(
            width: Tape.dx(2),
            color:
                field.isActive ? Theme.of(context).primaryColor : Colors.grey,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            Tape.dx(12.0),
          ),
        ),
      ),
      style: TextStyle(
        fontSize: Tape.dx(22),
        fontWeight: FontWeight.w500,
        color: BMColors.bmDarkblue,
      ),
      onChanged: (String val) {
        _handleInputChange(field, index);
      },
    );
  }

  Widget _actionButton(BuildContext context) {
    return SizedBox(
      width: Tape.dx(52.0),
      height: Tape.dx(48.0),
      child: RaisedButton(
        color: Theme.of(context).primaryColor,
        child: Icon(
          Icons.arrow_forward_ios,
          color: Colors.white,
        ),
        onPressed: _isValid
            ? () {
                String value = _formFields
                    .map((field) => field.controller.text)
                    .toList()
                    .join('');
                widget.onSubmit(value);
              }
            : null,
        shape: new RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(Tape.dx(12.0)),
            topRight: Radius.circular(Tape.dx(4.0)),
            bottomLeft: Radius.circular(Tape.dx(12.0)),
            bottomRight: Radius.circular(Tape.dx(12.0)),
          ),
        ),
      ),
    );
  }

  _handleInputChange(InputFieldState field, int index) {
    String val = field.controller.text.trim().toUpperCase();

    if (val.isNotEmpty && field.validation.allMatches(val[0]).length > 0) {
      if (val.length == 1) {
        if (index < _formFields.length - 1) {
          FocusScope.of(context).requestFocus(_formFields[index + 1].node);
        }
      } else {
        field.controller.text = val[0];
        // then we take the other values to the next input field
        if (index < _formFields.length - 1) {
          InputFieldState nextField = _formFields[index + 1];
          nextField.controller.text = val.substring(1);
          _handleInputChange(nextField, index + 1);
        }
      }

      // here we check if all inputs are correct
      _checkValidity();
    } else {
      // if the val in the text controller is empty, we move the cursor a step back
      if (val.isEmpty) {
        if (index > 0) {
          FocusScope.of(context).requestFocus(_formFields[index - 1].node);
        }
      } else {
        // then clear the text controller at the index
        field.controller.text = '';
      }
    }

    _updateActiveInputs(index);
  }

  _checkValidity() {
    bool valid = true;

    for (var field in _formFields) {
      if (field.controller.text.trim().length == 0) {
        valid = false;
      }
    }

    setState(() {
      _isValid = valid;
    });
  }

  // if the index falls before the first four, the first four are active
  // else the last two are active
  _updateActiveInputs(int index) {
    for (var i = 0; i < _formFields.length; i++) {
      _formFields[i].isActive = false;

      if (index <= 3) {
        if (i <= 3) {
          _formFields[i].isActive = true;
        } else {
          _formFields[i].isActive = false;
        }
      } else {
        if (i > 3) {
          _formFields[i].isActive = true;
        } else {
          _formFields[i].isActive = false;
        }
      }
    }

    setState(() {});
  }

  @override
  void dispose() {
    _formFields.map((field) {
      field.controller.dispose();
      field.node.dispose();
    });

    super.dispose();
  }
}
