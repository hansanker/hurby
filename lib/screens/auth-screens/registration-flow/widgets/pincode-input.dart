import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:hurby/shared/models/inputfield-state.model.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class PinCodeInput extends StatefulWidget {
  final Function onSubmit;
  final String defaultValue;

  PinCodeInput({
    @required this.onSubmit,
    this.defaultValue,
  });

  @override
  _PinCodeInputState createState() {
    return new _PinCodeInputState();
  }
}

class _PinCodeInputState extends State<PinCodeInput> {
  List<InputFieldState> _formFields = [
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.number,
      validation: new RegExp('[0-9]'),
      isActive: false,
    ),
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.number,
      validation: new RegExp('[0-9]'),
      isActive: false,
    ),
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.number,
      validation: new RegExp('[0-9]'),
      isActive: false,
    ),
    InputFieldState(
      node: FocusNode(),
      controller: TextEditingController(),
      type: TextInputType.number,
      validation: new RegExp('[0-9]'),
      isActive: false,
    ),
  ];

  @override
  void initState() {
    super.initState();

    // listens for when the initial build is done
    if (SchedulerBinding.instance.schedulerPhase ==
        SchedulerPhase.persistentCallbacks) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        _formFields[0].controller.text = widget.defaultValue;
        _handleInputChange(_formFields[0], 0);

        int focusIndex = widget.defaultValue.length == 0
            ? 0
            : widget.defaultValue.length - 1;
        FocusScope.of(context).requestFocus(_formFields[focusIndex].node);
      });
    }
  }

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: _buildFormInputs(),
    );
  }

  List<Widget> _buildFormInputs() {
    List<Widget> inputs = [];

    for (var i = 0; i < _formFields.length; i++) {
      InputFieldState field = _formFields[i];

      inputs.add(
        Container(
          width: Tape.dx(36.0),
          height: Tape.dx(48.0),
          margin: EdgeInsets.only(right: Tape.dx(5)),
          child: _codeInput(field, i),
        ),
      );
    }

    return inputs;
  }

  Widget _codeInput(InputFieldState field, int index) {
    return TextField(
      autocorrect: false,
      controller: field.controller,
      focusNode: field.node,
      textAlign: TextAlign.center,
      keyboardType: field.type,
      textCapitalization: TextCapitalization.characters,
      enabled: true,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(
          bottom: Tape.dx(20),
          left: Tape.dx(4),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            Tape.dx(12.0),
          ),
          borderSide: BorderSide(
            width: Tape.dx(2),
            color:
                field.isActive ? Theme.of(context).primaryColor : Colors.grey,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            Tape.dx(12.0),
          ),
        ),
      ),
      style: TextStyle(
        fontSize: Tape.dx(22),
        fontWeight: FontWeight.w500,
        color: BMColors.bmDarkblue,
      ),
      onChanged: (String val) {
        _handleInputChange(field, index);
      },
    );
  }

  _handleInputChange(InputFieldState field, int index) {
    String val = field.controller.text.trim().toUpperCase();

    if (val.isNotEmpty && field.validation.allMatches(val[0]).length > 0) {
      if (val.length == 1) {
        if (index < _formFields.length - 1) {
          FocusScope.of(context).requestFocus(_formFields[index + 1].node);
        }
      } else {
        field.controller.text = val[0];
        // then we take the other values to the next input field
        if (index < _formFields.length - 1) {
          InputFieldState nextField = _formFields[index + 1];
          nextField.controller.text = val.substring(1);
          _handleInputChange(nextField, index + 1);
        }
      }

      // here we check if all inputs are correct
      _checkValidity();
    } else {
      // if the val in the text controller is empty, we move the cursor a step back
      if (val.isEmpty) {
        if (index > 0) {
          FocusScope.of(context).requestFocus(_formFields[index - 1].node);
        }
      } else {
        // then clear the text controller at the index
        field.controller.text = '';
      }
    }
  }

  _checkValidity() {
    bool valid = true;
    String value =
        _formFields.map((field) => field.controller.text).toList().join('');

    for (var field in _formFields) {
      if (field.controller.text.trim().length == 0) {
        valid = false;
      }
    }

    if (valid) {
      widget.onSubmit(value);
    }
  }

  @override
  void dispose() {
    _formFields.map((field) {
      field.controller.dispose();
      field.node.dispose();
    });

    super.dispose();
  }
}
