import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:hurby/shared/models/stepsflow-data.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class AuthHome extends StatefulWidget {
  @override
  _AuthHomeState createState() {
    return _AuthHomeState();
  }
}

class _AuthHomeState extends State<AuthHome> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _langugeIconOpacity;
  Animation<double> _visibleHeight;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
        duration: const Duration(milliseconds: 2500), vsync: this);

    _langugeIconOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.75,
          0.9,
          curve: Curves.ease,
        ),
      ),
    );

    _visibleHeight = Tween<double>(
      begin: Tape.dx(300),
      end: 0.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.75,
          0.9,
          curve: Curves.ease,
        ),
      ),
    );

    // after the view is done rendering
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _controller.forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      mainContent: _mainContent(),
      footerContent: _footerContent(),
    );
  }

  Widget _mainContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(
          height: Tape.dx(50),
        ),
        Container(
          padding: EdgeInsets.all(0),
          child: FlareActor(
            'assets/flare/intro_anim.flr',
            animation: 'Animations',
          ),
          height: Tape.dx(300),
        ),
      ],
    );
  }

  Widget _footerContent() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        AnimatedBuilder(
          builder: (_, __) {
            return Opacity(
              opacity: _langugeIconOpacity.value,
              child: LanguageSelector(),
            );
          },
          animation: _controller,
        ),
        SizedBox(
          height: Tape.dx(20),
        ),
        Container(
          child: AnimatedBuilder(
            builder: _actionSheet,
            animation: _controller,
          ),
        ),
      ],
    );
  }

  Widget _actionSheet(BuildContext context, Widget child) {
    return Container(
      transform: Matrix4.translationValues(
        0.0,
        _visibleHeight.value,
        0.0,
      ),
      child: PaddedContainer(
        contentHeight: Tape.dx(168),
        content: Column(
          children: <Widget>[
            _newUserButton(context),
            _alreadyAnAccountButton(context),
          ],
        ),
      ),
    );
  }

  Widget _newUserButton(BuildContext context) {
    return SizedBox(
      height: Tape.dx(72.0),
      width: double.infinity,
      child: RaisedButton(
        onPressed: () {
          // Goto register flow
          Navigator.of(context).pushReplacementNamed('auth/register');
        },
        padding: EdgeInsets.all(
          Tape.dx(20.0),
        ),
        color: Theme.of(context).primaryColor,
        child: Text(
          'new user',
          style: TextStyle(
            color: Colors.white,
            fontSize: Tape.dx(26.0),
            fontWeight: FontWeight.w500,
          ),
        ),
        shape: new RoundedRectangleBorder(
          borderRadius: AppStyles.buttonRadiusAll,
        ),
      ),
    );
  }

  Widget _alreadyAnAccountButton(BuildContext context) {
    return InkWell(
      onTap: () {
        // Goto register flow
        Navigator.of(context).pushReplacementNamed(
          'auth/login',
          arguments: new AuthFlowData(),
        );
      },
      child: Container(
        padding: EdgeInsets.all(Tape.dx(20.0)),
        alignment: Alignment.center,
        child: Text(
          'Already an account?',
          style: TextStyle(
            decoration: TextDecoration.underline,
            color: Theme.of(context).primaryColor,
            fontSize: Tape.dx(20.0),
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}
