import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/info-message-widgets/info-message-box.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/spinner.service.dart';
import 'package:hurby/utils/screen.util.dart';
import 'package:i18n_extension/i18n_widget.dart';

class AppContainer extends StatefulWidget {
  final Widget content;
  AppContainer({@required this.content});

  @override
  AppContainerState createState() {
    return AppContainerState();
  }
}

class AppContainerState extends State<AppContainer>
    with TickerProviderStateMixin {
  SpinnerService _spinnerService = serviceInjector.spinnerService;
  PopupService _popupService = serviceInjector.popupService;
  AnimationController _infoAnimationController;
  Animation<double> _infoAnimation;
  List<StreamSubscription> _subs = [];
  bool _loading = false;

  @override
  void initState() {
    super.initState();

    _infoAnimationController = AnimationController(
        duration: const Duration(milliseconds: 500), vsync: this);

    _infoAnimation = Tween<double>(
      begin: 1000,
      end: 0.0,
    ).animate(
      CurvedAnimation(
        parent: _infoAnimationController,
        curve: Interval(
          0.0,
          1.0,
          curve: Curves.easeInOut,
        ),
      ),
    );

    // here we subscribe to when the popup is triggered
    _subs.add(
      _popupService.infoBoxStream$.listen((Widget info) {
        if (info != null) {
          _infoAnimationController.forward();
        } else {
          _infoAnimationController.reverse();
        }
      }),
    );

    // here we subscribe to the spinner service
    _subs.add(
      _spinnerService.spinner$.listen((status) {
        setState(() {
          _loading = status;
        });
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    Tape.setSize(MediaQuery.of(context));

    return SafeArea(
      bottom: false,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: WillPopScope(
          onWillPop: () async {
            return Navigator.of(context).canPop();
          },
          child: I18n(
            child: Stack(
              children: <Widget>[
                widget.content,
                AnimatedBuilder(
                  builder: _infoBox,
                  animation: _infoAnimationController,
                ),
                ..._screenCorners(),
                if (_loading)
                  LinearProgressIndicator(
                    backgroundColor: Colors.white,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _infoBox(_, __) {
    return Container(
      transform: Matrix4.translationValues(
        0.0,
        _infoAnimation.value,
        0.0,
      ),
      child: InfoMessageBox(),
    );
  }

  List<Widget> _screenCorners() {
    return [
      Positioned(
        child: Image(
          image: AssetImage('assets/images/screen_corner_topleft.png'),
        ),
        top: 0,
        left: 0,
        width: Tape.dx(24),
      ),
      Positioned(
        child: Image(
          image: AssetImage('assets/images/screen_corner_topright.png'),
        ),
        top: 0,
        right: 0,
        width: Tape.dx(4),
      ),
      Positioned(
        child: Image(
          image: AssetImage('assets/images/screen_corner_bottomleft.png'),
        ),
        bottom: 0,
        left: 0,
        width: Tape.dx(24),
      ),
      Positioned(
        child: Image(
          image: AssetImage('assets/images/screen_corner_bottomright.png'),
        ),
        bottom: 0,
        right: 0,
        width: Tape.dx(24),
      ),
    ];
  }

  @override
  void dispose() {
    super.dispose();

    // unsubscribe to all subs
    _subs.forEach((sub) => sub.cancel());
    _infoAnimationController.dispose();
  }
}
