import 'dart:async';

import 'package:flutter/material.dart';

class Tape {
  const Tape();

  // The default dimensions based on the iphone xr
  static double width = 360;
  static double height = 896;

  static double widthRatio;
  static double heightRatio;
  static Size deviceSize;
  static double keyboardHeight;

  static StreamController<double> _keyboardHeightStreamController =
      new StreamController.broadcast();
  static Stream<double> keyboardHeight$ =
      _keyboardHeightStreamController.stream;

  static setKeyboardHeight(double y) {
    if (keyboardHeight != y) {
      keyboardHeight = y;
      _keyboardHeightStreamController.add(y);
    }
  }

  static setSize(MediaQueryData media) {
    deviceSize = media.size;
    widthRatio = deviceSize.width / width;
    heightRatio = deviceSize.height / height;
    setKeyboardHeight(media.viewInsets.bottom);
  }

  static dx(double x) {
    return widthRatio * x;
  }

  static dy(double y) {
    return heightRatio * y;
  }
}
