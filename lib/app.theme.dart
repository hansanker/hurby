import 'package:flutter/material.dart';

class AppThemeData {
  static final theme = ThemeData(
    primaryColor: Color.fromRGBO(5, 185, 214, 1.0), //#05BCD6
    backgroundColor: Color.fromRGBO(5, 185, 214, 1.0), //#05BCD6
    accentColor: Color.fromRGBO(233, 99, 10, 1.0), // #FFA912
    fontFamily: 'Museo',
    scaffoldBackgroundColor: Color.fromRGBO(5, 185, 214, 1.0), //#05BCD6
  );
}
