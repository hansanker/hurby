import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class SuffixInput extends StatefulWidget {
  final Function onSubmit;
  final String defaultValue;

  SuffixInput({
    @required this.onSubmit,
    this.defaultValue,
  });

  @override
  _SuffixInputState createState() {
    return _SuffixInputState();
  }
}

class _SuffixInputState extends State<SuffixInput> {
  String _value;
  bool _hasValue = false;
  Widget _textInput;

  @override
  void initState() {
    super.initState();

    _value = widget.defaultValue;
    _hasValue = widget.defaultValue.length > 0;
    _updateTextInput();
  }

  _updateTextInput() {
    _textInput = TextInput(
      onSubmit: (String val) {
        widget.onSubmit(val);
      },
      onChange: (String val) {
        setState(() {
          _value = val;
          _hasValue = val.length > 0;
        });
      },
      defaultValue: _value,
      textType: TextInputType.emailAddress,
      min: 1,
      max: 10,
      alwaysActive: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        _hasValue ? _clearOption() : _noneOption(),
        Expanded(
          child: Container(
            padding: EdgeInsets.only(left: Tape.dx(12)),
            child: _textInput,
          ),
        ),
      ],
    );
  }

  Widget _noneOption() {
    return SizedBox(
      height: double.infinity,
      child: RaisedButton(
        color: Theme.of(context).primaryColor,
        child: Text(
          'none',
          style: TextStyle(
            color: Colors.white,
            fontSize: Tape.dx(23),
            fontWeight: FontWeight.w500,
          ),
        ),
        onPressed: () {
          setState(() {
            _value = '';
            widget.onSubmit('');
          });
        },
        shape: RoundedRectangleBorder(
          borderRadius: AppStyles.buttonRadiusAll,
        ),
      ),
    );
  }

  Widget _clearOption() {
    return SizedBox(
      width: 70.0,
      height: double.infinity,
      child: RaisedButton(
        color: Theme.of(context).primaryColor,
        child: Icon(Icons.clear, color: Colors.white),
        onPressed: () {
          setState(() {
            _value = '';
            _hasValue = false;
            _updateTextInput();
          });
        },
        shape: new RoundedRectangleBorder(
          borderRadius: AppStyles.panelRadiusAll,
        ),
      ),
    );
  }
}
