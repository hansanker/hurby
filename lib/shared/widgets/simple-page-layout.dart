import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

class SimplePageLayout extends StatefulWidget {
  final Widget headerContent;
  final Widget mainContent;
  final Widget footerContent;
  final double clearFixBy;

  SimplePageLayout({
    this.headerContent,
    @required this.mainContent,
    @required this.footerContent,
    this.clearFixBy,
  });

  _SimplePageLayoutState createState() {
    return _SimplePageLayoutState();
  }
}

class _SimplePageLayoutState extends State<SimplePageLayout> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            width: Tape.deviceSize.width,
            height: Tape.deviceSize.height,
            color: Theme.of(context).primaryColor,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(
                  top: widget.clearFixBy ?? 0,
                  bottom: Tape.dx(200),
                ),
                constraints: BoxConstraints(
                  minHeight: Tape.dx(500),
                ),
                child: widget.mainContent,
              ),
            ),
          ),
          if (widget.headerContent != null)
            Positioned(
              width: Tape.deviceSize.width,
              top: 0,
              child: widget.headerContent,
            ),
          Positioned(
            width: Tape.deviceSize.width,
            bottom: 0,
            child: widget.footerContent,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
