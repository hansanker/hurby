import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/shared/widgets/simple-page-layout.dart';
import 'package:hurby/shared/widgets/toolbar.dart';
import 'package:hurby/utils/screen.util.dart';

final _titleTextStyle = TextStyle(
  fontSize: Tape.dx(22),
  color: Colors.white,
  fontWeight: FontWeight.w500,
);

final _subtitleTextStyle = TextStyle(
  fontSize: Tape.dx(16),
  color: Colors.white,
  fontWeight: FontWeight.w500,
);

class PropertyEditor extends StatefulWidget {
  final String titleText;
  final String subtitleText;
  final String placeholder;
  final String defaultValue;
  final String headerImg;
  final Function(String) onValue;
  final Function onBack;
  final bool alwaysActive;
  final RegExp regex;
  final int min;
  final int max;
  final TextCapitalization capitalization;
  final Function onChange;
  final TextInputType textType;
  final Widget infoMessage;
  final String homeUrl;

  PropertyEditor({
    @required this.titleText,
    @required this.placeholder,
    @required this.headerImg,
    @required this.textType,
    @required this.onBack,
    @required this.homeUrl,
    this.subtitleText = '',
    this.onChange,
    this.min,
    this.max,
    this.defaultValue,
    this.onValue,
    this.alwaysActive = false,
    this.regex,
    this.capitalization,
    this.infoMessage,
  });

  @override
  _PropertyEditorState createState() {
    return _PropertyEditorState();
  }
}

class _PropertyEditorState extends State<PropertyEditor> {
  StreamSubscription _sub;

  @override
  void initState() {
    super.initState();

    // we listen to the keyboard height changes
    _sub = Tape.keyboardHeight$.listen((double _) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SimplePageLayout(
      headerContent: ToolBar(
        onPrevious: widget.onBack,
        homeUrl: widget.homeUrl,
        infoMessage: widget.infoMessage,
      ),
      mainContent: _bodyContent(),
      footerContent: _footerContent(),
    );
  }

  Widget _bodyContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        SizedBox(height: Tape.dx(40)),
        _logo(),
      ],
    );
  }

  Widget _logo() {
    return Image(
      image: AssetImage(widget.headerImg),
      fit: BoxFit.contain,
    );
  }

  Widget _footerContent() {
    return Container(
      transform: Matrix4.translationValues(
        0.0,
        -(Tape.keyboardHeight),
        0.0,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _titleText(),
          Container(
            height: Tape.dx(150),
            child: InputBox(
              inputField: TextInput(
                alwaysActive: widget.alwaysActive,
                capitalization: widget.capitalization,
                onChange: widget.onChange ?? () {},
                onSubmit: widget.onValue,
                defaultValue: widget.defaultValue,
                textType: widget.textType,
                placeholder: widget.placeholder,
                min: widget.min ?? 3,
                max: widget.max ?? 100,
                regex: widget.regex,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _titleText() {
    return Container(
      padding: EdgeInsets.only(
        left: Tape.dx(40),
        right: Tape.dx(40),
        top: Tape.dx(30),
      ),
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Text(
            widget.titleText,
            textAlign: TextAlign.center,
            style: _titleTextStyle,
          ),
          SizedBox(height: Tape.dx(4)),
          if (widget.subtitleText.isNotEmpty)
            Text(
              widget.subtitleText,
              textAlign: TextAlign.center,
              style: _subtitleTextStyle,
            ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }
}
