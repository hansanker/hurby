import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/shared/widgets/choose-option-popup/choose-option-popup.dart';
import 'package:hurby/shared/widgets/clickable-container.dart';
import 'package:hurby/shared/widgets/icon-button.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

final _titleStyle = TextStyle(
  fontSize: Tape.dx(20),
  fontWeight: FontWeight.w500,
  color: BMColors.bmDarkblue,
);

final headerShadow = BoxShadow(
  color: BMColors.bmPrimaryColor,
  blurRadius: 15.0,
  offset: Offset(0.0, 0.75),
);

class ToolBar extends StatelessWidget {
  final PopupService _popupService = serviceInjector.popupService;
  final Function onPrevious;
  final String title;
  final String homeUrl;
  final Widget infoMessage;
  final bool warnBeforeRouteHome;

  ToolBar({
    @required this.onPrevious,
    @required this.homeUrl,
    this.infoMessage,
    this.title = '',
    this.warnBeforeRouteHome = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          headerShadow,
        ],
        color: this.title.isNotEmpty ? Colors.white : Colors.transparent,
        borderRadius: AppStyles.panelRadiusAll,
      ),
      height: Tape.dx(48),
      child: _getContent(context),
    );
  }

  Widget _getContent(BuildContext context) {
    if (title.isNotEmpty) {
      return Card(
        margin: EdgeInsets.all(0),
        child: _innerContent(context),
        shape: RoundedRectangleBorder(borderRadius: AppStyles.panelRadiusAll),
        elevation: Tape.dx(2),
      );
    } else {
      return _innerContent(context);
    }
  }

  Widget _innerContent(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        ClickableContainer(
          onPressed: onPrevious,
          child: CustomIconButton(
            imageUrl: 'assets/images/submenu_back.png',
            size: Tape.dx(44),
          ),
        ),
        Expanded(
          child: Text(
            title,
            style: _titleStyle,
          ),
        ),
        Row(
          children: <Widget>[
            ClickableContainer(
              onPressed: () {
                _popupService.displayInfo(infoMessage);
              },
              child: CustomIconButton(
                imageUrl: 'assets/images/info.png',
                size: Tape.dx(44),
              ),
            ),
            ClickableContainer(
              onPressed: () async {
                if (warnBeforeRouteHome) {
                  dynamic res = await _popupService.showCustomDialog(
                    context: context,
                    content: ChooseOptionPopup(
                      messsageText: 'You want to stop?',
                      confirmText: 'yes',
                      cancelText: 'back',
                      image: 'assets/images/logo_mus_only.png',
                    ),
                  );

                  if (res) {
                    // Goto register flow
                    Navigator.of(context).pushReplacementNamed(
                      homeUrl,
                    );
                  }
                } else {
                  Navigator.of(context).pushReplacementNamed(
                    homeUrl,
                  );
                }
              },
              child: CustomIconButton(
                imageUrl: 'assets/images/submenu_menu.png',
                size: Tape.dx(44),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
