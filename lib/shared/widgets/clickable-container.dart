import 'package:flutter/material.dart';

class ClickableContainer extends StatelessWidget {
  final EdgeInsets padding;
  final Function onPressed;
  final Widget child;

  ClickableContainer({
    this.padding = EdgeInsets.zero,
    this.onPressed,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      padding: padding,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      minWidth: 0,
      height: 0,
      child: FlatButton(
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}
