import 'package:flutter/material.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

final _boxDecoration = BoxDecoration(
  color: Colors.white,
  border: Border.all(
    color: Colors.transparent,
  ),
  borderRadius: AppStyles.panelRadiusAll,
);

class PaddedContainer extends StatelessWidget {
  final Widget content;
  final double contentHeight;
  final bool showShadow;
  final EdgeInsets padding;

  PaddedContainer({
    @required this.content,
    @required this.contentHeight,
    this.showShadow = false,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    Widget _widgetContent = Container(
      padding: padding ??
          EdgeInsets.all(
            Tape.dx(12.0),
          ),
      decoration: _boxDecoration,
      child: Container(
        height: contentHeight,
        child: content,
      ),
    );

    return showShadow
        ? _shadowContainer(
            _widgetContent,
            context,
          )
        : _widgetContent;
  }

  Widget _shadowContainer(Widget content, BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).primaryColor,
            // has the effect of softening the shadow
            blurRadius: Tape.dx(20.0),
            // has the effect of extending the shadow
            spreadRadius: Tape.dx(10.0),
            offset: Offset(
              Tape.dx(10.0), // horizontal, move right 10
              Tape.dx(10.0), // vertical, move down 10
            ),
          )
        ],
      ),
      child: content,
    );
  }
}
