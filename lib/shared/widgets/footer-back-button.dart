import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/icon-button.dart';
import 'package:hurby/shared/widgets/padded-container.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class FooterbackButtonBar extends StatelessWidget {
  final Function onPressed;

  FooterbackButtonBar({
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return PaddedContainer(
      contentHeight: Tape.dx(44),
      content: ButtonTheme(
        padding: EdgeInsets.symmetric(
          vertical: 0,
          horizontal: 0,
        ),
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        minWidth: 0,
        height: 0,
        child: FlatButton(
          onPressed: onPressed,
          child: Align(
            alignment: Alignment.centerLeft,
            child: CustomIconButton(
              imageUrl: 'assets/images/icon_arrow_left.png',
              size: Tape.dx(50),
            ),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: AppStyles.buttonRadiusAll,
          ),
        ), //your original button
      ),
      showShadow: true,
    );
  }
}
