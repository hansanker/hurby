import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  final String imageUrl;
  final double size;

  CustomIconButton({
    @required this.imageUrl,
    this.size,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      child: Image(
        image: AssetImage(imageUrl),
      ),
    );
  }
}
