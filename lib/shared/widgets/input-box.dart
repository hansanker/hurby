import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/popup.service.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class InputBox extends StatefulWidget {
  final Widget inputField;
  final double height;

  InputBox({
    @required this.inputField,
    this.height,
  });

  @override
  _InputBoxState createState() {
    return _InputBoxState();
  }
}

class _InputBoxState extends State<InputBox> with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _codeInputErrorAnimation;
  PopupService _popupService = serviceInjector.popupService;

  String _errorMessage = '';
  StreamSubscription _sub;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);

    _codeInputErrorAnimation = Tween<double>(
      begin: Tape.dx(40),
      end: Tape.dx(-40),
    ).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(
          0.0,
          1.0,
          curve: Curves.ease,
        ),
      ),
    );

    _sub = _popupService.inputBoxError$.listen((String error) {
      _showError(error);
    });
  }

  @override
  didChangeDependencies() {
    super.didChangeDependencies();
  }

  _showError(String errorMessage) {
    _animationController.forward();

    setState(() {
      _errorMessage = errorMessage;
    });
  }

  _hideError() async {
    await _animationController.reverse();

    setState(() {
      _errorMessage = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: _content,
      animation: _animationController,
    );
  }

  Widget _content(_, __) {
    return Container(
      height: Tape.dx(150),
      child: Stack(
        children: <Widget>[
          Positioned(
            width: Tape.deviceSize.width,
            bottom: 0,
            child: _errorContainer(),
          ),
          Positioned(
            width: Tape.deviceSize.width,
            bottom: 0,
            child: _inputContainer(
              widget.inputField,
            ),
          ),
        ],
      ),
    );
  }

  Widget _inputContainer(Widget input) {
    return Container(
      height: Tape.dx(75),
      padding: EdgeInsets.all(Tape.dx(10)),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          color: Colors.transparent,
        ),
        borderRadius: AppStyles.panelRadiusTop,
        boxShadow: [
          new BoxShadow(
            color: Colors.grey,
            blurRadius: Tape.dx(5.0),
          ),
        ],
      ),
      child: widget.inputField,
    );
  }

  Widget _errorContainer() {
    return Container(
      height: Tape.dx(100),
      alignment: Alignment.topCenter,
      transform:
          Matrix4.translationValues(0.0, _codeInputErrorAnimation.value, 0.0),
      decoration: BoxDecoration(
        color:
            _errorMessage.isNotEmpty ? Color(0xFFffa910) : Colors.transparent,
        border: Border.all(
          color: Colors.transparent,
        ),
        borderRadius: AppStyles.panelRadiusTop,
        boxShadow: [
          new BoxShadow(
            color: Colors.grey,
            blurRadius: Tape.dx(5.0),
          ),
        ],
      ),
      child: _errorMessageBox(),
    );
  }

  Widget _errorMessageBox() {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(
            Tape.dx(30),
            Tape.dx(0),
            Tape.dx(30),
            Tape.dx(15),
          ),
          alignment: Alignment.center,
          child: Text(
            _errorMessage,
            style: TextStyle(
              fontSize: Tape.dx(18),
              color: Colors.white,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        Positioned(
          child: IconButton(
            icon: Icon(
              Icons.close,
              color: Colors.white,
            ),
            onPressed: () {
              _hideError();
            },
          ),
          top: 0,
          right: 0,
        ),
      ],
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    _sub.cancel();
    super.dispose();
  }
}
