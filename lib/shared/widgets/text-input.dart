import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class TextInput extends StatefulWidget {
  final Function onSubmit;
  final Function onChange;
  final String defaultValue;
  final TextInputType textType;
  final String placeholder;
  final RegExp regex;
  final int min;
  final int max;
  final TextCapitalization capitalization;
  final bool alwaysActive;

  TextInput({
    @required this.onSubmit,
    @required this.onChange,
    @required this.textType,
    this.defaultValue,
    this.placeholder,
    this.regex,
    this.capitalization = TextCapitalization.characters,
    @required this.min,
    @required this.max,
    this.alwaysActive = false,
  });

  @override
  _TextInputState createState() {
    return new _TextInputState();
  }
}

class _TextInputState extends State<TextInput> {
  TextEditingController _editingController;
  FocusNode _textNode = FocusNode();
  bool _isValid = false;

  @override
  void initState() {
    super.initState();

    // listens for when the initial build is done
    if (SchedulerBinding.instance.schedulerPhase ==
        SchedulerPhase.persistentCallbacks) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        _initWidget();
      });
    }
  }

  @override
  didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget);
    _initWidget();
  }

  _initWidget() {
    // we initialize it this way if we want to set the cursor position at the end of the text
    _editingController = new TextEditingController.fromValue(
      new TextEditingValue(
        text: widget.defaultValue,
        selection: new TextSelection.collapsed(
          offset: widget.defaultValue.length,
        ),
      ),
    );

    FocusScope.of(context).requestFocus(_textNode);
    _checkValidity(widget.defaultValue);
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      autocorrect: false,
      controller: _editingController,
      focusNode: _textNode,
      keyboardType: widget.textType,
      textCapitalization: widget.capitalization,
      textInputAction: TextInputAction.done,
      enabled: true,
      onEditingComplete: () {
        widget.onSubmit(_editingController.text);
      },
      decoration: InputDecoration(
        hintText: widget.placeholder != null ? widget.placeholder : '',
        hintStyle: TextStyle(
          color: BMColors.bmGrey,
        ),
        border: OutlineInputBorder(
          borderRadius: AppStyles.buttonRadiusAll,
        ),
        contentPadding: EdgeInsets.only(
          left: Tape.dx(20),
        ),
        suffixIcon: SizedBox(
          width: Tape.dx(60.0),
          height: double.infinity,
          child: RaisedButton(
            // disabledColor: Colors.grey,
            color: Theme.of(context).primaryColor,
            child: Icon(
              Icons.arrow_forward_ios,
              color: Colors.white,
            ),
            onPressed: _isValid || widget.alwaysActive
                ? () {
                    widget.onSubmit(_editingController.text);
                  }
                : null,
            shape: new RoundedRectangleBorder(
              borderRadius: AppStyles.buttonRadiusRight,
            ),
          ),
        ),
      ),
      style: TextStyle(
        fontSize: Tape.dx(25),
        fontWeight: FontWeight.w500,
        color: BMColors.bmDarkblue,
      ),
      onChanged: (String val) {
        _checkValidity(val);
        widget.onChange(val);
      },
    );
  }

  _checkValidity(String val) {
    setState(() {
      bool withinRange = val.length >= widget.min && val.length <= widget.max;
      _isValid = widget.regex != null
          ? widget.regex.hasMatch(val) && withinRange
          : withinRange;
    });
  }
}
