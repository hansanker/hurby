import 'package:flutter/material.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

final TextStyle _textStyle = TextStyle(
  color: Colors.white,
  fontSize: Tape.dx(26),
  fontWeight: FontWeight.w500,
);
final _buttonShape = RoundedRectangleBorder(
  borderRadius: AppStyles.panelRadiusAll,
);

class BinaryOption extends StatelessWidget {
  final Function onClickedFirst;
  final Function onClickedSecond;
  final dynamic firstOption;
  final dynamic secondOption;

  BinaryOption({
    @required this.firstOption,
    @required this.secondOption,
    @required this.onClickedFirst,
    @required this.onClickedSecond,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: SizedBox(
            height: double.infinity,
            child: RaisedButton(
              color: Theme.of(context).primaryColor,
              shape: _buttonShape,
              child: _getOption(firstOption),
              onPressed: () {
                onClickedFirst();
              },
            ),
          ),
        ),
        SizedBox(
          width: Tape.dx(8),
        ),
        Expanded(
          child: SizedBox(
            height: double.infinity,
            child: RaisedButton(
              color: Theme.of(context).primaryColor,
              shape: _buttonShape,
              child: _getOption(secondOption),
              onPressed: () {
                onClickedSecond();
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _getOption(var input) {
    if (input.runtimeType == String) {
      return Text(
        input,
        style: _textStyle,
        textAlign: TextAlign.center,
      );
    } else {
      return Container(
        alignment: Alignment.center,
        child: input,
      );
    }
  }
}
