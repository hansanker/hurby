import 'package:flutter/material.dart';
import 'package:hurby/main.dart';
import 'package:hurby/services/label.service.dart';
import 'package:hurby/shared/models/language.dart';
import 'package:hurby/utils/screen.util.dart';

class LanguageSelector extends StatefulWidget {
  final double radiusSize;

  LanguageSelector({
    this.radiusSize,
  });

  @override
  _LanguageSelectorState createState() {
    return _LanguageSelectorState();
  }
}

class _LanguageSelectorState extends State<LanguageSelector> {
  LabelService _labelService = serviceInjector.labelService;

  int _activeLangIndex = 0;
  Language _activeLanguage;
  List<Language> _languages;

  @override
  void initState() {
    super.initState();

    _languages = _labelService.getLanguages();
    _activeLanguage = _labelService.getActiveLanguage();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        child: CircleAvatar(
          backgroundImage: AssetImage(_activeLanguage.flag),
          backgroundColor: Colors.transparent,
          radius: widget.radiusSize ?? Tape.dx(24.87),
        ),
        onTap: () {
          setState(() {
            if (_activeLangIndex < _languages.length - 1) {
              _activeLangIndex += 1;
            } else {
              _activeLangIndex = 0;
            }

            _activeLanguage = _languages[_activeLangIndex];
            _labelService.setActiveLanguage(_activeLanguage);
          });
        },
      ),
    );
  }
}
