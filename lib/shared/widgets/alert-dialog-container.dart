import 'package:flutter/material.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class AlertDialogContainer extends StatelessWidget {
  final BuildContext context;
  final Widget child;

  AlertDialogContainer({
    @required this.context,
    @required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: MediaQuery.of(context).copyWith(
        viewInsets: EdgeInsets.symmetric(horizontal: Tape.dx(-8)),
      ),
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: AppStyles.panelRadiusAll,
        ),
        elevation: Tape.dx(5),
        backgroundColor: Colors.transparent,
        child: child,
      ),
    );
  }
}
