import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

mixin ChooseOptionPopupStyles {
  final titleText = TextStyle(
    fontSize: Tape.dx(21),
    fontWeight: FontWeight.w500,
    color: BMColors.bmDarkblue,
  );

  final buttonStyle = TextStyle(
    color: Colors.white,
    fontSize: Tape.dx(26.0),
    fontWeight: FontWeight.w700,
  );
}
