import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/choose-option-popup/choose-option-popup.styles.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

final _buttonShape = RoundedRectangleBorder(
  borderRadius: AppStyles.buttonRadiusAll,
);

class ChooseOptionPopup extends StatelessWidget with ChooseOptionPopupStyles {
  final String messsageText;
  final String confirmText;
  final Color confirmButtonColor;
  final String cancelText;
  final String image;

  ChooseOptionPopup({
    @required this.messsageText,
    this.confirmText,
    this.cancelText,
    @required this.image,
    this.confirmButtonColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(Tape.dx(12)),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: AppStyles.panelRadiusAll,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            alignment: Alignment.topCenter,
            child: _logo(),
          ),
          Container(
            padding: EdgeInsets.all(Tape.dx(20)),
            child: Text(
              messsageText,
              style: titleText,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: Tape.dx(30)),
          _confirmButton(context),
          SizedBox(height: Tape.dx(10)),
          _backButton(context),
        ],
      ),
    );
  }

  Widget _confirmButton(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        Navigator.of(context).pop(true);
      },
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: confirmButtonColor ?? Theme.of(context).primaryColor,
      child: Text(
        confirmText ?? 'dashboard',
        style: buttonStyle,
      ),
      shape: _buttonShape,
    );
  }

  Widget _backButton(BuildContext context) {
    return RaisedButton(
      onPressed: () {
        Navigator.of(context).pop(false);
      },
      padding: EdgeInsets.all(
        Tape.dx(20),
      ),
      color: Theme.of(context).primaryColor,
      child: Text(
        cancelText ?? 'back',
        style: buttonStyle,
      ),
      shape: _buttonShape,
    );
  }

  Widget _logo() {
    return Image(
      image: AssetImage(image),
      fit: BoxFit.contain,
      width: Tape.dx(314),
      height: Tape.dx(153),
    );
  }
}
