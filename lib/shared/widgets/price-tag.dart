import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

class PriceTag extends StatelessWidget {
  final String unitPrice;
  final String fractionalPrice;
  final bool negative;
  final Color color;

  PriceTag({
    this.unitPrice,
    this.fractionalPrice,
    this.negative = false,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    String minusSign = negative ? '-' : '';
    double price = double.parse(unitPrice) + double.parse(fractionalPrice);

    if (price > 0) {
      return RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          style: DefaultTextStyle.of(context).style,
          children: [
            TextSpan(
              text: '$minusSign € $unitPrice,',
              style: TextStyle(
                fontSize: Tape.dx(20),
                fontWeight: FontWeight.w500,
                color: color ?? Theme.of(context).primaryColor,
              ),
            ),
            TextSpan(
              text: fractionalPrice,
              style: TextStyle(
                fontSize: Tape.dx(16),
                fontWeight: FontWeight.w500,
                color: color ?? Theme.of(context).primaryColor,
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
        padding: EdgeInsets.only(right: Tape.dx(12)),
        alignment: Alignment.centerRight,
        child: Text(
          'free',
          style: TextStyle(
            fontSize: Tape.dx(17),
            fontWeight: FontWeight.w500,
            color: color ?? Theme.of(context).primaryColor,
          ),
        ),
      );
    }
  }
}
