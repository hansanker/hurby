import 'package:flutter/material.dart';
import 'package:hurby/utils/screen.util.dart';

mixin CustomListTileStyle {
  final leadingPadding = EdgeInsets.fromLTRB(
    Tape.dx(8),
    Tape.dx(8),
    Tape.dx(0),
    Tape.dx(8),
  );

  final bodyPadding = EdgeInsets.fromLTRB(
    Tape.dx(16),
    Tape.dx(8),
    Tape.dx(0),
    Tape.dx(8),
  );
}
