import 'package:flutter/material.dart';
import 'package:hurby/shared/models/custom-list-tile-config.dart';
import 'package:hurby/shared/widgets/custom-list-tile/custom-list-tile.style.dart';
import 'package:hurby/shared/widgets/icon-button.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

BorderRadius _borderRadius;
BorderRadius _borderRadiusTrailing;

class CustomListTile extends StatelessWidget with CustomListTileStyle {
  final CustomListTileConfig config;
  final List<CustomListTileConfig> items;

  CustomListTile({
    this.config,
    this.items,
  });

  @override
  Widget build(BuildContext context) {
    final List<Widget> tiles = [];
    (this.items != null ? this.items : [config]).forEach((tileConfig) {
      tiles.add(
        _CustomListTileInner(
          config: tileConfig,
        ),
      );
    });

    return Card(
      margin: EdgeInsets.all(0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: tiles,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: AppStyles.buttonRadiusAll,
      ),
      elevation: Tape.dx(2),
    );
  }
}

// Here is the custom tile inner widget
class _CustomListTileInner extends StatelessWidget with CustomListTileStyle {
  final CustomListTileConfig config;

  _CustomListTileInner({
    this.config,
  });

  @override
  Widget build(BuildContext context) {
    // here we init the border radius
    _updateBorderRadius();

    // if we need to show the bottom border
    Border bottomBorder = config.showBottomBorder
        ? Border(
            bottom: BorderSide(
              color: BMColors.bmGrey,
              width: Tape.dx(0.6),
            ),
          )
        : null;

    return FlatButton(
      padding: EdgeInsets.all(0),
      child: Container(
        decoration: BoxDecoration(
          border: bottomBorder,
        ),
        child: _innerContainer(),
      ),
      onPressed: config.onClick,
      shape: RoundedRectangleBorder(
        borderRadius: _borderRadius,
      ),
    );
  }

  Widget _innerContainer() {
    return Container(
      height: config.tileHeight ?? Tape.dx(90),
      decoration: BoxDecoration(
        borderRadius: _borderRadius,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: Tape.dx(52),
            padding: leadingPadding,
            alignment: Alignment.topCenter,
            child: config.leadingImg != null
                ? Image(
                    image: AssetImage(config.leadingImg),
                    fit: BoxFit.contain,
                  )
                : EmptyWidget(),
          ),
          Expanded(
            child: Container(
              padding: bodyPadding,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  if (config.title != null)
                    Text(
                      config.title,
                      style: TextStyle(
                        fontSize: config.titleSize ?? Tape.dx(17),
                        color: config.titleColor,
                      ),
                    ),
                  if (config.subTitle != null)
                    Text(
                      config.subTitle,
                      style: TextStyle(
                        fontSize: config.subTitleSize ?? Tape.dx(14),
                        color: config.subTitleColor,
                      ),
                    ),
                  if (config.body != null)
                    Stack(
                      children: <Widget>[
                        if (config.isLink)
                          Container(
                            transform: Matrix4.translationValues(
                              Tape.dx(-16),
                              Tape.dx(2),
                              0.0,
                            ),
                            child: CustomIconButton(
                              imageUrl: 'assets/images/icon_arrow_left.png',
                              size: Tape.dx(20),
                            ),
                          ),
                        _bodyContent(),
                      ],
                    ),
                ],
              ),
            ),
          ),
          if (config.trailingWidget != null)
            Container(
              width: config.trailingWidth ?? Tape.dx(30),
              child: config.trailingWidget,
              alignment: config.trailingAlignment,
              decoration: BoxDecoration(
                color: config.trailingBackgroundColor,
                borderRadius: _borderRadiusTrailing,
                border: Border.all(
                  width: Tape.dx(2.5),
                  color: Colors.white,
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget _bodyContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: Tape.dx(4),
        ),
        for (var text in config.body)
          Text(
            text,
            style: TextStyle(
              fontSize: config.bodySize ?? Tape.dx(14),
              color: config.bodyColor,
            ),
          ),
      ],
    );
  }

  // This method update the radius of the tile box accordingly
  _updateBorderRadius() {
    if (config.showBorderRadiusTop && !config.showBorderRadiusBottom) {
      _borderRadius = AppStyles.buttonRadiusTop;
      _borderRadiusTrailing = AppStyles.buttonRadiusRight;
    } else if (config.showBorderRadiusBottom && !config.showBorderRadiusTop) {
      _borderRadius = AppStyles.buttonRadiusRight;
      _borderRadiusTrailing = AppStyles.buttonRadiusRight;
    } else if (config.showBorderRadiusBottom && config.showBorderRadiusTop) {
      _borderRadius = AppStyles.buttonRadiusAll;
      _borderRadiusTrailing = AppStyles.buttonRadiusRight;
    } else {
      _borderRadius = BorderRadius.zero;
      _borderRadiusTrailing = BorderRadius.zero;
    }
  }
}
