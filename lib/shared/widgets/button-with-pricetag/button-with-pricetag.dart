import 'package:flutter/material.dart';
import 'package:hurby/shared/models/button-pricetag-data.dart';
import 'package:hurby/shared/widgets/index.dart';
import 'package:hurby/styles.dart';
import 'package:hurby/utils/screen.util.dart';

class ButtonWithPriceTag extends StatelessWidget {
  final ButtonWithPriceTagData data;
  final Function onPressed;
  final String theme;

  ButtonWithPriceTag({
    @required this.data,
    @required this.onPressed,
    this.theme = 'dark',
  });

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      padding: EdgeInsets.symmetric(
        vertical: 0,
        horizontal: 0,
      ),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      minWidth: 0,
      height: 0,
      child: RaisedButton(
        color: _getMainButtonColor(),
        onPressed: onPressed,
        child: Container(
          height: Tape.dx(72),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              if (data.leadingImage != null)
                Container(
                  padding: EdgeInsets.only(
                    top: Tape.dx(8),
                    bottom: Tape.dx(8),
                  ),
                  width: Tape.dx(72),
                  child: data.leadingImage,
                  decoration: BoxDecoration(
                    borderRadius: AppStyles.buttonRadiusLeft,
                  ),
                ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: EdgeInsets.only(
                    left: Tape.dx(
                      data.leadingImage == null ? 16 : 0,
                    ),
                    top: Tape.dx(8),
                    bottom: Tape.dx(8),
                  ),
                  child: _getBodyContent(),
                ),
              ),
              Container(
                child: Image(
                  image: AssetImage(
                    theme == 'dark'
                        ? 'assets/images/pricetagline_white.png'
                        : 'assets/images/pricetagline_color.png',
                  ),
                ),
              ),
              Container(
                alignment: Alignment.center,
                width: Tape.dx(82),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    _trailingContent(context),
                  ],
                ),
                decoration: BoxDecoration(
                  borderRadius: AppStyles.buttonRadiusRight,
                  color: _getTrailingButtonColor(),
                ),
              ),
            ],
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: AppStyles.buttonRadiusAll,
        ),
      ),
    );
  }

  Widget _trailingContent(BuildContext context) {
    if (data.isActive) {
      return Column(
        children: <Widget>[
          if (data.priceTitle != null)
            Container(
              alignment: Alignment.centerRight,
              padding: EdgeInsets.only(right: Tape.dx(12)),
              child: Text(
                data.priceTitle,
                style: TextStyle(
                  color: theme == 'dark'
                      ? Colors.white
                      : Theme.of(context).primaryColor,
                  fontSize: Tape.dx(14),
                ),
              ),
            ),
          PriceTag(
            unitPrice: data.unitPrice,
            fractionalPrice: data.fractionalPrice,
            color:
                theme == 'dark' ? Colors.white : Theme.of(context).primaryColor,
          )
        ],
      );
    } else {
      return Container(
        padding: EdgeInsets.only(right: Tape.dx(12)),
        alignment: Alignment.centerRight,
        child: Text(
          'not available',
          style: TextStyle(
            color:
                theme == 'dark' ? Colors.white : Theme.of(context).primaryColor,
            fontSize: Tape.dx(13),
          ),
          textAlign: TextAlign.right,
        ),
      );
    }
  }

  Color _getMainButtonColor() {
    if (data.isActive) {
      return theme == 'dark' ? BMColors.bmPrimaryColor : Colors.white;
    } else {
      return Color.fromRGBO(159, 178, 211, 1);
    }
  }

  Color _getTrailingButtonColor() {
    if (data.isActive) {
      return theme == 'dark'
          ? Color.fromRGBO(54, 205, 224, 1)
          : Color.fromRGBO(205, 242, 247, 1);
    } else {
      return Color.fromRGBO(159, 178, 211, 1);
    }
  }

  Widget _getBodyContent() {
    if (data.title != null && data.subtitle != null) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            data.title,
            style: TextStyle(
              color: theme == 'dark' ? Colors.white : BMColors.bmDarkblue,
              fontWeight: FontWeight.w500,
              fontSize: Tape.dx(20),
            ),
          ),
          Text(
            data.subtitle,
            style: TextStyle(
              color: theme == 'dark' ? Colors.white : BMColors.bmPrimaryColor,
              fontWeight: FontWeight.w500,
              fontSize: Tape.dx(14),
            ),
          ),
        ],
      );
    } else if (data.image != null && data.title == null) {
      return Container(
        alignment: Alignment.center,
        child: data.image,
      );
    } else {
      return Text(
        data.title,
        style: TextStyle(
          color: theme == 'dark' ? Colors.white : BMColors.bmPrimaryColor,
          fontWeight: FontWeight.w500,
          fontSize: Tape.dx(24),
        ),
      );
    }
  }
}
