import 'package:flutter/material.dart';

class InputFieldState {
  final TextInputType type;
  final FocusNode node;
  final TextEditingController controller;
  final RegExp validation;
  bool isActive;

  InputFieldState({
    this.node,
    this.controller,
    this.type,
    this.validation,
    this.isActive,
  });
}
