import 'package:flutter/cupertino.dart';

class Language {
  String symbol;
  String flag;
  String name;

  Language({
    @required this.symbol,
    @required this.flag,
    @required this.name,
  });
}
