import 'package:flutter/material.dart';

class CustomListTileConfig {
  double tileHeight;
  String leadingImg;
  String title;
  Color titleColor;
  double titleSize;
  String subTitle;
  Color subTitleColor;
  double subTitleSize;
  List<String> body;
  Color bodyColor;
  double bodySize;
  Function onClick;
  Widget trailingWidget;
  Alignment trailingAlignment;
  Color trailingBackgroundColor;
  double trailingWidth;
  bool showBorderRadiusTop;
  bool showBorderRadiusBottom;
  bool showBottomBorder;
  bool isLink;

  // here we set the defaults for this configs
  CustomListTileConfig({
    this.tileHeight,
    this.leadingImg,
    this.title,
    this.titleColor,
    this.titleSize,
    this.subTitle,
    this.subTitleColor,
    this.subTitleSize,
    this.body,
    this.bodyColor,
    this.bodySize,
    this.onClick,
    this.trailingWidget,
    this.trailingAlignment,
    this.trailingBackgroundColor,
    this.trailingWidth,
    this.showBorderRadiusTop = false,
    this.showBorderRadiusBottom = false,
    this.showBottomBorder = false,
    this.isLink = false,
  });
}
