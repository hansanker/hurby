import 'package:flutter/material.dart';

class ButtonWithPriceTagData {
  String id;
  Image image;
  Image leadingImage;
  String title;
  String subtitle;
  String unitPrice;
  String fractionalPrice;
  String priceTitle;
  bool isActive;

  ButtonWithPriceTagData({
    this.id,
    this.image,
    this.leadingImage,
    this.title,
    this.subtitle,
    this.unitPrice,
    this.fractionalPrice,
    this.priceTitle,
    this.isActive = true,
  });
}
