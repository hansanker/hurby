import 'package:hurby/shared/models/button-pricetag-data.dart';
import 'package:hurby/shared/models/profile.dart';

List<ParcelSizeData> _parcelSides = [
  new ParcelSizeData(
    id: 'less_than_50',
    title: '0 - 50 cm',
    price: 0.0,
    isActive: true,
  ),
  new ParcelSizeData(
    id: '50_to_100',
    title: '50 - 100 cm',
    price: 0.0,
    isActive: true,
    priceTitle: 'extra',
  ),
  new ParcelSizeData(
    id: 'longer_than_100',
    title: '100 cm +',
    price: 0.0,
    isActive: false,
  ),
];

class WebshopData {
  String did;
  String companyCode;
  String companyName;
  String companyImage;
  double price;

  WebshopData({
    this.did,
    this.companyCode,
    this.companyName,
    this.companyImage,
    this.price,
  });
}

class ParcelSizeData {
  String id;
  String title;
  double price;
  bool isActive;
  String priceTitle;

  ParcelSizeData({
    this.id,
    this.title,
    this.price,
    this.isActive,
    this.priceTitle,
  });
}

class WebShopReturnFlowData {
  int allCount = 0;
  dynamic packageSizeSurchargeRawData;
  dynamic pickupInfoRawData;
  dynamic selectedPickupDate;
  WebshopData selectedWebShop;
  WebshopData otherWebshop;
  List<WebshopData> webshopList = [];
  ButtonWithPriceTagData selectedParcelType;
  ParcelSizeData selectedPackageLength;
  List<ParcelSizeData> parcelSides = _parcelSides;
  Profile userProfile;
  dynamic activeOrder = {};
}

class AuthFlowData {
  String postalCode = '';
  String houseNumber = '';
  String suffix = '';
  String emailAddress = '';
  String phoneNumber = '06';
  String errorMessage = '';
  String verificationId = '';
  String pinCode = '';
  String street = '';
  String city = '';
  bool addressHasSuffix;
  dynamic registeredUserResponse;
  Profile userProfile;
}

class PaymentFlowData {
  List<dynamic> bankList = [];
  dynamic selectedBank;
  String paymentUrl;
  String txId;
  List<dynamic> orders = [];
  String promoCode = '';
  dynamic promocodePrice;
}

class LibraryInfo {
  String id;
  String title;
  String subtitle;
  double price;
  String imageUrl;
  String priceTitle;
  String companyCode;

  LibraryInfo({
    this.id,
    this.title,
    this.subtitle,
    this.price,
    this.imageUrl,
    this.priceTitle,
    this.companyCode,
  });
}

class LibraryFlowData {
  dynamic pickupInfoRawData;
  dynamic selectedPickupDate;
  Profile userProfile;
  int itemCount;
  List<LibraryInfo> libraryList = [];
  LibraryInfo selectedLibrary;
  dynamic activeOrder = {};
}
