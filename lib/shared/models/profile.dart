class SenderReceiverDTO {
  String personDid;
  String type;
  bool verified;
  String companyName;
  bool knockFirst;
  bool retailerPaysPickupFee;

  SenderReceiverDTO({
    this.personDid,
    this.type,
    this.verified,
    this.companyName,
    this.knockFirst,
    this.retailerPaysPickupFee,
  });

  Map<String, dynamic> toJSON() {
    return {
      'personDid': this.personDid,
      'type': this.type,
      'verified': this.verified,
      'companyName': this.companyName,
      'knockFirst': this.knockFirst,
      'retailerPaysPickupFee': this.retailerPaysPickupFee,
    };
  }

  factory SenderReceiverDTO.fromJson(dynamic json) {
    return SenderReceiverDTO(
      personDid: "${json['personDid']}",
      type: json['type'],
      verified: json['verified'],
      companyName: json['companyName'],
      knockFirst: json['knockFirst'],
      retailerPaysPickupFee: json['retailerPaysPickupFee'],
    );
  }
}

class PhysicalAddressDTO {
  String did;
  String addressLine1;
  String postalCode;
  String city;
  String countryCode;
  String houseNumber;
  String houseNumberSuffix;

  PhysicalAddressDTO({
    this.did,
    this.addressLine1,
    this.postalCode,
    this.city,
    this.countryCode,
    this.houseNumber,
    this.houseNumberSuffix,
  });

  Map<String, dynamic> toJSON() {
    return {
      'did': this.did,
      'addressLine1': this.addressLine1,
      'postalCode': this.postalCode,
      'city': this.city,
      'countryCode': this.countryCode,
      'houseNumber': this.houseNumber,
      'houseNumberSuffix': this.houseNumberSuffix,
    };
  }

  factory PhysicalAddressDTO.fromJson(dynamic json) {
    return PhysicalAddressDTO(
      did: "${json['did']}",
      addressLine1: json['addressLine1'],
      postalCode: json['postalCode'],
      city: json['city'],
      countryCode: json['countryCode'],
      houseNumber: json['houseNumber'],
      houseNumberSuffix: json['houseNumberSuffix'],
    );
  }
}

class PersonDTO {
  String did;
  String type;
  String firstName;
  String lastName;
  String mobile;
  String email;
  bool active;
  String affiliateCode;
  SenderReceiverDTO senderReceiverDTO;
  PhysicalAddressDTO physicalAddressDTO;

  PersonDTO({
    this.did,
    this.type,
    this.firstName,
    this.lastName,
    this.mobile,
    this.email,
    this.active,
    this.affiliateCode,
    this.senderReceiverDTO,
    this.physicalAddressDTO,
  });

  Map<String, dynamic> toJSON() {
    return {
      'did': this.did,
      'type': this.type,
      'firstName': this.firstName,
      'lastName': this.lastName,
      'mobile': this.mobile,
      'active': this.active,
      'affiliateCode': this.affiliateCode,
      'senderReceiverDTO': this.senderReceiverDTO.toJSON(),
      'physicalAddressDTO': this.physicalAddressDTO.toJSON(),
    };
  }

  factory PersonDTO.fromJson(dynamic json) {
    return PersonDTO(
      did: "${json['did']}",
      type: json['type'],
      firstName: json['firstName'] ?? '',
      lastName: json['lastName'] ?? '',
      mobile: json['mobile'] ?? '',
      active: json['active'],
      affiliateCode: json['affiliateCode'],
      senderReceiverDTO: SenderReceiverDTO.fromJson(json['senderReceiverDTO']),
      physicalAddressDTO:
          PhysicalAddressDTO.fromJson(json['physicalAddressDTO']),
    );
  }
}

class SiteDTO {
  String siteDid;
  dynamic parentSite;

  SiteDTO({
    this.siteDid,
    this.parentSite,
  });

  Map<String, dynamic> toJSON() {
    return {
      'siteDid': this.siteDid,
      'parentSite': this.parentSite,
    };
  }

  factory SiteDTO.fromJson(dynamic json) {
    return SiteDTO(
      siteDid: "${json['siteDid']}",
      parentSite: json['parentSite'],
    );
  }
}

class Profile {
  String did;
  String accessToken;
  String username;
  String userEmail;
  String role;
  bool active;
  String lang;
  PersonDTO personDTO;
  SiteDTO siteDTO;

  Profile({
    this.did,
    this.accessToken,
    this.username,
    this.userEmail,
    this.role,
    this.active,
    this.lang,
    this.personDTO,
    this.siteDTO,
  });

  Map<String, dynamic> toJSON() {
    return {
      'did': this.did,
      'accessToken': this.accessToken,
      'username': this.username,
      'userEmail': this.userEmail,
      'role': this.role,
      'active': this.active,
      'lang': this.lang,
      'personDTO': this.personDTO.toJSON(),
      'siteDTO': this.siteDTO.toJSON(),
    };
  }

  factory Profile.fromJson(dynamic json) {
    return Profile(
      did: "${"${json['did']}"}",
      username: json['username'],
      userEmail: json['userEmail'],
      role: json['role'],
      active: json['active'],
      lang: json['lang'],
      personDTO: PersonDTO.fromJson(json['personDTO']),
      siteDTO: SiteDTO.fromJson(json['siteDTO']),
    );
  }
}
