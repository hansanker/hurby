class PriceBreakdown {
  String unitPrice;
  String fractionalPrice;

  PriceBreakdown({
    this.unitPrice,
    this.fractionalPrice,
  });
}
