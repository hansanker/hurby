import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hurby/app-container.dart';
import 'package:hurby/app.theme.dart';
import 'package:hurby/screens/auth-screens/auth-screens.dart';
import 'package:hurby/screens/dashboard-screens/dashboard-screens.dart';
import 'package:hurby/services/injector.dart';
import 'package:hurby/shared/widgets/index.dart';

/*
 * Run the main app
 */
Injector serviceInjector;

// Main app always in portrait mode
void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(new MyApp(
    key: ValueKey('root'),
  ));
}

// Here is the app main component
class MyApp extends StatefulWidget {
  final ValueKey key;
  MyApp({this.key}) : super(key: key);

  @override
  MyAppState createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  bool _isInjectorReady = false;

  @override
  void initState() {
    super.initState();

    // Set the statusbar to black
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.light.copyWith(
        statusBarColor: Colors.black,
        statusBarIconBrightness: Brightness.light,
      ),
    );

    // The preferred screen orientation
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
        .then((_) {
      // here we init the injector
      _initInjector();
    });
  }

  _initInjector() async {
    serviceInjector = new Injector();
    await serviceInjector.init();

    setState(() {
      _isInjectorReady = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isInjectorReady) {
      return MaterialApp(
        title: 'Hurby',
        theme: AppThemeData.theme,
        debugShowCheckedModeBanner: false,
        initialRoute: '/auth',
        routes: {
          '/': (_) => EmptyWidget(),
          '/auth': (context) {
            return AppContainer(
              content: AuthScreen(),
            );
          },
          '/dashboard': (context) {
            return AppContainer(
              content: DashboardScreen(),
            );
          },
        },
      );
    } else {
      return Container(
        color: Color.fromRGBO(5, 185, 214, 1.0),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
