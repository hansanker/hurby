import 'package:hurby/shared/models/language.dart';

class LabelService {
  List<Language> _languages = [
    new Language(
      name: 'Dutch',
      symbol: 'nl',
      flag: 'assets/images/nl.png',
    ),
    new Language(
      name: 'English',
      symbol: 'en',
      flag: 'assets/images/en.png',
    ),
  ];

  Language _activeLanguage;

  LabelService() {
    _activeLanguage = _languages[0];
  }

  Language getActiveLanguage() {
    return _activeLanguage;
  }

  List<Language> getLanguages() {
    return _languages;
  }

  void setActiveLanguage(Language lang) {
    _activeLanguage = lang;
  }
}
