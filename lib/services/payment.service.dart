import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client;
import 'package:hurby/services/index.dart';
import 'package:hurby/services/storage.service.dart';
import 'dart:convert';

class PaymentService {
  String _baseUrl = 'http://149.210.228.159:8080/Buurtmus/rest';
  StorageService storageService;
  LabelService labelService;
  Client _client = Client();

  PaymentService({
    @required this.storageService,
    @required this.labelService,
  });

  Future<dynamic> getPromoCode(String promoCode) async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/customerApp/getPromoCode?promoCode=$promoCode',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  // customerApp/updatePayment
  Future<dynamic> updatePayment(List<int> orderDids, String promocodeId) async {
    dynamic payload = {
      'loginDetails': {
        'accessToken': storageService.getItem('accessToken'),
        'lang': labelService.getActiveLanguage().symbol
      },
      'orderDids': orderDids,
      'promoCodeDid': promocodeId
    };

    final response = await _client.post(
      '$_baseUrl/customerApp/updatePayment',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  // getBanksForPaymentGateway
  Future<List<dynamic>> getBanksForPaymentGateway() async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/md/getBanksForPaymentGateway?paymentGateway=SISOW&paymentMethod=IDEAL',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  // checkPaymentStatus
  Future<dynamic> checkPaymentStatus(String txId) async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/customerApp/checkPaymentStatus?txId=$txId',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  // getSisowPaymentURL
  Future<dynamic> getSisowPaymentURL(
      List<int> orderDids, String promocodeId, String bankCode) async {
    dynamic payload = {
      'loginDetails': {
        'accessToken': storageService.getItem('accessToken'),
        'lang': labelService.getActiveLanguage().symbol
      },
      'bankCode': bankCode,
      'orderDids': orderDids,
      'promoCodeDid': promocodeId
    };

    final response = await _client.post(
      '$_baseUrl/customerApp/getSisowPaymentURL',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }
}
