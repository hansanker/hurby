import 'package:flutter/material.dart';
import 'package:http/http.dart' show Client;
import 'package:hurby/services/index.dart';
import 'dart:convert';

import 'package:hurby/services/storage.service.dart';
import 'package:hurby/shared/models/price-breakdown.dart';

class OrderService {
  String _baseUrl = 'http://149.210.228.159:8080/Buurtmus/rest';
  Client _client = Client();
  StorageService storageService;
  LabelService labelService;

  OrderService({
    @required this.storageService,
    @required this.labelService,
  });

  Future<dynamic> getShipmentRateSummary() async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/md/getShipmentRateSummary',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> getRetailers() async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/order/getRetailers',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> getLibrariesPerHub(String hubId) async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/customerApp/getLibrariesPerHub?hubDid=$hubId',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> getOtherWebshopRate() async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/customerApp/getOtherRetailerFee',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<List<dynamic>> getUnPaidOrders() async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/customerApp/getUnPaidOrders',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> getPackageSizeSurcharge() async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/md/getPackageSizeSurcharge',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> getOrderCountPerOrderType() async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/customerApp/getOrderCountPerOrderType',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> getOrderDetails(String orderId) async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/customerApp/getOrderDetails?did=$orderId',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> getPickupInfo() async {
    dynamic payload = {
      'loginDetails': {
        'accessToken': storageService.getItem('accessToken'),
        'lang': labelService.getActiveLanguage().symbol
      },
      'issueOrderNumber': 'Y'
    };

    final response = await _client.post(
      '$_baseUrl/customerApp/getPickupInfo',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> createOrder(dynamic order) async {
    dynamic payload = {
      'loginDetails': {
        'accessToken': storageService.getItem('accessToken'),
        'lang': labelService.getActiveLanguage().symbol,
      },
      'orders': [order],
    };

    final response = await _client.post(
      '$_baseUrl/customerApp/createOrders',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> deleteOrder(int orderId) async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.delete(
      '$_baseUrl/order/deleteOrder/$orderId',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  PriceBreakdown getPriceComponents(
    double priceNumber,
  ) {
    List<String> priceComponents = [];
    if (priceNumber > 0) {
      priceComponents = priceNumber.toStringAsFixed(2).split('.');
    } else {
      priceComponents = ['0', '00'];
    }

    return new PriceBreakdown(
      unitPrice: priceComponents[0],
      fractionalPrice: priceComponents[1],
    );
  }
}
