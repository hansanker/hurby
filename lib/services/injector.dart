import 'package:hurby/services/auth.service.dart';
import 'package:hurby/services/data.service.dart';
import 'package:hurby/services/index.dart';
import 'package:hurby/services/label.service.dart';
import 'package:hurby/services/order.service.dart';
import 'package:hurby/services/payment.service.dart';
import 'package:hurby/services/popup.service.dart';
import 'package:hurby/services/postcode.service.dart';
import 'package:hurby/services/spinner.service.dart';
import 'package:hurby/services/storage.service.dart';

class Injector {
  AuthService authService = new AuthService();
  PopupService popupService = new PopupService();
  SpinnerService spinnerService = new SpinnerService();
  PostcodeService postcodeService = new PostcodeService();
  LabelService labelService = new LabelService();
  StorageService storageService = new StorageService();

  PaymentService paymentService;
  ProfileService profileService;
  OrderService orderService;
  DataService dataService;

  Future<bool> init() async {
    storageService = new StorageService();
    await storageService.initStorage();

    profileService = new ProfileService(
      storageService: storageService,
    );

    orderService = new OrderService(
      storageService: storageService,
      labelService: labelService,
    );

    dataService = new DataService(
      orderService: orderService,
    );

    paymentService = new PaymentService(
      storageService: storageService,
      labelService: labelService,
    );

    return true;
  }
}
