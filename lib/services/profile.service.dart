import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' show Client;
import 'dart:convert';

import 'package:hurby/services/storage.service.dart';
import 'package:hurby/shared/models/profile.dart';

class ProfileService {
  String _baseUrl = 'http://149.210.228.159:8080/Buurtmus/rest/customerApp';
  Client _client = Client();
  StorageService storageService;

  ProfileService({
    @required this.storageService,
  });

  Future<Profile> getCurrentUserProfile() async {
    String accessToken = storageService.getItem('accessToken');
    dynamic payload = {"accessToken": accessToken};

    final response = await _client.post(
      '$_baseUrl/getProfile',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    if (response.statusCode == 200) {
      return Profile.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> updateUserProfile(Profile profile) async {
    String accessToken = storageService.getItem('accessToken');
    profile.accessToken = accessToken;

    final response = await _client.post(
      '$_baseUrl/updateProfile',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(profile.toJSON()),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }

  Future<dynamic> getMainMenuTexts() async {
    String accessToken = storageService.getItem('accessToken');

    final response = await _client.get(
      '$_baseUrl/getMainMenuTexts',
      headers: {
        'Content-Type': 'application/json',
        'accessToken': accessToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }
}
