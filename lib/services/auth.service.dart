import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' show Client;
import 'dart:convert';

import 'package:hurby/services/storage.service.dart';

class AuthService {
  String _baseUrl = 'http://149.210.228.159:8080/Buurtmus/rest/customerApp';
  Client _client = Client();
  StorageService _storageService = new StorageService();

  AuthService() {
    // here we init the storage service
    _storageService.initStorage();
  }

  Future<bool> sendRegistrationOTP(String number) async {
    final response = await _client.get(
      '$_baseUrl/sendRegistrationOTP?mobileNo=$number',
      headers: {
        'Content-Type': 'application/json',
      },
    );

    return response.body == 'true';
  }

  Future<bool> verifyRegistrationOTP({
    @required String number,
    @required String otp,
  }) async {
    final response = await _client.get(
      '$_baseUrl/verifyOTP?mobileNo=$number&otp=$otp',
      headers: {
        'Content-Type': 'application/json',
      },
    );

    return response.body == 'true';
  }

  Future<String> sendLoginOTP(String number) async {
    final response = await _client.get(
      '$_baseUrl/sendLoginOTP?mobileNo=$number',
      headers: {
        'Content-Type': 'application/json',
      },
    );

    dynamic res = json.decode(response.body);
    return res['responseCode'];
  }

  Future<dynamic> verifyLoginOTP({
    @required String number,
    @required String otp,
  }) async {
    final response = await _client.get(
      '$_baseUrl/loginWithOTP?mobileNo=$number&otp=$otp',
      headers: {
        'Content-Type': 'application/json',
      },
    );

    return json.decode(response.body);
  }

  Future<dynamic> registerUser({
    @required dynamic payload,
  }) async {
    final response = await _client.post(
      '$_baseUrl/registerUser',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    return json.decode(response.body);
  }

  bool isLoggedIn() {
    return _storageService.getItem('accessToken') != null;
  }

  // may refactor this
  void logout(BuildContext context) {
    _storageService.clear();
    Navigator.of(context, rootNavigator: true)
        .pushReplacementNamed('/auth');
  }
}
