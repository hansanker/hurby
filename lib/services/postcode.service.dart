import 'package:flutter/widgets.dart';
import 'package:http/http.dart' show Client;
import 'dart:convert';

class PostcodeService {
  String _baseUrl = 'http://149.210.228.159:8080/Buurtmus/rest/customerApp';
  Client _client = Client();

  Future<bool> isvalidNeighbourhood({
    @required String houseNumber,
    @required String postalCode,
    @required String suffix,
    @required String lang,
  }) async {
    dynamic payload = {
      'loginDetails': {
        'appId': 'CONSUMER_APP',
        'accessToken': '',
        'userName': '',
        'password': '',
        'lang': lang,
      },
      'address': {
        'houseNumber': houseNumber,
        'houseNumberSuffix': suffix,
        'street': '',
        'postalCode': postalCode,
        'city': '',
        'countryCode': 'NL'
      }
    };

    final response = await _client.post(
      '$_baseUrl/isValidNeighbourhood',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    if (response.statusCode == 200) {
      String res = response.body;
      return res == 'true';
    } else {
      // If that call was not successful, we return false
      return false;
    }
  }

  Future<bool> saveFailedRegistrationAttempt({
    @required String houseNumber,
    @required String postalCode,
    @required String suffix,
    @required String lang,
    @required String email,
  }) async {
    dynamic payload = {
      'loginDetails': {
        'appId': 'CONSUMER_APP',
        'accessToken': '',
        'userName': '',
        'lang': lang,
      },
      'address': {
        'houseNumber': houseNumber,
        'houseNumberSuffix': suffix,
        'street': '',
        'postalCode': postalCode,
        'city': '',
        'countryCode': 'NL'
      },
      'email': email,
      'mobileNumber': '',
      'firstName': '',
      'lastName': '',
      'type': 'PERSONAL'
    };

    final response = await _client.post(
      '$_baseUrl/saveFailedRegistrationAttempt',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    if (response.statusCode == 200) {
      String res = response.body;
      return res == 'true';
    } else {
      // If that call was not successful, we return false
      return false;
    }
  }

  Future<dynamic> getAddress({
    @required String houseNumber,
    @required String postalCode,
    @required String suffix,
    @required String lang,
  }) async {
    dynamic payload = {
      'loginDetails': {
        'appId': 'CONSUMER_APP',
        'accessToken': '',
        'userName': '',
        'lang': lang,
      },
      'address': {
        'houseNumber': houseNumber,
        'houseNumberSuffix': suffix,
        'street': '',
        'postalCode': postalCode,
        'city': '',
        'countryCode': 'NL'
      },
    };

    final response = await _client.post(
      '$_baseUrl/getAddress',
      headers: {
        'Content-Type': 'application/json',
      },
      body: json.encode(payload),
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      // If that call was not successful, we return false
      return null;
    }
  }
}
