import 'dart:async';

class SpinnerService {
  StreamController<bool> _spinnerStreamController;
  Stream<bool> spinner$;
  bool isLoading = false;

  SpinnerService() {
    _spinnerStreamController = new StreamController.broadcast();
    spinner$ = _spinnerStreamController.stream;
  }

  void toggleSpinner(bool status) {
    isLoading = status;
    _spinnerStreamController.add(status);
  }
}
