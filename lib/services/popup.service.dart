import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hurby/shared/widgets/alert-dialog-container.dart';
import 'package:hurby/utils/screen.util.dart';

class PopupService {
  // input box stream
  StreamController<String> _inputBoxErrorStreamController;
  Stream<String> inputBoxError$;

  // info box stream
  StreamController<Widget> _infoBoxStreamController;
  Stream<Widget> infoBoxStream$;

  PopupService() {
    // input box stream
    _inputBoxErrorStreamController = new StreamController.broadcast();
    inputBoxError$ = _inputBoxErrorStreamController.stream;

    // info box stream
    _infoBoxStreamController = new StreamController.broadcast();
    infoBoxStream$ = _infoBoxStreamController.stream;
  }

  // here we just hold a global variable to show the popup on on the

  void showInputBoxError(String error) {
    _inputBoxErrorStreamController.add(error);
  }

  void displayInfo(Widget info) {
    _infoBoxStreamController.add(info);
  }

  void dismissInfo() {
    _infoBoxStreamController.add(null);
  }

  void displayToastErrorMessage(String message, BuildContext context) {
    final snackBar = SnackBar(
      content: Text(
        message,
        style: TextStyle(color: Colors.white, fontSize: Tape.dx(16)),
      ),
      backgroundColor: Colors.red,
      behavior: SnackBarBehavior.floating,
    );

    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    Scaffold.of(context).showSnackBar(snackBar);
  }

  void displayToastSuccessMessage(String message, BuildContext context) {
    final snackBar = SnackBar(
      content: Text(
        message,
        style: TextStyle(color: Colors.white, fontSize: Tape.dx(16)),
      ),
      backgroundColor: Colors.green,
      behavior: SnackBarBehavior.floating,
    );

    // Find the Scaffold in the widget tree and use it to show a SnackBar.
    Scaffold.of(context).showSnackBar(snackBar);
  }

  Future<dynamic> showCustomDialog({BuildContext context, Widget content}) {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => AlertDialogContainer(
        context: context,
        child: content,
      ),
    );
  }
}
