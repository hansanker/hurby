import 'package:hurby/services/order.service.dart';
import 'package:hurby/shared/models/button-pricetag-data.dart';
import 'package:hurby/shared/models/price-breakdown.dart';

class DataService {
  List<ButtonWithPriceTagData> _parcelTypes = [
    new ButtonWithPriceTagData(
      id: 'STD_SHIPMENT_NL_FLOW',
      title: 'parcel NL - EU',
      priceTitle: 'from',
      unitPrice: '0',
      fractionalPrice: '00',
    ),
    new ButtonWithPriceTagData(
      id: 'STD_RETURN_LIBRARY_FLOW',
      title: 'library return',
      unitPrice: '0',
      fractionalPrice: '00',
    ),
    new ButtonWithPriceTagData(
      id: 'STD_RETURN_FLOW',
      title: 'webshop return',
      unitPrice: '0',
      fractionalPrice: '00',
    ),
    new ButtonWithPriceTagData(
      id: 'STD_SHIPMENT_LOCAL_FLOW',
      title: 'local parcel',
      unitPrice: '0',
      fractionalPrice: '00',
    ),
  ];

  bool _popupOnOverviewPage = true;

  OrderService orderService;

  DataService({
    this.orderService,
  });

  updateParcelRates(List<dynamic> rates) async {
    // here we go through the parcel types and update them
    for (dynamic rawParcelData in rates) {
      // find the parcel in the list of parcel types and update it
      ButtonWithPriceTagData parcelFromList = _parcelTypes.firstWhere(
        (parcel) {
          return parcel.id == rawParcelData['orderFlow'];
        },
        orElse: () {
          return null;
        },
      );

      if (parcelFromList != null) {
        // if the price type is minimum, we show the from subtag
        String pivPriceString = '${rawParcelData['piv']}';
        double pivPriceNumber = double.tryParse(pivPriceString);

        if (rawParcelData['priceType'] == 'MINIMUM') {
          parcelFromList.priceTitle = 'from';
        }

        PriceBreakdown priceComponents = orderService.getPriceComponents(
          pivPriceNumber,
        );

        // update the parcel
        parcelFromList.unitPrice = priceComponents.unitPrice;
        parcelFromList.fractionalPrice = priceComponents.fractionalPrice;
      }
    }
  }

  List<ButtonWithPriceTagData> getParcelTypes() {
    return _parcelTypes;
  }

  bool canPopupOnOverviewPage() {
    bool res = _popupOnOverviewPage;
    _popupOnOverviewPage = false;

    return res;
  }
}
