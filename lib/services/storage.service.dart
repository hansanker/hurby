import 'package:shared_preferences/shared_preferences.dart';

class StorageService {
  SharedPreferences _storage;

  initStorage() async {
    _storage = await SharedPreferences.getInstance();
  }

  getItem(String key) {
    return _storage.get(key);
  }

  Future<bool> setItem(String key, String value) {
    return _storage.setString(key, value);
  }

  Future<bool> clear() {
    return _storage.clear();
  }
}
