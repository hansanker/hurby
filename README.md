# hurby com.hurby.app

# Dev commands
flutter emulators
flutter emulators --launch apple_ios_simulator - ios
flutter emulators --launch Nexus_5X_API_27 - android
flutter run
flutter build appbundle
flutter build apk

# Open Xcode work space
flutter build ios && open ios/Runner.xcworkspace

# release flutter lock manually
rm ./flutter/bin/cache/lockfile

# with the google-info.plist
for ios we copy the file into the ios/runner file.
Then we open xcode and also drag the reference to the file in our project to the runner file in xcode

# useful resource
https://github.com/flutter/plugins/blob/master/packages/firebase_auth/example/lib/signin_page.dart
https://github.com/fireship-io/192-flutter-fcm-push-notifications/blob/master/lib/main.dart
https://medium.com/flutter-community/a-deep-dive-into-flutter-textfields-f0e676aaab7a
https://appitventures.com/blog/how-to-setup-flutter-on-android-studio/
https://flutter.dev/docs/deployment/android
https://flutter.dev/docs/development/data-and-backend/state-mgmt/options
https://stackoverflow.com/questions/55140674/dispose-widget-when-navigating-to-new-route
https://www.didierboelens.com/2018/06/widget---state---context---inheritedwidget/
https://flutter.dev/docs/development/ui/interactive
https://medium.com/flutter-community/i18n-extension-flutter-b966f4c65df9
https://api.flutter.dev/flutter/widgets/LimitedBox-class.html

# to enable extensions
dartanalyzer --enable-experiment=extension-methods

# update flutter launcher icons
flutter pub run flutter_launcher_icons:main

# update spash screen 
flutter pub run flutter_native_splash:create